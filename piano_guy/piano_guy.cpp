//include files
#include <gl/freeglut.h>
#include <cmath>
#include <stdio.h>
#include <string.h>
#include <conio.h>


//macros
#define PI 3.14159
#define RAD(x)  (x * (PI/180.0f))

//global variable declarations
bool bIsFullScreen = false;

//entry point function
int main(int argc, char* argv[])
{
	//function declarations
	void initializer(void);
	void resize(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void uninitializer(void);

	//code
	glutInit(&argc, argv);							//glutInit is used to initialize the GLUT library.


	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);	//glutInitDisplayMode sets the initial display mode.bitwise OR-ing of GLUT display mode bit masks
	glutInitWindowSize(400, 400);					//sets window size
	glutInitWindowPosition(150, 150);					//sets position from which window will appear
	glutCreateWindow("Dont know what it is");		//title to window

	initializer();

	glutReshapeFunc(resize);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutCloseFunc(uninitializer);


	glutMainLoop();


	return (0);
}

void initializer(void)
{
	//glClearColor(16.0f/255.0f, 218.0f/255.0f, 235.0f/255.0f, 1.0f);
	glClearColor(0.5f, 0.5f, 0.5f, 1.0f);//fills window background with given clrs (r,g,b,alpha)
}

void resize(int width, int height)
{
	glMatrixMode(GL_PROJECTION);					//Specifies which matrix stack is the target for subsequent matrix operations
	glLoadIdentity();								//replace the current matrix with the identity matrix
	glViewport(0, 0, (GLsizei)width, (GLsizei)height); //specifies the area of the window that the drawing region should be put into. 
}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();


	//func declaration 
	void DrawRajpalYadav(void);

	
	DrawRajpalYadav();
	

	glutSwapBuffers();
}

void DrawRajpalYadav(void)
{
	//shirt
	glBegin(GL_QUADS);
	{
		glColor3f(255.0f / 255.0f, 55.0f / 255.0f, 128.0f / 255.0f);

		glVertex3f(0.18f, 0.5f, 0.0f);
		glVertex3f(-0.18f, 0.5f, 0.0f);
		glVertex3f(-0.18f, 0.0f, 0.0f);
		glVertex3f(0.18f, 0.0f, 0.0f);
	}
	glEnd();

	

	//pants
	glBegin(GL_QUADS);
	{
		glColor3f(1.0f, 1.0f, 1.0f);

		glVertex3f(0.18f, 0.0f, 0.0f);
		glVertex3f(-0.18f, 0.0f, 0.0f);
		glVertex3f(-0.18f, -0.1f, 0.0f);
		glVertex3f(0.18f, -0.1f, 0.0f);

	}
	glEnd();

	glBegin(GL_QUADS);
	{
		glColor3f(1.0f,1.0f,1.0f);

		glVertex3f(-0.01f, -0.1f, 0.0f);
		glVertex3f(-0.18f, -0.1f, 0.0f);
		glVertex3f(-0.18f,- 0.6f, 0.0f);
		glVertex3f(-0.1f, -0.6f, 0.0f);
		
		
	}
	glEnd();

	glBegin(GL_QUADS);
	{
		glColor3f(1.0f, 1.0f, 1.0f);

		glVertex3f(0.18f, -0.1f, 0.0f);
		glVertex3f(0.01f, -0.1f, 0.0f);
		glVertex3f(0.1f, -0.6f, 0.0f);
		glVertex3f(0.18f, -0.6f, 0.0f);
	}
	glEnd();

	//neck
	glBegin(GL_QUADS);
	{
		glColor3f(242.0f / 255.0f, 200.0f / 255.0f, 182.0f / 255.0f);

		glVertex3f(0.03f, 0.55f, 0.0f);
		glVertex3f(-0.03f, 0.55f, 0.0f);
		glVertex3f(-0.03f, 0.5f, 0.0f);
		glVertex3f(0.03f, 0.5f, 0.0f);

	}
	glEnd();

	//bow
	glBegin(GL_TRIANGLES);
	{
		glColor3f(0.1f, 0.1f, 0.1f);

		glVertex3f(0.0f, 0.5f, 0.0f);
		glVertex3f(-0.06f, 0.52f, 0.0f);
		glVertex3f(-0.06f, 0.48f, 0.0f);
	}
	glEnd();

	glBegin(GL_TRIANGLES);
	{
		glColor3f(0.1f, 0.1f, 0.1f);

		glVertex3f(0.0f, 0.5f, 0.0f);
		glVertex3f(0.06f, 0.48f, 0.0f);
		glVertex3f(0.06f, 0.52f, 0.0f);

	}
	glEnd();

	//head
	float fAngle = 0.0f;
	float fRadx = 0.09f;
	float fRady = 0.18f;
	float fX = 0, fY = 0;

	glBegin(GL_LINE_LOOP);
	for (fAngle = 0.0f; fAngle <= 360.0f; fAngle += 0.1f)
	{
		fX = cos(RAD(fAngle)) * fRadx;
		fY = sin(RAD(fAngle)) * fRady;

		glColor3f(230.0f / 255.0f, 190.0f / 255.0f, 172.0f / 255.0f);

		glVertex3f(0.0f, 0.71f, 0.0f);
		glVertex3f(fX, fY + 0.71f, 0.0f);

	}
	glEnd();
	

	////head2
	//glBegin(GL_POLYGON);
	//{
	//	glColor3f(255.0f / 255.0f, 232.0f / 255.0f, 201.0f / 255.0f);

	//	glVertex3f(0.02f, 0.54f, 0.0f);
	//	glVertex3f(0.05f, 0.60f, 0.0f);
	//	glVertex3f(0.07f, 0.65f, 0.0f);
	//	glVertex3f(0.09f, 0.69f, 0.0f);
	//	glVertex3f(-0.09f, 0.69f, 0.0f);
	//	glVertex3f(-0.07f, 0.65f, 0.0f);
	//	glVertex3f(-0.05f, 0.60f, 0.0f);
	//	glVertex3f(-0.02f, 0.54f, 0.0f);

	//}
	//glEnd();

	//hairs
	fRadx = 0.09f;
	fRady = 0.19f;
	glBegin(GL_POLYGON);
	{
		glColor3f(0.1f, 0.1f, 0.1f);
		for (fAngle = 20.0f; fAngle <= 160.0f; fAngle += 0.1f)
		{
			fX = cos(RAD(fAngle)) * fRadx;
			fY = sin(RAD(fAngle)) * fRady;

			glVertex3f(fX, fY + 0.71f, 0.0f);

		}

		glVertex3f(-0.05f, 0.8f, 0.0f);
		glVertex3f(-0.05f, 0.75f, 0.0f);

		glVertex3f(0.0f, 0.8f, 0.0f);
		glVertex3f(0.0f, 0.75f, 0.0f);

		glVertex3f(0.05f, 0.8f, 0.0f);
		glVertex3f(0.05f, 0.75f, 0.0f);

	}
	glEnd();

	//hands
	//left
	glBegin(GL_QUADS);
	{
		glColor3f(255.0f / 255.0f, 55.0f / 255.0f, 128.0f / 255.0f);

		glVertex3f(-0.18f, 0.5f, 0.0f);
		glVertex3f(-0.5f, 0.5f, 0.0f);
		glVertex3f(-0.5f, 0.37f, 0.0f);
		glVertex3f(-0.18f, 0.37f, 0.0f);

	}
	glEnd();

	//right
	glBegin(GL_QUADS);
	{
		glColor3f(255.0f / 255.0f, 55.0f / 255.0f, 128.0f / 255.0f);

		glVertex3f(0.18f, 0.5f, 0.0f);
		glVertex3f(0.5f, 0.5f, 0.0f);
		glVertex3f(0.5f, 0.37f, 0.0f);
		glVertex3f(0.18f, 0.37f, 0.0f);

	}
	glEnd();
	
	//ears
	fRadx = 0.01f;
	fRady = 0.05f;
	//right ear
	fX = 0, fY = 0;
	glBegin(GL_LINE_LOOP);
	for (fAngle = 0.0f; fAngle <= 360.0f; fAngle += 0.1f)
	{
		fX = cos(RAD(fAngle)) * fRadx;
		fY = sin(RAD(fAngle)) * fRady;

		glColor3f(230.0f / 255.0f, 190.0f / 255.0f, 172.0f / 255.0f);

		glVertex3f(0.09f, 0.69f, 0.0f);
		glVertex3f(fX+0.09, fY + 0.69f, 0.0f);

	}
	glEnd();

	//left ear
	fX = 0, fY = 0;
	glBegin(GL_LINE_LOOP);
	for (fAngle = 0.0f; fAngle <= 360.0f; fAngle += 0.1f)
	{
		fX = cos(RAD(fAngle)) * fRadx;
		fY = sin(RAD(fAngle)) * fRady;

		glColor3f(230.0f / 255.0f, 190.0f / 255.0f, 172.0f / 255.0f);

		glVertex3f(-0.09f, 0.69f, 0.0f);
		glVertex3f(fX - 0.09, fY + 0.69f, 0.0f);

	}
	glEnd();

	//thuglife glasses
	//left glass
	glBegin(GL_QUADS);
	{
		glColor3f(0.0f, 0.0f, 0.0f);

		glVertex3f(-0.02f, 0.73f, 0.0f);
		glVertex3f(-0.08f, 0.73f, 0.0f);
		glVertex3f(-0.08f, 0.68f, 0.0f);
		glVertex3f(-0.02f, 0.68f, 0.0f);

	}
	glEnd();
	//white squares
	glBegin(GL_QUADS);
	{
		glColor3f(1.0f, 1.0f, 1.0f);

		glVertex3f(-0.07f, 0.70f, 0.0f);
		glVertex3f(-0.08f, 0.70f, 0.0f);
		glVertex3f(-0.08f, 0.69f, 0.0f);
		glVertex3f(-0.07f, 0.69f, 0.0f);

	}
	glEnd();
	glBegin(GL_QUADS);
	{
		glColor3f(1.0f, 1.0f, 1.0f);

		glVertex3f(-0.05f, 0.70f, 0.0f);
		glVertex3f(-0.06f, 0.70f, 0.0f);
		glVertex3f(-0.06f, 0.69f, 0.0f);
		glVertex3f(-0.05f, 0.69f, 0.0f);

	}
	glEnd();
	glBegin(GL_QUADS);
	{
		glColor3f(1.0f, 1.0f, 1.0f);

		glVertex3f(-0.06f, 0.69f, 0.0f);
		glVertex3f(-0.07f, 0.69f, 0.0f);
		glVertex3f(-0.07f, 0.68f, 0.0f);
		glVertex3f(-0.06f, 0.68f, 0.0f);

	}
	glEnd();
	glBegin(GL_QUADS);
	{
		glColor3f(1.0f, 1.0f, 1.0f);

		glVertex3f(-0.04f, 0.69f, 0.0f);
		glVertex3f(-0.05f, 0.69f, 0.0f);
		glVertex3f(-0.05f, 0.68f, 0.0f);
		glVertex3f(-0.04f, 0.68f, 0.0f);
	}
	glEnd();

	//right glass
	glBegin(GL_QUADS);
	{
		glColor3f(0.0f, 0.0f, 0.0f);

		glVertex3f(0.02f, 0.73f, 0.0f);
		glVertex3f(0.08f, 0.73f, 0.0f);
		glVertex3f(0.08f, 0.68f, 0.0f);
		glVertex3f(0.02f, 0.68f, 0.0f);

	}
	glEnd();
	//white squares
	glBegin(GL_QUADS);
	{
		glColor3f(1.0f, 1.0f, 1.0f);

		glVertex3f(0.03f, 0.70f, 0.0f);
		glVertex3f(0.02f, 0.70f, 0.0f);
		glVertex3f(0.02f, 0.69f, 0.0f);
		glVertex3f(0.03f, 0.69f, 0.0f);

	}
	glEnd();
	glBegin(GL_QUADS);
	{
		glColor3f(1.0f, 1.0f, 1.0f);

		glVertex3f(0.05f, 0.70f, 0.0f);
		glVertex3f(0.04f, 0.70f, 0.0f);
		glVertex3f(0.04f, 0.69f, 0.0f);
		glVertex3f(0.05f, 0.69f, 0.0f);

	}
	glEnd();
	glBegin(GL_QUADS);
	{
		glColor3f(1.0f, 1.0f, 1.0f);

		glVertex3f(0.04f, 0.69f, 0.0f);
		glVertex3f(0.03f, 0.69f, 0.0f);
		glVertex3f(0.03f, 0.68f, 0.0f);
		glVertex3f(0.04f, 0.68f, 0.0f);

	}
	glEnd();
	glBegin(GL_QUADS);
	{
		glColor3f(1.0f, 1.0f, 1.0f);

		glVertex3f(0.06f, 0.69f, 0.0f);
		glVertex3f(0.05f, 0.69f, 0.0f);
		glVertex3f(0.05f, 0.68f, 0.0f);
		glVertex3f(0.06f, 0.68f, 0.0f);
	}
	glEnd();
	//nose point
	glBegin(GL_QUADS);
	{
		glColor3f(0.0f, 0.0f, 0.0f);

		glVertex3f(0.02f, 0.73f, 0.0f);
		glVertex3f(-0.02f, 0.73f, 0.0f);
		glVertex3f(-0.02f, 0.72f, 0.0f);
		glVertex3f(0.02f, 0.72f, 0.0f);
	}
	glEnd();


	//shoes
	//left
	glBegin(GL_POLYGON);
	{
		glColor3f(0.0f, 0.0f, 0.0f);

		glVertex3f(-0.1f, -0.6f, 0.0f);
		glVertex3f(-0.18f, -0.6f, 0.0f);
		glVertex3f(-0.25f, -0.65f, 0.0f);
		glVertex3f(-0.25f, -0.70f, 0.0f);
		glVertex3f(-0.1f, -0.70f, 0.0f);

	}
	glEnd();

	//right
	glBegin(GL_POLYGON);
	{
		glColor3f(0.0f, 0.0f, 0.0f);

		glVertex3f(0.1f, -0.6f, 0.0f);
		glVertex3f(0.1f, -0.70f, 0.0f);
		glVertex3f(0.25f, -0.70f, 0.0f);
		glVertex3f(0.25f, -0.65f, 0.0f);
		glVertex3f(0.18f, -0.6f, 0.0f);
	}
	glEnd();




}


void keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 27:
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if (bIsFullScreen == false)
		{
			glutFullScreen();
			bIsFullScreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bIsFullScreen = false;
		}
		break;
	default:
		break;
	}
}

void uninitializer(void)
{
	//
}

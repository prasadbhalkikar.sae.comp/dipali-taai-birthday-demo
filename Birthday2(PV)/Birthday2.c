#include<Windows.h> // Windows header files 
#include "OGL.h"
#include<stdlib.h> // For exit
#include<stdio.h> // For file IO (input output)
#include<math.h>
#include<mmsystem.h>



// OpenGL header files
#include<gl/GL.h>
#include<gl/GLU.h>

// MACROS

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#define PB_PI 3.14159
#define RAD(x)  (x * (PB_PI/180.0f))  // MACRO by vinit in from SCENE 1 file
#define SSH_PI 3.14159
#define RADIUS 0.1f
#define PS_PI 3.142f
#define DBB_PI 3.14159
#define PAB_PI 3.14159265359


//macros for rotation
#define RIGHT_HAND_ANGLE_END 20.0f

// Link with OpenGL libraray

#pragma comment(lib,"OpenGL32.lib")
#pragma comment(lib, "glu32.lib")
#pragma comment(lib, "Winmm.lib")

//global function delaration



LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM); // Entry point function

// global variable declaration
FILE* gpFile = NULL;

HWND ghwnd = NULL;
BOOL gbActive = FALSE;

DWORD dwStyle = 0;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
BOOL gbFullscreen = FALSE;



//OpenGL related Global variables
HDC ghdc = NULL;   // DC
HGLRC ghrc = NULL; // rendating context


// variable and structs declaration for all things

GLfloat girl_leg_angle = 0.0f;

struct sPosition
{
	float x;
	float y;
	float z;
};

struct cColor
{
	float r;
	float g;
	float b;
};

struct sPosition sCarPosition = { 0.0f ,-0.75f,0.0f };
struct sPosition sLinePosition = { 0.0f , -0.80f,0.0f };
struct sPosition sLinePosition1 = { 0.0f , 1.3f,0.0f };
struct sPosition sLinePosition2 = { 0.0f , -0.95f,0.0f };
struct sPosition sLinePosition3 = { 0.0f , -0.95f,0.0f };
struct sPosition sPositionHappy = { 0.0f , 1.3f,0.0f };
struct sPosition sPositionBirthday = { 0.0f , 1.3f,0.0f};
struct sPosition sPositionDipali = { 0.0f , 1.3f,0.0f };
struct sPosition sRajuPosition = { -0.95f , 0.0f,0.0f };
struct sPosition sDanceCharacterPositionLeft = { -2.0f , 0.0f,0.0f };
struct sPosition sDanceCharacterPositionRight = { 2.0f , 0.0f,0.0f };

struct sPosition sColorChangeRGB = { 0.0f , 0.0f,0.0f };


struct sPosition sGirlPosition = { 0.0f,0.3f,0.0f };
struct sPosition sGirlPositiontranslate = { 0.0f,-1.0f,-1.0f };

struct sPosition sLeftHandAngle = { 0.0f,0.0f,358.0f };
struct sPosition sLeftHandPosi = { 0.0f,0.0f,0.0f };
struct sPosition sRightHandAngle = { 0.0f,0.0f,358.0f };
struct sPosition sRightHandBottomPosi = { 0.0f,0.0f,0.0f };

struct cColor sCarBrake = { 0.0f,0.0f,0.0f };
struct cColor sceneChange = { 0.0f,0.0f,0.0f };
float alpha = 0.0f;



float x_1 = 0.0f;
float y_1 = 0.0f;
float angleRadian;

float M1T = -2.5f;
float T1Ch = TRUE;
float M1S = 1.0f;

float M2T = -2.5f;
float T2Ch = FALSE;
float balloonTranslate1 = -1.0f;

BOOL check = FALSE;
BOOL check2 = FALSE;

GLfloat Bx = 0.0f;
GLfloat angle = 0.0f;

BOOL doorClose = TRUE;

float ScaleX = 0.05f;
float ScaleY = 0.05f;
float ScaleZ = 0.05f;

float lettersR = 173.0f / 255.0f;
float lettersG = 37.0f / 255.0f;
float lettersB = 218.0f / 255.0f;

BOOL scene_1 = TRUE;
BOOL scene_2 = FALSE;
BOOL scene_3 = FALSE;
BOOL scene_4 = FALSE;
BOOL scene_5 = FALSE;
BOOL bIsReversed = FALSE;
BOOL sceneChange_1 = FALSE;
BOOL sceneChangeDecrease = FALSE;

float topYCoordinate = 2.0f;
float bottomYCoordinate = -2.0f;
//float yCoordinate = topYCoordinate;
float yCoordinateChange = 0.00025f;


void renderBitmapChar(float x, float y, char* string);


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCnmdLine, int iCmdShow) // function defition
{

	// Function Declarations

	int initialize(void);
	void uninitialized(void);
	void display(void);
	void update(void);

	// Local variable Declarations

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("PBWindow");
	int iResult = 0;
	BOOL bDone = FALSE;

	gpFile = fopen("Log.txt", "w");
	if (gpFile == NULL)
	{
		MessageBox(NULL, TEXT("Log file cannot be open!!!"), TEXT("File create error"), MB_OK | MB_ICONERROR);
		exit(0);
	}
	fprintf(gpFile, "Program started successfully \n");

	//Code
	//WNDCLASSEX initialisation

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	// Register WNDCLASSEX
	RegisterClassEx(&wndclass);

	int WW = WIN_WIDTH;
	int WH = WIN_HEIGHT;

	float SW = GetSystemMetrics(SM_CXSCREEN);
	float SH = GetSystemMetrics(SM_CYSCREEN);
	int X = (SW - WW) / 2;
	int Y = (SH - WH) / 2;


	//Create Window 
	hwnd = CreateWindowEx(
		WS_EX_APPWINDOW,
		szAppName,
		TEXT("Prasad Bhalkikar"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		X,
		Y,
		WW,
		WH,
		NULL,
		NULL,
		hInstance,
		NULL
	);

	ghwnd = hwnd;

	// initialization

	iResult = initialize();

	if (iResult != 0)
	{
		MessageBox(hwnd, TEXT("Initialize() FAILED !!!"), TEXT(" error"), MB_OK | MB_ICONERROR);
		DestroyWindow(hwnd);

	}

	// Show Window
	ShowWindow(hwnd, iCmdShow);

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);


	// Game Loop
	while (bDone == FALSE)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = TRUE;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActive == TRUE)
			{
				// Render
				display();

				// Update
				update();

			}
		}
	}

	//uninitialization
	uninitialized();

	return((int)msg.wParam);
}

// callBack Function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declaration

	void resize(int, int);

	void ToggleFullscreen(void);

	HDC hdc;
	PAINTSTRUCT ps;
	RECT rc;
	TCHAR str[] = TEXT("Hello World !!!");

	// code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActive = TRUE;
		break;

	case WM_KILLFOCUS:
		gbActive = FALSE;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_ERASEBKGND:
		return 0;


	case WM_KEYDOWN:
		switch (LOWORD(wParam))
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;

	case WM_CHAR:
		switch (LOWORD(wParam))
		{
		case 'F':
		case 'f':
			if (gbFullscreen == FALSE)
			{
				ToggleFullscreen();
				gbFullscreen = TRUE;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = FALSE;
			}
			break;
		}
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));

}

void ToggleFullscreen(void)
{
	// local variable declarations

	MONITORINFO mi = { sizeof(MONITORINFO) };

	// code
	if (gbFullscreen == FALSE)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(
					ghwnd,
					HWND_TOP, //WS_OVERLAPPED is brought back using this statement 
					mi.rcMonitor.left, // this is rect rc , that is rectasngle vala rc giving us left of monitor  
					mi.rcMonitor.top, // this is rect rc , that is rectasngle vala rc giving us top of monitor
					mi.rcMonitor.right - mi.rcMonitor.left, // right of monitor - left gives width
					mi.rcMonitor.bottom - mi.rcMonitor.top, // height of
					SWP_NOZORDER | SWP_FRAMECHANGED
				);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPos(
			ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED
		);
		ShowCursor(TRUE);
	}

}

int initialize(void)
{
	// Function Declarations

	void resize(int, int);

	// Code
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR)); 	// Remaining members are kept zero in pfd struct using this.

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	// Get the DC (device context , painter)

	ghdc = GetDC(ghwnd);
	if (ghdc == NULL)
	{
		fprintf(gpFile, "GetDC() failed \n");
		return -1;
	}

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd); // iPixelFormatIndex must be always non-zero positive
	if (iPixelFormatIndex == 0)
	{
		fprintf(gpFile, "ChoosePixelFormat() Failed \n");
		return -2;
	}

	// set obtained pixel format step-4

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		fprintf(gpFile, "SetPixelFormat() Failed \n");
		return -3;
	}
	// Tell Windows graphing libraray to give OpenGL compatible context from this DC
	// Create OpenGl context from device context.

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		fprintf(gpFile, "wglCreateContext() failed \n");
		return -4;
	}
	// now ghdc will end its role and will give control to ghrc for furthrt steps
	//Make rendering context current

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(gpFile, "wglMakeCurrent() failed \n");
		return -5;
	}
	// set the clear colour of window to blue

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	// Here OpenGL starts.....


	//PlaySound(MAKEINTRESOURCE(MYWAVE), GetModuleHandle(NULL), SND_RESOURCE | SND_ASYNC | SND_LOOP);


	PlaySound(MAKEINTRESOURCE(MYWAVE4), GetModuleHandle(NULL), SND_RESOURCE | SND_ASYNC);
	resize(WIN_WIDTH, WIN_HEIGHT);

	return 0;
}

void resize(int width, int height)
{
	// code
	if (height <= 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	//gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.f);

}

void display(void)
{
	// prototype

	void scene5(void);
	void scene1(void);
	void scene2(void);
	void scene3(void);
	void musicSymbol(void);
	void scene4(void);
	void quad_blackOut(void);

	// Code
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// background quad 


	if (scene_1 == TRUE)
	{
		scene1();
	}
	//if (sceneChange_1 == TRUE && scene_1 == FALSE)
	//{
	//	quad_blackOut();
	//}
	if (scene_1 == FALSE && scene_2 == TRUE)
	{
		scene2();
	}
	if (scene_2 == FALSE && scene_3 == TRUE && scene_1 == FALSE)
	{
		scene3();
	}
	if (scene_3 == FALSE && scene_2 == FALSE && scene_4 == TRUE && scene_1 == FALSE)
	{
		scene4();
	}
	if (scene_3 == FALSE && scene_2 == FALSE && scene_5 == TRUE && scene_1 == FALSE && scene_4 == FALSE)
	{
		scene5();
	}

	scene2();
	scene_2 = TRUE;




	


	SwapBuffers(ghdc);

}

void quad_blackOut(void)
{
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glBegin(GL_QUADS);
	{

		glColor4f(sceneChange.r, sceneChange.g, sceneChange.b, alpha);
		glVertex3f(1.0f, 1.0f, 0.0f);
		glVertex3f(-1.0f, 1.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 0.0f);
		glVertex3f(1.0f, -1.0f, 0.0f);
	}
	glEnd();
	glDisable(GL_BLEND);

}

void scene1(void)
{
	void DrawRoad(void);
	void DrawFootPath(void);
	void DrawSea(void);
	void DrawCar(void);

	DrawRoad();
	DrawFootPath();
	DrawSea();

	glTranslatef(sCarPosition.x, sCarPosition.y, sCarPosition.z);
	glScalef(0.50f, 0.75f, 0.25f);
	DrawCar();

	
	glLoadIdentity();

}

void scene2(void) {
	// prototype
	//func declaration 
	void DrawRoad2(void);
	void DrawFootPath2(void);
	void DrawSky2(void);
	void DrawCar2(void);
	void DrawStreetLamp2(void);
	void theatreBuilding(void);
	void DrawBuilding1(void);
	void DrawBuilding2(void);
	void sideViewCar(void);


	//code

	DrawSky2();
	DrawRoad2();
	DrawFootPath2();

	glLoadIdentity();
	glTranslatef(1.0f, -0.3f, 0.0f);
	glScalef(0.4f, 0.5f, 0.0f);
	DrawBuilding1();

	glLoadIdentity();
	glTranslatef(-0.84f, -0.32f, 0.0f);
	glScalef(0.4f, 0.4f, 0.0f);
	DrawBuilding2();

	glLoadIdentity();
	glTranslatef(-0.8f, -0.5f, 0.0f);
	glScalef(0.15f, 0.2f, 0.0f);
	DrawStreetLamp2();

	glLoadIdentity();
	glTranslatef(0.8f, -0.5f, 0.0f);
	glScalef(0.15f, 0.2f, 0.0f);
	DrawStreetLamp2();

	glLoadIdentity();
	glTranslatef(0.0f, -0.36f, 0.0f);
	glScalef(0.8f, 1.2f, 0.0f);
	theatreBuilding();

	glLoadIdentity();
	glTranslatef(sLinePosition.x, sLinePosition.y, sLinePosition.z);
	//glScalef(0.50f, 0.75f, 0.25f);

	glLineWidth(2.0f);
	glBegin(GL_LINES);
	{
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.00001f, 0.0f, 0.0f);
	}
	glEnd();

	glLoadIdentity();
	glTranslatef(0.0f, -0.7f, 0.0f);
	glScalef(0.5f, 0.6f, 0.0f);
	sideViewCar();


	glLoadIdentity();


}

void sideViewCar(void)
{


	glBegin(GL_QUADS); // Car Base
	glColor3f(255.0f / 255.0f, 59.0f / 255.0f, 139.0f / 255.0f);
	glVertex3f(0.6f, -0.2f, 0.0f);
	glVertex3f(0.6f, 0.0f, 0.0f);
	glVertex3f(-0.6f, 0.0f, 0.0f);
	glVertex3f(-0.6f, -0.2f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); // Car Upper Base
	glColor3f(255.0f / 255.0f, 59.0f / 255.0f, 139.0f / 255.0f);
	glVertex3f(0.35f, 0.0f, 0.0f);
	glVertex3f(0.25f, 0.2f, 0.0f);
	glVertex3f(-0.25f, 0.2f, 0.0f);
	glVertex3f(-0.35f, 0.0f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); // Back Widow
	glColor3f(188.0f / 255.0f, 188.0f / 255.0f, 237.0f / 255.0f);
	glVertex3f(-0.02f, 0.0f, 0.0f);
	glVertex3f(-0.02f, 0.15f, 0.0f);
	glVertex3f(-0.25f, 0.15f, 0.0f);
	glVertex3f(-0.3f, 0.0f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); // Front Widow
	glColor3f(188.0f / 255.0f, 188.0f / 255.0f, 237.0f / 255.0f);
	glVertex3f(0.02f, 0.0f, 0.0f);
	glVertex3f(0.02f, 0.15f, 0.0f);
	glVertex3f(0.25f, 0.15f, 0.0f);
	glVertex3f(0.3f, 0.0f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); // Car Light
	glColor3f(246.0f / 255.0f, 255.0f / 255.0f, 91.0f / 255.0f);
	glVertex3f(0.9f, -0.15f, 0.0f);
	glVertex3f(0.9f, 0.0f, 0.0f);
	glVertex3f(0.6f, -0.05f, 0.0f);
	glVertex3f(0.6f, -0.1f, 0.0f);
	glEnd();

	//---------------- Car Wheels-------------

	// back mud flaps
	float angle = 0.0f;
	float x = 0.0f;
	float y = 0.0f;

	float angleRadian = 0.0f;

	glBegin(GL_LINES);

	for (angle = 0.0f; angle <= 180.0f; angle = angle + 0.10f)
	{
		float r = 0.12f;
		angleRadian = angle * (SSH_PI / 180.0f);
		x = cos(angleRadian) * r;
		y = sin(angleRadian) * r;

		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.4f, -0.2f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(x + (-0.4f), y + (-0.2f), 0.0f);
	}

	glEnd();

	// front mud flaps

	glBegin(GL_LINES);

	for (angle = 0.0f; angle <= 180.0f; angle = angle + 0.10f)
	{
		float r = 0.12f;
		angleRadian = angle * (SSH_PI / 180.0f);
		x = cos(angleRadian) * r;
		y = sin(angleRadian) * r;

		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.4f, -0.2f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(x + (0.4f), y + (-0.2f), 0.0f);
	}

	glEnd();

	//back wheels

	glBegin(GL_LINES); // outer circle

	for (angle = 0.0f; angle <= 360.0f; angle = angle + 0.10f)
	{
		float r = 0.1f;
		angleRadian = angle * (SSH_PI / 180.0f);
		x = cos(angleRadian) * r;
		y = sin(angleRadian) * r;

		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(-0.4f, -0.2f, 0.0f);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(x + (-0.4f), y + (-0.2f), 0.0f);
	}

	glEnd();


	glBegin(GL_LINES); // inner circle 

	for (angle = 0.0f; angle <= 360.0f; angle = angle + 0.10f)
	{
		float r = 0.08f;
		angleRadian = angle * (SSH_PI / 180.0f);
		x = cos(angleRadian) * r;
		y = sin(angleRadian) * r;

		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.4f, -0.2f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(x + (-0.4f), y + (-0.2f), 0.0f);
	}

	glEnd();


	//Front wheels

	glBegin(GL_LINES); // outer circle

	for (angle = 0.0f; angle <= 360.0f; angle = angle + 0.10f)
	{
		float r = 0.1f;
		angleRadian = angle * (SSH_PI / 180.0f);
		x = cos(angleRadian) * r;
		y = sin(angleRadian) * r;

		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.4f, -0.2f, 0.0f);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(x + (0.4f), y + (-0.2f), 0.0f);
	}

	glEnd();


	glBegin(GL_LINES); // Inner Circle

	for (angle = 0.0f; angle <= 360.0f; angle = angle + 0.10f)
	{
		float r = 0.08f;
		angleRadian = angle * (SSH_PI / 180.0f);
		x = cos(angleRadian) * r;
		y = sin(angleRadian) * r;

		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.4f, -0.2f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(x + (0.4f), y + (-0.2f), 0.0f);
	}

	glEnd();


	glLineWidth(2.0f);
	glBegin(GL_LINE_LOOP);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.01f, 0.0f, 0.0f);
	glVertex3f(-0.01f, 0.16f, 0.0f);
	glVertex3f(-0.25f, 0.16f, 0.0f);
	glVertex3f(-0.32f, -0.01f, 0.0f);
	glVertex3f(-0.01f, -0.01f, 0.0f);

	glEnd();

	glLineWidth(2.0f);
	glBegin(GL_LINE_LOOP);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.01f, 0.0f, 0.0f);
	glVertex3f(0.01f, 0.16f, 0.0f);
	glVertex3f(0.25f, 0.16f, 0.0f);
	glVertex3f(0.32f, -0.01f, 0.0f);
	glVertex3f(0.01f, -0.01f, 0.0f);

	glEnd();

	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.18f, 0.0f);
	glVertex3f(0.0f, -0.19f, 0.0f);

	glEnd();

	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.25f, 0.18f, 0.0f);
	glVertex3f(0.25f, 0.18f, 0.0f);

	glEnd();

	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.27f, -0.19f, 0.0f);
	glVertex3f(0.27f, -0.19f, 0.0f);

	glEnd();

	glBegin(GL_LINE_LOOP); // back Door handle 
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.01f, -0.05f, 0.0f);
	glVertex3f(-0.01f, -0.03f, 0.0f);
	glVertex3f(-0.03f, -0.04f, 0.0f);

	glEnd();

	glBegin(GL_LINE_LOOP); // Front Door handle 
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.01f, -0.05f, 0.0f);
	glVertex3f(0.01f, -0.03f, 0.0f);
	glVertex3f(0.03f, -0.04f, 0.0f);

	glEnd();
}

void DrawRoad2(void)
{
	//main road
	glColor3f(52.0f / 255.0f, 52.0f / 255.0f, 52.0f / 255.0f);
	glBegin(GL_QUADS);
	{
		glVertex3f(1.0f, -0.7f, 0.0f);
		glVertex3f(-1.0f, -0.7f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 0.0f);
		glVertex3f(1.0f, -1.0f, 0.0f);
	}
	glEnd();

	//road strips
	glColor3f(1.0f, 1.0f, 1.0f);
	glBegin(GL_QUADS);
	{
		glVertex3f(-1.0f, -0.84f, 0.0f);
		glVertex3f(-1.0f, -0.86f, 0.0f);
		glVertex3f(-0.8f, -0.86f, 0.0f);
		glVertex3f(-0.8f, -0.84f, 0.0f);
	}
	glEnd();

	glBegin(GL_QUADS);
	{
		glVertex3f(-0.4f, -0.84f, 0.0f);
		glVertex3f(-0.4f, -0.86f, 0.0f);
		glVertex3f(0.0f, -0.86f, 0.0f);
		glVertex3f(0.0f, -0.84f, 0.0f);
	}
	glEnd();

	glBegin(GL_QUADS);
	{
		glVertex3f(0.4f, -0.84f, 0.0f);
		glVertex3f(0.4f, -0.86f, 0.0f);
		glVertex3f(0.8f, -0.86f, 0.0f);
		glVertex3f(0.8f, -0.84f, 0.0f);
	}
	glEnd();


}

void DrawFootPath2(void)
{
	//footpath
	glColor3f(128.0f / 255.0f, 128.0f / 255.0f, 128.0f / 255.0f);
	glBegin(GL_QUADS);
	{
		glVertex3f(1.0f, -0.7f, 0.0f);
		glVertex3f(1.0f, -0.6f, 0.0f);
		glVertex3f(-1.0f, -0.6f, 0.0f);
		glVertex3f(-1.0f, -0.7f, 0.0f);

	}
	glEnd();

}

void DrawSky2(void)
{

	glBegin(GL_POLYGON);
	{

		glColor3f(77.0f / 255.0f, 96.0f / 255.0f, 167.0f / 255.0f);
		glVertex3f(1.0f, -1.0f, 0.0f);

		glColor3f(44.0f / 255.0f, 76.0f / 255.0f, 144.0f / 255.0f);
		glVertex3f(1.0f, -0.3f, 0.0f);

		glColor3f(8.0f / 255.0f, 10.0f / 255.0f, 116.0f / 255.0f);
		glVertex3f(1.0f, 0.3f, 0.0f);

		//glColor3f(6.0f / 255.0f, 7.0f / 255.0f, 45.0f / 255.0f);
		//glColor3f(7.0f / 255.0f, 8.0f / 255.0f, 100.0f / 255.0f);
		glColor3f(0.0f, 0.0f, 0.1f);
		glVertex3f(1.0f, 1.0f, 0.0f);
		glVertex3f(-1.0f, 1.0f, 0.0f);

		glColor3f(8.0f / 255.0f, 10.0f / 255.0f, 116.0f / 255.0f);
		glVertex3f(-1.0f, 0.3f, 0.0f);

		glColor3f(44.0f / 255.0f, 76.0f / 255.0f, 144.0f / 255.0f);
		glVertex3f(-1.0f, -0.3f, 0.0f);

		glColor3f(77.0f / 255.0f, 96.0f / 255.0f, 167.0f / 255.0f);
		glVertex3f(-1.0f, -1.0f, 0.0f);

	}
	glEnd();
}

void DrawCar2(void)
{

}

void DrawStreetLamp2(void)
{
	glColor3f(0.1f, 0.1f, 0.1f);

	//base1
	glBegin(GL_QUADS);
	{
		glVertex3f(0.1f, -0.63f, 0.0f);
		glVertex3f(-0.1f, -0.63f, 0.0f);
		glVertex3f(-0.1f, -0.65f, 0.0f);
		glVertex3f(0.1f, -0.65f, 0.0f);

	}
	glEnd();

	//base2
	glBegin(GL_QUADS);
	{
		glVertex3f(0.07f, -0.61f, 0.0f);
		glVertex3f(-0.07f, -0.61f, 0.0f);
		glVertex3f(-0.07f, -0.63f, 0.0f);
		glVertex3f(0.07f, -0.63f, 0.0f);

	}
	glEnd();

	//base3
	glBegin(GL_QUADS);
	{
		glVertex3f(0.02f, -0.40f, 0.0f);
		glVertex3f(-0.02f, -0.40f, 0.0f);
		glVertex3f(-0.05f, -0.61f, 0.0f);
		glVertex3f(0.05f, -0.61f, 0.0f);

	}
	glEnd();

	//pole
	glBegin(GL_QUADS);
	{
		glVertex3f(0.01f, 0.60f, 0.0f);
		glVertex3f(-0.01f, 0.60f, 0.0f);
		glVertex3f(-0.01f, -0.40f, 0.0f);
		glVertex3f(0.01f, -0.40f, 0.0f);

	}
	glEnd();

	//lamp
	glBegin(GL_QUADS);
	{
		glVertex3f(0.05f, 0.61f, 0.0f);
		glVertex3f(-0.05f, 0.61f, 0.0f);
		glVertex3f(-0.05f, 0.60f, 0.0f);
		glVertex3f(0.05f, 0.60f, 0.0f);

	}
	glEnd();

	glBegin(GL_QUADS);
	{
		glVertex3f(0.10f, 0.80f, 0.0f);
		glVertex3f(0.05f, 0.61f, 0.0f);
		glVertex3f(0.05f, 0.60f, 0.0f);
		glVertex3f(0.10f, 0.79f, 0.0f);

	}
	glEnd();

	glBegin(GL_QUADS);
	{
		glVertex3f(0.10f, 0.81f, 0.0f);
		glVertex3f(-0.10f, 0.81f, 0.0f);
		glVertex3f(-0.10f, 0.80f, 0.0f);
		glVertex3f(0.10f, 0.80f, 0.0f);

	}
	glEnd();

	glBegin(GL_QUADS);
	{
		glVertex3f(-0.10f, 0.80f, 0.0f);
		glVertex3f(-0.10f, 0.79f, 0.0f);
		glVertex3f(-0.05f, 0.60f, 0.0f);
		glVertex3f(-0.05f, 0.61f, 0.0f);

	}
	glEnd();


	//light shade
	glBegin(GL_QUADS);
	{
		glColor3f(255.0f / 255.0f, 255.0f / 255.0f, 153.0f / 255.0f);
		glVertex3f(0.10f, 0.8f, 0.0f);
		glVertex3f(-0.10f, 0.8f, 0.0f);
		glVertex3f(-0.05f, 0.61f, 0.0f);
		glVertex3f(0.05f, 0.61f, 0.0f);

	}
	glEnd();

	glColor3f(0.1f, 0.1f, 0.1f);
	glBegin(GL_QUADS);
	{
		glVertex3f(-0.10f, 0.80f, 0.0f);
		glVertex3f(-0.10f, 0.79f, 0.0f);
		glVertex3f(-0.05f, 0.60f, 0.0f);
		glVertex3f(-0.05f, 0.61f, 0.0f);

	}
	glEnd();


	glBegin(GL_QUADS);
	{
		glVertex3f(0.005f, 0.80f, 0.0f);
		glVertex3f(0.0f, 0.80f, 0.0f);
		glVertex3f(0.0f, 0.61f, 0.0f);
		glVertex3f(0.005f, 0.61f, 0.0f);

	}
	glEnd();

	glBegin(GL_QUADS);
	{
		glVertex3f(0.1f, 0.83f, 0.0f);
		glVertex3f(-0.1f, 0.83f, 0.0f);
		glVertex3f(-0.1f, 0.81f, 0.0f);
		glVertex3f(0.1f, 0.81f, 0.0f);

	}
	glEnd();

	glBegin(GL_QUADS);
	{
		glVertex3f(0.07f, 0.84f, 0.0f);
		glVertex3f(-0.07f, 0.84f, 0.0f);
		glVertex3f(-0.07f, 0.83f, 0.0f);
		glVertex3f(0.07f, 0.83f, 0.0f);

	}
	glEnd();

	glBegin(GL_QUADS);
	{
		glVertex3f(0.07f, 0.84f, 0.0f);
		glVertex3f(0.04f, 0.86f, 0.0f);
		glVertex3f(-0.04f, 0.86f, 0.0f);
		glVertex3f(-0.07f, 0.84f, 0.0f);

	}
	glEnd();

}

void DrawBuilding1(void)
{
	//left building
	glBegin(GL_QUADS);
	{
		glColor3f(0.2f, 0.2f, 0.5f);
		glVertex3f(0.0f, -0.6f, 0.0f);
		glVertex3f(0.0f, 0.8f, 0.0f);
		glVertex3f(-0.4f, 0.8f, 0.0f);
		glVertex3f(-0.4f, -0.6f, 0.0f);
	}
	glEnd();

	float xOffsetf;
	for (xOffsetf = -0.225f; xOffsetf < 0.0f; xOffsetf += 0.165f)
	{
		glColor3f(255.0f / 255.0f, 255.0f / 255.0f, 153.0f / 255.0f);
		glBegin(GL_QUADS);
		{
			//1st floor

			glVertex3f(xOffsetf, -0.2f, 0.0f);
			xOffsetf -= 0.125f;
			glVertex3f(xOffsetf, -0.2f, 0.0f);
			glVertex3f(xOffsetf, -0.05f, 0.0f);
			xOffsetf += 0.125f;
			glVertex3f(xOffsetf, -0.05f, 0.0f);

			//2nd floor
			glVertex3f(xOffsetf, 0.2f, 0.0f);
			xOffsetf -= 0.125f;
			glVertex3f(xOffsetf, 0.2f, 0.0f);
			glVertex3f(xOffsetf, 0.05f, 0.0f);
			xOffsetf += 0.125f;
			glVertex3f(xOffsetf, 0.05f, 0.0f);


		}
		glEnd();
	}

	//left building hexagon
	glBegin(GL_POLYGON);
	{
		glColor3f(0.36078f, 0.91764f, 0.9980f); //may be mirror
		glVertex3f(-0.2f, 0.75f, 0.0f);
		glVertex3f(-0.3f, 0.65f, 0.0f);
		glVertex3f(-0.3f, 0.55f, 0.0f);
		glVertex3f(-0.2f, 0.45f, 0.0f);
		glVertex3f(-0.1f, 0.55f, 0.0f);
		glVertex3f(-0.1f, 0.65f, 0.0f);
	}
	glEnd();



	//left building car gate
	glBegin(GL_QUADS);
	{
		glColor3f(0.1f, 0.1f, 0.1f); //dark blue
		glVertex3f(-0.05f, -0.6f, 0.0f);
		glVertex3f(-0.05f, -0.4f, 0.0f);
		glVertex3f(-0.35f, -0.4f, 0.0f);
		glVertex3f(-0.35f, -0.6f, 0.0f);
	}
	glEnd();

}

void DrawBuilding2(void)
{
	// Buildings
	glBegin(GL_QUADS);
	glColor3f(0.4f, 0.3f, 0.4f);
	glVertex3f(0.4f, 0.9f, 0.0f);
	glVertex3f(-0.2f, 0.9f, 0.0f);
	glVertex3f(-0.2f, -0.7f, 0.0f);
	glVertex3f(0.4f, -0.7f, 0.0f);

	glVertex3f(-0.2f, 0.5f, 0.0f);
	glVertex3f(-0.7f, 0.5f, 0.0f);
	glVertex3f(-0.7f, -0.7f, 0.0f);
	glVertex3f(-0.2f, -0.7f, 0.0f);

	glEnd();

	// Windows And doors
	glBegin(GL_QUADS);
	// 1
	glColor3f(1.0f, 1.0f, 0.2f);
	glVertex3f(0.3f, 0.8f, 0.0f);
	glVertex3f(0.2f, 0.8f, 0.0f);
	glVertex3f(0.2f, 0.6f, 0.0f);
	glVertex3f(0.3f, 0.6f, 0.0f);

	// 2
	glVertex3f(0.3f, 0.5f, 0.0f);
	glVertex3f(0.2f, 0.5f, 0.0f);
	glVertex3f(0.2f, 0.3f, 0.0f);
	glVertex3f(0.3f, 0.3f, 0.0f);

	// 3
	glVertex3f(0.3f, 0.2f, 0.0f);
	glVertex3f(0.2f, 0.2f, 0.0f);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glVertex3f(0.3f, 0.0f, 0.0f);

	// 4
	glVertex3f(0.3f, -0.1f, 0.0f);
	glVertex3f(0.2f, -0.1f, 0.0f);
	glVertex3f(0.2f, -0.3f, 0.0f);
	glVertex3f(0.3f, -0.3f, 0.0f);

	// Left side
	// 1
	glVertex3f(0.0f, 0.8f, 0.0f);
	glVertex3f(-0.1f, 0.8f, 0.0f);
	glVertex3f(-0.1f, 0.6f, 0.0f);
	glVertex3f(0.0f, 0.6f, 0.0f);

	// 2
	glVertex3f(0.0f, 0.5f, 0.0f);
	glVertex3f(-0.1f, 0.5f, 0.0f);
	glVertex3f(-0.1f, 0.3f, 0.0f);
	glVertex3f(0.0f, 0.3f, 0.0f);

	// 3
	glVertex3f(0.0f, 0.2f, 0.0f);
	glVertex3f(-0.1f, 0.2f, 0.0f);
	glVertex3f(-0.1f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);

	// 4
	glVertex3f(0.0f, -0.1f, 0.0f);
	glVertex3f(-0.1f, -0.1f, 0.0f);
	glVertex3f(-0.1f, -0.3f, 0.0f);
	glVertex3f(0.0f, -0.3f, 0.0f);

	// Second Building windows
	// Right side
	glVertex3f(-0.3, 0.1f, 0.0f);
	glVertex3f(-0.4, 0.1f, 0.0f);
	glVertex3f(-0.4, 0.0f, 0.0f);
	glVertex3f(-0.3, 0.0f, 0.0f);

	// Left side
	glVertex3f(-0.5, 0.1f, 0.0f);
	glVertex3f(-0.6, 0.1f, 0.0f);
	glVertex3f(-0.6, 0.0f, 0.0f);
	glVertex3f(-0.5, 0.0f, 0.0f);

	// Middle window
	glVertex3f(-0.35, -0.2f, 0.0f);
	glVertex3f(-0.55, -0.2f, 0.0f);
	glVertex3f(-0.55, -0.3f, 0.0f);
	glVertex3f(-0.35, -0.3f, 0.0f);
	glEnd();

	// Upper Window
	glBegin(GL_POLYGON);
	glColor3f(0.2f, 1.0f, 1.0f);

	glVertex3f(-0.45f, 0.4f, 0.0f);
	glVertex3f(-0.52f, 0.33f, 0.0f);
	glVertex3f(-0.52f, 0.21f, 0.0f);
	glVertex3f(-0.45f, 0.15f, 0.0f);
	glVertex3f(-0.38f, 0.21f, 0.0f);
	glVertex3f(-0.38f, 0.33f, 0.0f);
	glVertex3f(-0.45f, 0.4f, 0.0f);
	glEnd();

	// Doors
	glBegin(GL_QUADS);
	glColor3f(0.2f, 0.2f, 0.2f);
	glVertex3f(0.3f, -0.5f, 0.0f);
	glVertex3f(0.2f, -0.5f, 0.0f);
	glVertex3f(0.2f, -0.7f, 0.0f);
	glVertex3f(0.3f, -0.7f, 0.0f);

	glVertex3f(-0.25, -0.5f, 0.0f);
	glVertex3f(-0.65, -0.5f, 0.0f);
	glVertex3f(-0.65, -0.7f, 0.0f);
	glVertex3f(-0.25, -0.7f, 0.0f);
	glEnd();
}

void scene3(void)
{
	// prototype
	void backgroundScene_3(void);
	void girlCharacter(void);

	backgroundScene_3();

	glTranslatef(sGirlPosition.x, sGirlPositiontranslate.y, sGirlPositiontranslate.z);
	glScalef(0.2f, 0.25f, 0.0f);
	girlCharacter();
	glLoadIdentity();

	glTranslatef(sLinePosition2.x, sLinePosition2.y, sLinePosition2.z);
	//glScalef(0.50f, 0.75f, 0.25f);
	glLineWidth(2.0f);
	glBegin(GL_LINES);
	{
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.00000001f, 0.0f, 0.0f);
	}
	glEnd();

}



void scene4(void) {

	// prototype


	void stage(void);
	void characterPrasad(void);
	void characterParas(void);
	void characterVinit(void);
	void characterSanket(void);
	void DrawMicStand(void);
	void DrawMic(void);
	void DrawPiano(void);
	void musicSymbol(void);
	void scene_4_GuitarGuy(void);
	void scene_4_DrumSet(void);
	void happyBirthday(void);
	void drawBalloon(float R, float G, float B);
	stage();

	/*glLoadIdentity();
	glTranslatef(0.6f, -0.2f, 0.0f);
	glScalef(0.1f, 0.1f, 0.0f);*/

	glLoadIdentity();
	happyBirthday();

	glLoadIdentity();
	glTranslatef(0.4f, -0.2f, 0.0f);
	glScalef(0.35f, 0.33f, 0.5f);
	scene_4_DrumSet();

	glLoadIdentity();
	glTranslatef(0.7f, -0.5f, 0.0f);
	glScalef(0.2f, 0.3f, 0.3f);
	characterParas();

	glLoadIdentity();

	scene_4_GuitarGuy();

	glLoadIdentity();
	glTranslatef(-0.5f, -0.3f, 0.0f);
	glScalef(0.2f, 0.3f, 0.5f);
	characterVinit();
	glLoadIdentity();


	glTranslatef(-0.5f, -0.33f, 0.0f);
	glScalef(0.6f, 0.7f, 0.0f);
	DrawPiano();

	glLoadIdentity();
	glTranslatef(0.0f, -0.4f, 0.0f);
	glScalef(0.4f, 0.4f, 0.0f);
	DrawMicStand();

	glLoadIdentity();
	glTranslatef(-0.08f, -0.495f, 0.0f);
	glScalef(0.8f, 0.8f, 0.0f);
	glRotatef(340.0f, 0.0f, 0.0f, 1.0f);
	DrawMic();



	glLoadIdentity();

	glTranslatef(-0.75f, -0.45f, 0.0f);
	glScalef(0.35f, 0.33f, 0.5f);
	characterSanket();
	glLoadIdentity();

	glLoadIdentity();
	glTranslatef(sLinePosition1.x, sLinePosition1.y, sLinePosition1.z);
	//glScalef(0.50f, 0.75f, 0.25f);
	glLineWidth(2.0f);
	glBegin(GL_LINES);
	{
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.0000000001f, 0.0f, 0.0f);
	}
	glEnd();

	glLoadIdentity();
}

void scene5(void)
{
	void rightpipaniZanj(void);
	void leftpipaniZanj(void);
	//void endCredits(void);
	glLoadIdentity();


	rightpipaniZanj();

	//glTranslatef(sDanceCharacterPositionLeft.x, sDanceCharacterPositionLeft.y, sDanceCharacterPositionLeft.z);

	leftpipaniZanj();

	glTranslatef(sLinePosition3.x, sLinePosition3.y, sLinePosition3.z);
	//glScalef(0.50f, 0.75f, 0.25f);
	glLineWidth(2.0f);
	glBegin(GL_LINES);
	{
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.0000000001f, 0.0f, 0.0f);
	}
	glEnd();

	//endCredits();

	//glTranslatef(sCurtianPositionLeft.x, sCurtianPositionLeft.y, sCurtianPositionLeft.z);
	//glBegin(GL_QUADS);
	//{
	//	glColor3f(0.0f, 0.0f, 0.0f);
	//	glVertex3f(0.0f, 1.0f, 0.0f);

	//	glVertex3f(-1.0f, 1.0f, 0.0f);

	//	glVertex3f(-1.0f, -1.0f, 0.0f);

	//	glVertex3f(0.0f, -1.0f, 0.0f);
	//}

	//glLoadIdentity();
	//glTranslatef(sCurtianPositionRight.x, sCurtianPositionRight.y, sCurtianPositionRight.z);
	//glBegin(GL_QUADS);
	//{
	//	glColor3f(0.0f, 0.0f, 0.0f);
	//	glVertex3f(1.0f, 1.0f, 0.0f);

	//	glVertex3f(0.0f, 1.0f, 0.0f);

	//	glVertex3f(0.0f, -1.0f, 0.0f);

	//	glVertex3f(1.0f, -1.0f, 0.0f);
	//}
	//glEnd();

}

void balloonString()
{
	glBegin(GL_LINE_STRIP);

	glColor3f(1.0f, 1.0f, 1.0f);

	glVertex3f(0.1f, 0.2f, 0.0f);
	glVertex3f(0.07f, 0.15f, 0.0f);
	glVertex3f(0.07f, 0.14f, 0.0f);
	glVertex3f(0.08f, 0.08f, 0.0f);
	glVertex3f(0.09f, 0.06f, 0.0f);
	glVertex3f(0.095f, 0.04f, 0.0f);
	glVertex3f(0.1f, 0.0f, 0.0f);
	glVertex3f(0.1f, -0.06f, 0.0f);
	glVertex3f(0.1f, -0.1f, 0.0f);
	glVertex3f(0.07f, -0.15f, 0.0f);
	glVertex3f(0.05f, -0.18f, 0.0f);

	glEnd();
}

void circle(float R, float G, float B)
{
	float fAngle = 0.0f;
	float x = 0.0f, y = 0.0f;
	const float radius = 0.5f;
	float angleInRadians = 0.0f;
	// 135, 206, 235
	glBegin(GL_LINE_LOOP);

	for (fAngle = 0.0f; fAngle < 360.0f; fAngle = fAngle + 0.2f)
	{
		angleInRadians = fAngle * (PAB_PI / 180.0f);

		x = cos(angleInRadians) * radius;
		y = sin(angleInRadians) * radius;

		//glColor3f(135.0f / 255.0f, 206.0f / 255.0f, 235.0f / 255.0f);
		glColor3f(R / 255.0f, G / 255.0f, B / 255.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(x, y, 0.0f);

	}

	glEnd();
}

void drawBalloon(float R, float G, float B)
{
	glPushMatrix();
	{

		glPushMatrix();
		{
			//glScalef(0.2f, 0.2f, 1.0f);

			glScalef(0.8f, 1.0f, 1.0f);
			circle(R, G, B);
		}
		glPopMatrix();

		glPushMatrix();
		{
			glScalef(1.0f, 2.0f, 1.0f);

			glTranslatef(-0.075f, -0.42f, 0.0f);
			balloonString();
		}
		glPopMatrix();
	}
	glPopMatrix();


}


void DrawPiano(void)
{
	//piano base
	glColor3f(0.1f, 0.1f, 0.1f);
	glBegin(GL_QUADS);
	{
		glVertex3f(0.2f, 0.0f, 0.0f);
		glVertex3f(-0.2f, 0.0f, 0.0f);
		glVertex3f(-0.2f, -0.2f, 0.0f);
		glVertex3f(0.2f, -0.2f, 0.0f);
	}
	glEnd();

	glBegin(GL_QUADS);
	{
		glVertex3f(0.19f, 0.0f, 0.0f);
		glVertex3f(-0.15f, 0.0f, 0.0f);
		glVertex3f(-0.15f, 0.1f, 0.0f);
		glVertex3f(0.19f, 0.1f, 0.0f);
	}
	glEnd();

	//piano top 
	glBegin(GL_QUADS);
	{
		glVertex3f(0.185f, 0.1f, 0.0f);
		glVertex3f(-0.1f, 0.3f, 0.0f);
		glVertex3f(-0.11f, 0.25f, 0.0f);
		glVertex3f(0.1f, 0.1f, 0.0f);
	}
	glEnd();

	//top stand
	glBegin(GL_QUADS);
	{
		glVertex3f(-0.07f, 0.23f, 0.0f);
		glVertex3f(-0.1f, 0.30f, 0.0f);
		glVertex3f(0.0f, 0.1f, 0.0f);
		glVertex3f(0.02f, 0.1f, 0.0f);
	}
	glEnd();

	//piano wheels 
	float fAngle = 0.0f;
	float fRadx = 0.02f;
	float fRady = 0.03f;
	float fX = 0, fY = 0;

	//right
	glBegin(GL_LINE_LOOP);
	for (fAngle = 0.0f; fAngle <= 360.0f; fAngle += 0.1f)
	{
		fX = cos(RAD(fAngle)) * fRadx;
		fY = sin(RAD(fAngle)) * fRady;


		glVertex3f(0.15f, -0.2f, 0.0f);
		glVertex3f(fX + 0.15f, fY - 0.2f, 0.0f);

	}
	glEnd();

	//left
	glBegin(GL_LINE_LOOP);
	for (fAngle = 0.0f; fAngle <= 360.0f; fAngle += 0.1f)
	{
		fX = cos(RAD(fAngle)) * fRadx;
		fY = sin(RAD(fAngle)) * fRady;


		glVertex3f(-0.15f, -0.2f, 0.0f);
		glVertex3f(fX - 0.15f, fY - 0.2f, 0.0f);

	}
	glEnd();
}

void DrawMicStand(void)
{
	//Mic stand base 
	float fAngle = 0.0f;
	float fRadx = 0.1f;
	float fRady = 0.07f;
	float fX = 0, fY = 0;

	//bottom
	glColor3f(0.5f, 0.5f, 0.5f);
	glBegin(GL_LINE_LOOP);
	for (fAngle = 0.0f; fAngle <= 360.0f; fAngle += 0.1f)
	{
		fX = cos(RAD(fAngle)) * fRadx;
		fY = sin(RAD(fAngle)) * fRady;


		glVertex3f(0.0f, -0.8f, 0.0f);
		glVertex3f(fX, fY - 0.8f, 0.0f);

	}
	glEnd();

	//mic rod
	glColor3f(0.2f, 0.2f, 0.2f);
	glBegin(GL_QUADS);
	{
		glVertex3f(0.01f, 0.2f, 0.0f);
		glVertex3f(-0.01f, 0.2f, 0.0f);
		glVertex3f(-0.01f, -0.8f, 0.0f);
		glVertex3f(0.01f, -0.8f, 0.0f);
	}
	glEnd();

	//mic stand
	glColor3f(0.6f, 0.6f, 0.6f);
	glBegin(GL_QUADS);
	{
		glVertex3f(0.016f, 0.25f, 0.0f);
		glVertex3f(-0.016f, 0.25f, 0.0f);
		glVertex3f(-0.012f, 0.2f, 0.0f);
		glVertex3f(0.012f, 0.2f, 0.0f);
	}
	glEnd();
}

void DrawMic(void)
{
	glColor3f(0.1f, 0.1f, 0.1f);

	glBegin(GL_QUADS);
	{
		glVertex3f(0.01f, 0.30f, 0.0f);
		glVertex3f(-0.01f, 0.30f, 0.0f);
		glVertex3f(-0.01f, 0.2f, 0.0f);
		glVertex3f(0.01f, 0.2f, 0.0f);
	}
	glEnd();

	//mic
	float fAngle = 0.0f;
	float fRadx = 0.013f;
	float fRady = 0.020f;
	float fX = 0, fY = 0;

	//mic
	glBegin(GL_LINE_LOOP);
	for (fAngle = 0.0f; fAngle <= 360.0f; fAngle += 0.1f)
	{
		fX = cos(RAD(fAngle)) * fRadx;
		fY = sin(RAD(fAngle)) * fRady;


		glVertex3f(0.0f, 0.30f, 0.0f);
		glVertex3f(fX, fY + 0.30f, 0.0f);
	}
	glEnd();

	//mic shades
	glColor3f(0.5f, 0.5f, 0.5f);
	glLineWidth(2.0f);
	glBegin(GL_LINES);
	{
		glVertex3f(0.013f, 0.30f, 0.0f);
		glVertex3f(-0.013f, 0.30f, 0.0f);
	}
	glEnd();

	glLineWidth(2.2f);
	glBegin(GL_LINES);
	{
		glVertex3f(0.013f, 0.28f, 0.0f);
		glVertex3f(-0.013f, 0.28f, 0.0f);
	}
	glEnd();

	glLineWidth(2.5f);
	glBegin(GL_LINES);
	{
		glVertex3f(0.013f, 0.2f, 0.0f);
		glVertex3f(-0.013f, 0.2f, 0.0f);
	}
	glEnd();

}


void musicSymbol(void)
{
	void musicSymbol1(void);
	void musicSymbol2(void);
	
	/*glLoadIdentity();
	glScalef(0.2f, 0.2f, 0.0f);*/
	musicSymbol1();

	glLoadIdentity();
	glTranslatef(-0.0f, -0.43f, 0.0f);
	glScalef(0.5f, 0.40f, 0.0f);
	musicSymbol2();

}



void stage(void)
{

	glBegin(GL_POLYGON);

	//glColor3f(136.0f / 255.0f, 8.0f / 255.0f, 8.0f / 255.0f);

	//glColor3f(212.0f / 255.0f, 0.0f / 255.0f, 0.0f / 255.0f);

	/*glColor3f(136.0f / 255.0f, 8.0f / 255.0f, 8.0f / 255.0f);
	glVertex3f(1.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, 0.0f);*/

	glEnd();



	glBegin(GL_POLYGON);

	//	rgb(204, 119, 34)
	glColor3f(204.0f / 255.0f, 119.0f / 255.0f, 34.0f / 255.0f);
	//glColor3f(139.0f / 255.0f, 69.0f / 255.0f, 19.0f / 255.0f);


	glVertex3f(0.95f, -0.8f, 0.0f);
	glVertex3f(-0.95f, -0.8f, 0.0f);
	glVertex3f(-0.95f, -0.9f, 0.0f);
	glVertex3f(0.95f, -0.9f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	//rgb(139, 69, 19)

	//glColor3f(204.0f / 255.0f, 119.0f / 255.0f, 34.0f / 255.0f);
	glColor3f(139.0f / 255.0f, 69.0f / 255.0f, 19.0f / 255.0f);

	glVertex3f(0.7f, -0.4f, 0.0f);
	glVertex3f(-0.7f, -0.4f, 0.0f);
	glVertex3f(-0.95f, -0.8f, 0.0f);
	glVertex3f(0.95f, -0.8f, 0.0f);

	glColor3f(136.0f / 255.0f, 8.0f / 255.0f, 8.0f / 255.0f);
	glVertex3f(0.7f, 0.8f, 0.0f);
	glVertex3f(-0.7f, 0.8f, 0.0f);
	glVertex3f(-0.7f, -0.4f, 0.0f);
	glVertex3f(0.7f, -0.4f, 0.0f);

	glEnd();

}

void rightpipaniZanj(void)
{

	void Bai(void);
	void Pipani(void);
	void Zanj(void);
	void rightHandOfZanj(void);
	void leftHandOfZanj(void);

	//glLoadIdentity();
	//Bai();



	glLoadIdentity();
	glScalef(0.6f, 0.6f, 0.0f);
	glTranslatef(0.75f, 0.2f, 0.0f);
	Pipani();

	// Right side zanjwala
	// Hands
	//Left
	glLoadIdentity();
	glTranslatef(0.8f, -0.1f, 0.0f);
	glScalef(0.8f, 0.8f, 0.0f);
	leftHandOfZanj();
	// Right
	glLoadIdentity();
	glTranslatef(0.8f, -0.1f, 0.0f);
	glScalef(0.8f, 0.8f, 0.0f);
	rightHandOfZanj();

	glLoadIdentity();
	glTranslatef(0.8f, -0.18f, 0.0f);
	glScalef(0.8f, 0.8f, 0.0f);
	Zanj();

}

void leftpipaniZanj(void)
{
	void Pipani(void);
	void Zanj(void);
	void rightHandOfZanj(void);
	void leftHandOfZanj(void);

	glLoadIdentity();
	glScalef(0.6f, 0.6f, 0.0f);
	glTranslatef(-0.75f, 0.2f, 0.0f);
	Pipani();

	// left side zanjwala
// Hands
//Left
	glLoadIdentity();
	glTranslatef(-0.8f, -0.1f, 0.0f);
	glScalef(0.8f, 0.8f, 0.0f);
	leftHandOfZanj();
	// Right
	glLoadIdentity();
	glTranslatef(-0.8f, -0.1f, 0.0f);
	glScalef(0.8f, 0.8f, 0.0f);
	rightHandOfZanj();

	glLoadIdentity();
	glTranslatef(-0.8f, -0.18f, 0.0f);
	glScalef(0.8f, 0.8f, 0.0f);
	Zanj();
	glLoadIdentity();
}

void girlCharacter(void)
{
	float x = 0.0f;
	float y = 0.0f;






	glBegin(GL_QUADS);
	//Body chest 
	glColor3f(0.1f, 0.5f, 0.5f);
	glVertex3f(0.25f, 0.43f, 0.0f);
	glColor3f(0.1f, 0.5f, 0.5f);
	glVertex3f(-0.25f, 0.43f, 0.0f);
	glColor3f(0.1f, 0.5f, 0.5f);
	glVertex3f(-0.25f, -0.15f, 0.0f);
	glColor3f(0.1f, 0.5f, 0.5f);
	glVertex3f(0.25f, -0.15f, 0.0f);

	glEnd();




	glBegin(GL_QUADS);
	//sholder 1 

	glColor3f(0.1f, 0.5f, 0.5f);
	glVertex3f(-0.25f, 0.43f, 0.0f);
	glColor3f(0.1f, 0.5f, 0.5f);
	glVertex3f(-0.37f, 0.2f, 0.0f);
	glColor3f(0.1f, 0.5f, 0.5f);
	glVertex3f(-0.27f, 0.2f, 0.0f);
	glColor3f(0.1f, 0.5f, 0.5f);
	glVertex3f(-0.25f, 0.32f, 0.0f);

	//Hand 1
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(-0.28f, 0.2f, 0.0f);
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(-0.36f, 0.2f, 0.0f);
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(-0.33f, -0.05f, 0.0f);
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(-0.28f, -0.05f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);
	//sholder 2

	glColor3f(0.1f, 0.5f, 0.5f);
	glVertex3f(0.25f, 0.43f, 0.0f);
	glColor3f(0.1f, 0.5f, 0.5f);
	glVertex3f(0.37f, 0.2f, 0.0f);
	glColor3f(0.1f, 0.5f, 0.5f);
	glVertex3f(0.27f, 0.2f, 0.0f);
	glColor3f(0.1f, 0.5f, 0.5f);
	glVertex3f(0.25f, 0.32f, 0.0f);

	//Hand 1
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(0.28f, 0.2f, 0.0f);
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(0.36f, 0.2f, 0.0f);
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(0.33f, -0.05f, 0.0f);
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(0.28f, -0.05f, 0.0f);

	glEnd();

	//glBegin(GL_LINES);
	//{
	//	// shoulder cloth 1

	//	for (float angle = 0.0f; angle <= 180.0f; angle = angle + 0.1f)
	//	{

	//		x = cos(angle) * 0.13;
	//		y = sin(angle) / 1.5 * 0.13;

	//		glColor3f(0.1f, 0.5f, 0.5f);
	//		glVertex3f(-0.15f, 0.36f, 0.0f);

	//		glColor3f(0.1f, 0.5f, 0.5f);
	//		glVertex3f(x + (-0.15f), y + (0.36f), 0.0f);

	//	}
	//}
	//glEnd();

	//glBegin(GL_LINES);
	//{
	//	// shoulder cloth 2

	//	for (float angle = 0.0f; angle <= 180.0f; angle = angle + 0.1f)
	//	{

	//		x = cos(angle) * 0.13;
	//		y = sin(angle) / 1.5 * 0.13;

	//		glColor3f(0.1f, 0.5f, 0.5f);
	//		glVertex3f(0.16f, 0.36f, 0.0f);

	//		glColor3f(0.1f, 0.5f, 0.5f);
	//		glVertex3f(x + (0.16f), y + (0.36f), 0.0f);

	//	}
	//}
	//glEnd();


	//glLoadIdentity();
	//glTranslatef(0.0f, 0.0f, -3.0f);
	//glRotatef(girl_leg_angle, 1.0f, .0f, 0.0f);

	glBegin(GL_QUADS);

	//LEG 1

	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(-0.1f, -0.4f, 0.0f);
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(-0.24f, -0.4f, 0.0f);
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(-0.22f, -0.8f, 0.0f);
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(-0.12f, -0.8f, 0.0f);

	glEnd();


	//glLoadIdentity();
	//glTranslatef(0.0f, 0.0f, -3.0f);
	glBegin(GL_QUADS);

	//LEG 2

	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(0.1f, -0.4f, 0.0f);
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(0.24f, -0.4f, 0.0f);
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(0.22f, -0.8f, 0.0f);
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(0.12f, -0.8f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	//FROCK

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.245f, -0.13f, 0.0f);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-0.245f, -0.13f, 0.0f);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-0.29f, -0.5f, 0.0f);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.29f, -0.5f, 0.0f);

	glEnd();


	glBegin(GL_QUADS);

	//Shoes 1

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-0.115f, -0.8f, 0.0f);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-0.225f, -0.8f, 0.0f);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-0.22f, -0.9f, 0.0f);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-0.12f, -0.9f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	//Shoes 2

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.115f, -0.8f, 0.0f);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.225f, -0.8f, 0.0f);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.22f, -0.9f, 0.0f);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.12f, -0.9f, 0.0f);

	glEnd();

	glBegin(GL_LINES);
	{
		// Hair 1

		for (float angle = 0.0f; angle <= 180.0f; angle = angle + 0.1f)
		{

			x = cos(angle) * 0.16;
			y = sin(angle) * 0.16;

			glColor3f(0.0f, 0.0f, 0.0f);
			glVertex3f(0.0f, 0.60f, 0.0f);

			glColor3f(0.0f, 0.0f, 0.0f);
			glVertex3f(x + (0.0f), y + (0.60f), 0.0f);

		}
	}
	glEnd();

	glBegin(GL_LINES);
	{
		// Hair 2

		for (float angle = 0.0f; angle <= 180.0f; angle = angle + 0.1f)
		{

			x = cos(angle) * 0.1;
			y = sin(angle) * 0.1;

			glColor3f(0.0f, 0.0f, 0.0f);
			glVertex3f(0.0f, 0.47f, 0.0f);

			glColor3f(0.0f, 0.0f, 0.0f);
			glVertex3f(x + (0.0f), y + (0.47f), 0.0f);

		}
	}
	glEnd();

	glBegin(GL_LINES);
	{
		// Hair 3

		for (float angle = 0.0f; angle <= 180.0f; angle = angle + 0.1f)
		{

			x = cos(angle) * 0.07;
			y = sin(angle) * 0.07;

			glColor3f(0.0f, 0.0f, 0.0f);
			glVertex3f(0.0f, 0.4f, 0.0f);

			glColor3f(0.0f, 0.0f, 0.0f);
			glVertex3f(x + (0.0f), y + (0.4f), 0.0f);

		}
	}
	glEnd();

	glBegin(GL_LINES);
	{
		// Hair 4

		for (float angle = 0.0f; angle <= 180.0f; angle = angle + 0.1f)
		{

			x = cos(angle) * 0.05;
			y = sin(angle) * 0.05;

			glColor3f(0.0f, 0.0f, 0.0f);
			glVertex3f(0.0f, 0.34f, 0.0f);

			glColor3f(0.0f, 0.0f, 0.0f);
			glVertex3f(x + (0.0f), y + (0.34f), 0.0f);

		}
	}
	glEnd();

	glBegin(GL_LINES);
	{
		// Hair 5

		for (float angle = 0.0f; angle <= 180.0f; angle = angle + 0.1f)
		{

			x = cos(angle) * 0.03;
			y = sin(angle) * 0.03;

			glColor3f(0.0f, 0.0f, 0.0f);
			glVertex3f(0.0f, 0.29f, 0.0f);

			glColor3f(0.0f, 0.0f, 0.0f);
			glVertex3f(x + (0.0f), y + (0.29f), 0.0f);

		}
	}
	glEnd();

}

void characterPrasad(void)
{
	// Circle
	float x = 0.0f;
	float y = 0.0f;

	float angle_rad = 0.0f;

	//glRotatef(rAngle, 1.0f, 0.0f, 0.0f);



	glBegin(GL_LINES);

	// Face
	for (float angle = 0.0f; angle <= 360.0f; angle = angle + 0.001f)
	{


		x = cos(angle) / 1.6 * 0.2;
		y = sin(angle) / 1.25 * 0.2;

		glColor3f(1.0f, 0.8f, 0.5f);
		glVertex3f(0.00f, 0.66f, 0.0f);

		glColor3f(1.0f, 0.8f, 0.5f);
		glVertex3f(x + (0.0f), y + (0.66f), 0.0f);

	}

	glEnd();

	glBegin(GL_TRIANGLE_STRIP);
	{
		// ear 1
		for (float angle = 0.0f; angle <= 360.0f; angle = angle + 0.001f)
		{
			angle_rad = angle * (PB_PI / 180.0f);

			x = cos(angle_rad) / 3 * 0.1;
			y = sin(angle_rad) / 2 * 0.1;

			glColor3f(1.0f, 0.8f, 0.5f);
			glVertex3f(-0.115f, 0.64f, 0.0f);

			glColor3f(1.0f, 0.8f, 0.5f);
			glVertex3f(x + (-0.115f), y + (0.64f), 0.0f);

		}
	}

	glEnd();

	glBegin(GL_TRIANGLE_STRIP);
	{

		// ear 2
		for (float angle = 0.0f; angle <= 360.0f; angle = angle + 0.001f)
		{
			angle_rad = angle * (PB_PI / 180.0f);

			x = cos(angle_rad) / 3 * 0.1;
			y = sin(angle_rad) / 2 * 0.1;

			glColor3f(1.0f, 0.8f, 0.5f);
			glVertex3f(0.115f, 0.64f, 0.0f);

			glColor3f(1.0f, 0.8f, 0.5f);
			glVertex3f(x + (0.115f), y + (0.64f), 0.0f);

		}

	}

	glEnd();

	glBegin(GL_TRIANGLE_STRIP);
	{
		// sunglass 1
		for (float angle = 0.0f; angle <= 360.0f; angle = angle + 0.001f)
		{
			angle_rad = angle * (PB_PI / 180.0f);

			x = cos(angle_rad) / 2 * 0.08;
			y = sin(angle_rad) / 2 * 0.08;

			glColor3f(0.0f, 0.0f, 0.0f);
			glVertex3f(-0.06f, 0.65f, 0.0f);

			glColor3f(0.0f, 0.0f, 0.0f);
			glVertex3f(x + (-0.06f), y + (0.65f), 0.0f);

		}
	}
	glEnd();

	glBegin(GL_TRIANGLE_STRIP);
	{

		// sunglass 2
		for (float angle = 0.0f; angle <= 360.0f; angle = angle + 0.001f)
		{
			angle_rad = angle * (PB_PI / 180.0f);

			x = cos(angle_rad) / 2 * 0.08;
			y = sin(angle_rad) / 2 * 0.08;

			glColor3f(0.0f, 0.0f, 0.0f);
			glVertex3f(0.06f, 0.65f, 0.0f);

			glColor3f(0.0f, 0.0f, 0.0f);
			glVertex3f(x + (0.06f), y + (0.65f), 0.0f);

		}
	}
	glEnd();

	glBegin(GL_LINES);
	{
		// sunglass holder
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(-0.08f, 0.65f, 0.0f);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.08f, 0.65f, 0.0f);

		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(-0.08f, 0.65f, 0.0f);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(-0.13f, 0.69f, 0.0f);

		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.08f, 0.65f, 0.0f);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.13f, 0.69f, 0.0f);

	}
	glEnd();

	glBegin(GL_POLYGON);
	//Hair 

	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.2f, 0.85f, 0.0f);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.15f, 0.85f, 0.0f);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.13f, 0.8f, 0.0f);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.12f, 0.72f, 0.0f);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.12f, 0.72f, 0.0f);

	glEnd();



	glBegin(GL_QUADS);
	// Neck 
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(0.030f, 0.55f, 0.0f);
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(-0.030f, 0.55f, 0.0f);
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(-0.030f, 0.45f, 0.0f);
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(0.030f, 0.45f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);
	//Body chest 
	glColor3f(0.1f, 0.5f, 0.5f);
	glVertex3f(0.25f, 0.45f, 0.0f);
	glColor3f(0.1f, 0.5f, 0.5f);
	glVertex3f(-0.25f, 0.45f, 0.0f);
	glColor3f(0.1f, 0.5f, 0.5f);
	glVertex3f(-0.25f, -0.15f, 0.0f);
	glColor3f(0.1f, 0.5f, 0.5f);
	glVertex3f(0.25f, -0.15f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);
	//sholder 1 

	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(-0.25f, 0.45f, 0.0f);
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(-0.4f, 0.2f, 0.0f);
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(-0.3f, 0.2f, 0.0f);
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(-0.25f, 0.32f, 0.0f);

	//Hand 1
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(-0.3f, 0.2f, 0.0f);
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(-0.4f, 0.2f, 0.0f);
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(-0.3f, -0.05f, 0.0f);
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(-0.25f, -0.05f, 0.0f);

	glEnd();



	glBegin(GL_QUADS);
	//sholder 2 

	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(0.25f, 0.45f, 0.0f);
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(0.4f, 0.2f, 0.0f);
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(0.3f, 0.2f, 0.0f);
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(0.25f, 0.32f, 0.0f);

	//Hand 2
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(0.3f, 0.2f, 0.0f);
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(0.4f, 0.2f, 0.0f);
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(0.1f, 0.1f, 0.0f);
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(0.1f, 0.1f, -0.8f);

	glEnd();

	glBegin(GL_QUADS);
	//lower body leg 1
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.10f, -0.1f, 0.0f);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-0.25f, -0.1f, 0.0f);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-0.28f, -0.80f, 0.0f);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-0.17f, -0.80f, 0.0f);



	glEnd();

	glBegin(GL_QUADS);
	//lower body 2 

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.28f, -0.80f, 0.0f);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.25f, -0.1f, 0.0f);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-0.10f, -0.1f, 0.0f);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.17f, -0.80f, 0.0f);


	glEnd();


	glBegin(GL_QUADS);
	//Body chest 
	glColor3f(0.1f, 0.5f, 0.5f);
	glVertex3f(0.25f, 0.45f, 0.0f);
	glColor3f(0.1f, 0.5f, 0.5f);
	glVertex3f(-0.25f, 0.45f, 0.0f);
	glColor3f(0.1f, 0.5f, 0.5f);
	glVertex3f(-0.25f, -0.15f, 0.0f);
	glColor3f(0.1f, 0.5f, 0.5f);
	glVertex3f(0.25f, -0.15f, 0.0f);

	glEnd();


	glBegin(GL_POLYGON);
	//Shoes 1

	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.17f, -0.80f, 0.0f);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.28f, -0.80f, 0.0f);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.40f, -0.85f, 0.0f);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.40f, -0.90f, 0.0f);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.17f, -0.90f, 0.0f);

	glEnd();

	glBegin(GL_POLYGON);
	//Shoes 2

	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.17f, -0.80f, 0.0f);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.28f, -0.80f, 0.0f);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.40f, -0.85f, 0.0f);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.40f, -0.90f, 0.0f);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.17f, -0.90f, 0.0f);

	glEnd();
}
void leftHand()
{

	glBegin(GL_QUADS);
	//Hand 2
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(0.3f, 0.2f, 0.0f);
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(0.4f, 0.2f, 0.0f);
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(0.1f, 0.1f, 0.0f);
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(0.1f, 0.1f, -0.8f);

	glEnd();

	glBegin(GL_QUADS);
	//sholder 2 

	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(0.25f, 0.45f, 0.0f);
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(0.4f, 0.2f, 0.0f);
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(0.3f, 0.2f, 0.0f);
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(0.25f, 0.32f, 0.0f);

	glEnd();


}
void hands()
{
	glBegin(GL_QUADS);
	//sholder 1 

	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(-0.25f, 0.45f, 0.0f);
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(-0.4f, 0.2f, 0.0f);
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(-0.3f, 0.2f, 0.0f);
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(-0.25f, 0.32f, 0.0f);

	//Hand 1
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(-0.3f, 0.2f, 0.0f);
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(-0.4f, 0.2f, 0.0f);
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(-0.3f, -0.05f, 0.0f);
	glColor3f(1.0f, 0.8f, 0.5f);
	glVertex3f(-0.25f, -0.05f, 0.0f);

	glEnd();

	glPushMatrix();
	{
		glTranslatef(-0.1f, 0.0f, 0.0f);

		leftHand();
	}
	glPopMatrix();



}
void characterParas(void)
{
	// Neck
	glBegin(GL_QUADS);
	glColor3f(0.9f, 0.7f, 0.6f);
	glVertex3f(0.09f, 0.75f, 0.0f);
	glVertex3f(-0.09f, 0.75f, 0.0f);
	glVertex3f(-0.09f, 0.65f, 0.0f);
	glVertex3f(0.09f, 0.65f, 0.0f);
	glEnd();

	// Face
	glBegin(GL_POLYGON);
	glColor3f(0.9098039215686275f, 0.7450980392156863f, 0.6745098039215686);
	glVertex3f(0.15f, 1.0f, 0.0f);
	glVertex3f(-0.15f, 1.0f, 0.0f);
	glVertex3f(-0.15f, 0.8f, 0.0f);
	glVertex3f(-0.04f, 0.7f, 0.0f);
	glVertex3f(0.04f, 0.7f, 0.0f);
	glVertex3f(0.15f, 0.8f, 0.0f);
	glEnd();

	// Ears
	glBegin(GL_QUADS);
	glVertex3f(0.15f, 0.95f, 0.0f);
	glVertex3f(0.17f, 0.95f, 0.0f);
	glVertex3f(0.17f, 0.85f, 0.0f);
	glVertex3f(0.15f, 0.85f, 0.0f);

	glVertex3f(-0.15f, 0.95f, 0.0f);
	glVertex3f(-0.17f, 0.95f, 0.0f);
	glVertex3f(-0.17f, 0.85f, 0.0f);
	glVertex3f(-0.15f, 0.85f, 0.0f);
	glEnd();

	// Sun Glasses
	glBegin(GL_QUADS);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.13f, 0.955f, 0.0f);
	glVertex3f(0.13f, 0.89f, 0.0f);
	glVertex3f(0.02f, 0.89f, 0.0f);
	glVertex3f(0.02f, 0.955f, 0.0f);

	glVertex3f(-0.13f, 0.955f, 0.0f);
	glVertex3f(-0.13f, 0.89f, 0.0f);
	glVertex3f(-0.02f, 0.89f, 0.0f);
	glVertex3f(-0.02f, 0.955f, 0.0f);
	glEnd();

	glBegin(GL_LINES);
	glVertex3f(0.15f, 0.93f, 0.0f);
	glVertex3f(-0.15f, 0.93f, 0.0f);
	glEnd();

	// Hair
	glBegin(GL_POLYGON);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.16f, 1.0f, 0.0f);
	glVertex3f(-0.13f, 1.1f, 0.0f);
	glVertex3f(0.1f, 1.15f, 0.0f);
	glVertex3f(0.2f, 1.15f, 0.0f);
	glVertex3f(0.155f, 1.0f, 0.0f);
	glEnd();
	glBegin(GL_QUADS);
	glVertex3f(0.15f, 1.0f, 0.0f);
	glVertex3f(0.155f, 1.0f, 0.0f);
	glVertex3f(0.155f, 0.97f, 0.0f);
	glVertex3f(0.15f, 0.97f, 0.0f);

	glVertex3f(-0.15f, 1.0f, 0.0f);
	glVertex3f(-0.16f, 1.0f, 0.0f);
	glVertex3f(-0.16f, 0.97f, 0.0f);
	glVertex3f(-0.15f, 0.97f, 0.0f);
	glEnd();


	// Inner Shirt
	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.3f, 0.65f, 0.0f);
	glVertex3f(-0.3f, 0.65f, 0.0f);
	glVertex3f(-0.3f, 0.0f, 0.0f);
	glVertex3f(0.3f, 0.0f, 0.0f);

	glEnd();

	// Hands
	glBegin(GL_QUADS);
#if 0

	// Right hand forearm
	glColor3f(0.9098039215686275f, 0.7450980392156863f, 0.6745098039215686);
	glVertex3f(0.6f, 0.65f, 0.0f);
	glVertex3f(1.0f, 0.62f, 0.0f);
	glVertex3f(1.0f, 0.53f, 0.0f);
	glVertex3f(0.6f, 0.5f, 0.0f);

	// Left hand forearm
	glColor3f(0.9098039215686275f, 0.7450980392156863f, 0.6745098039215686);
	glVertex3f(-0.6f, 0.65f, 0.0f);
	glVertex3f(-1.0f, 0.62f, 0.0f);
	glVertex3f(-1.0f, 0.53f, 0.0f);
	glVertex3f(-0.6f, 0.5f, 0.0f);

	// Right hand bisep
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.3f, 0.65f, 0.0f);
	glVertex3f(0.6f, 0.65f, 0.0f);
	glVertex3f(0.6f, 0.5f, 0.0f);
	glVertex3f(0.3f, 0.5f, 0.0f);



	// Left hand bisep
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-0.3f, 0.65f, 0.0f);
	glVertex3f(-0.6f, 0.65f, 0.0f);
	glVertex3f(-0.6f, 0.5f, 0.0f);
	glVertex3f(-0.3f, 0.5f, 0.0f);
#endif


	glEnd();

	glPushMatrix();
	{
		glScalef(1.3f, 1.3f, 0.0f);
		glTranslatef(0.05f, 0.065f, 0.0f);
		hands();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(0.14f, -0.7f, 0.0f);
		glScalef(4.0f, 4.0f, 0.0f);
		DrawMic();
	}
	glPopMatrix();




	// Fingers
	// LEft
	//glBegin(GL_POLYGON);
	//glVertex3f(-0.20f, 0.53f, 0.0f);
	//glVertex3f(-0.20f, 0.65f, 0.0f);
	//glVertex3f(-0.204f, 0.65f, 0.0f);
	//glVertex3f(-0.204f, 0.53f, 0.0f);
	//glVertex3f(-0.22f, 0.53f, 0.0f);
	//glVertex3f(-0.22f, 0.62f, 0.0f);
	//glVertex3f(-0.20f, 0.62f, 0.0f);
	//glEnd();
	//// Right
	//glBegin(GL_POLYGON);
	//glVertex3f(1.0f, 0.53f, 0.0f);
	//glVertex3f(1.0f, 0.65f, 0.0f);
	//glVertex3f(1.04f, 0.65f, 0.0f);
	//glVertex3f(1.04f, 0.53f, 0.0f);
	//glVertex3f(1.2f, 0.53f, 0.0f);
	//glVertex3f(1.2f, 0.62f, 0.0f);
	//glVertex3f(1.0f, 0.62f, 0.0f);
	//glEnd();


	// Legs
	glBegin(GL_POLYGON);
	glColor3f(0.6f, 0.8f, 0.9f);
	glVertex3f(0.3f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, -0.2f, 0.0f);
	glVertex3f(0.06f, -0.4f, 0.0f);
	glVertex3f(0.33f, -0.4f, 0.0f);

	glEnd();
	glBegin(GL_QUADS);
	glVertex3f(0.33f, -0.4f, 0.0f);
	glVertex3f(0.06f, -0.4f, 0.0f);
	glVertex3f(0.15f, -0.8f, 0.0f);
	glVertex3f(0.3f, -0.8f, 0.0f);


	glVertex3f(-0.33f, -0.4f, 0.0f);
	glVertex3f(-0.06f, -0.4f, 0.0f);
	glVertex3f(-0.15f, -0.8f, 0.0f);
	glVertex3f(-0.3f, -0.8f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.6f, 0.8f, 0.9f);
	glVertex3f(-0.3f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.0f, -0.2f, 0.0f);
	glVertex3f(-0.06f, -0.4f, 0.0f);
	glVertex3f(-0.33f, -0.4f, 0.0f);

	glEnd();

	// Shoes
	glBegin(GL_QUADS);
	glColor3f(0.9098039215686275f, 0.7450980392156863f, 0.6745098039215686);
	glVertex3f(-0.15f, -0.8f, 0.0f);
	glVertex3f(-0.15f, -0.9f, 0.0f);
	glVertex3f(-0.45f, -0.9f, 0.0f);
	glVertex3f(-0.45f, -0.8f, 0.0f);

	glVertex3f(0.15f, -0.8f, 0.0f);
	glVertex3f(0.15f, -0.9f, 0.0f);
	glVertex3f(0.45f, -0.9f, 0.0f);
	glVertex3f(0.45f, -0.8f, 0.0f);

	glEnd();
}

void characterVinit(void)
{
	//shirt
	glBegin(GL_QUADS);
	{
		glColor3f(255.0f / 255.0f, 55.0f / 255.0f, 128.0f / 255.0f);

		glVertex3f(0.18f, 0.5f, 0.0f);
		glVertex3f(-0.18f, 0.5f, 0.0f);
		glVertex3f(-0.18f, 0.0f, 0.0f);
		glVertex3f(0.18f, 0.0f, 0.0f);
	}
	glEnd();



	//pants
	glBegin(GL_QUADS);
	{
		glColor3f(1.0f, 1.0f, 1.0f);

		glVertex3f(0.18f, 0.0f, 0.0f);
		glVertex3f(-0.18f, 0.0f, 0.0f);
		glVertex3f(-0.18f, -0.1f, 0.0f);
		glVertex3f(0.18f, -0.1f, 0.0f);

	}
	glEnd();

	glBegin(GL_QUADS);
	{
		glColor3f(1.0f, 1.0f, 1.0f);

		glVertex3f(-0.01f, -0.1f, 0.0f);
		glVertex3f(-0.18f, -0.1f, 0.0f);
		glVertex3f(-0.18f, -0.6f, 0.0f);
		glVertex3f(-0.1f, -0.6f, 0.0f);


	}
	glEnd();

	glBegin(GL_QUADS);
	{
		glColor3f(1.0f, 1.0f, 1.0f);

		glVertex3f(0.18f, -0.1f, 0.0f);
		glVertex3f(0.01f, -0.1f, 0.0f);
		glVertex3f(0.1f, -0.6f, 0.0f);
		glVertex3f(0.18f, -0.6f, 0.0f);
	}
	glEnd();

	//neck
	glBegin(GL_QUADS);
	{
		glColor3f(242.0f / 255.0f, 200.0f / 255.0f, 182.0f / 255.0f);

		glVertex3f(0.03f, 0.55f, 0.0f);
		glVertex3f(-0.03f, 0.55f, 0.0f);
		glVertex3f(-0.03f, 0.5f, 0.0f);
		glVertex3f(0.03f, 0.5f, 0.0f);

	}
	glEnd();

	//bow
	glBegin(GL_TRIANGLES);
	{
		glColor3f(0.1f, 0.1f, 0.1f);

		glVertex3f(0.0f, 0.5f, 0.0f);
		glVertex3f(-0.06f, 0.52f, 0.0f);
		glVertex3f(-0.06f, 0.48f, 0.0f);
	}
	glEnd();

	glBegin(GL_TRIANGLES);
	{
		glColor3f(0.1f, 0.1f, 0.1f);

		glVertex3f(0.0f, 0.5f, 0.0f);
		glVertex3f(0.06f, 0.48f, 0.0f);
		glVertex3f(0.06f, 0.52f, 0.0f);

	}
	glEnd();

	//head
	float fAngle = 0.0f;
	float fRadx = 0.09f;
	float fRady = 0.18f;
	float fX = 0, fY = 0;

	glBegin(GL_LINE_LOOP);
	for (fAngle = 0.0f; fAngle <= 360.0f; fAngle += 0.1f)
	{
		fX = cos(RAD(fAngle)) * fRadx;
		fY = sin(RAD(fAngle)) * fRady;

		glColor3f(230.0f / 255.0f, 190.0f / 255.0f, 172.0f / 255.0f);

		glVertex3f(0.0f, 0.71f, 0.0f);
		glVertex3f(fX, fY + 0.71f, 0.0f);

	}
	glEnd();


	////head2
	//glBegin(GL_POLYGON);
	//{
	//	glColor3f(255.0f / 255.0f, 232.0f / 255.0f, 201.0f / 255.0f);

	//	glVertex3f(0.02f, 0.54f, 0.0f);
	//	glVertex3f(0.05f, 0.60f, 0.0f);
	//	glVertex3f(0.07f, 0.65f, 0.0f);
	//	glVertex3f(0.09f, 0.69f, 0.0f);
	//	glVertex3f(-0.09f, 0.69f, 0.0f);
	//	glVertex3f(-0.07f, 0.65f, 0.0f);
	//	glVertex3f(-0.05f, 0.60f, 0.0f);
	//	glVertex3f(-0.02f, 0.54f, 0.0f);

	//}
	//glEnd();

	//hairs
	fRadx = 0.09f;
	fRady = 0.19f;
	glBegin(GL_POLYGON);
	{
		glColor3f(0.1f, 0.1f, 0.1f);
		for (fAngle = 20.0f; fAngle <= 160.0f; fAngle += 0.1f)
		{
			fX = cos(RAD(fAngle)) * fRadx;
			fY = sin(RAD(fAngle)) * fRady;

			glVertex3f(fX, fY + 0.71f, 0.0f);

		}

		glVertex3f(-0.05f, 0.8f, 0.0f);
		glVertex3f(-0.05f, 0.75f, 0.0f);

		glVertex3f(0.0f, 0.8f, 0.0f);
		glVertex3f(0.0f, 0.75f, 0.0f);

		glVertex3f(0.05f, 0.8f, 0.0f);
		glVertex3f(0.05f, 0.75f, 0.0f);

	}
	glEnd();

	//hands
	//left
	glBegin(GL_QUADS);
	{
		glColor3f(255.0f / 255.0f, 55.0f / 255.0f, 128.0f / 255.0f);

		glVertex3f(-0.18f, 0.5f, 0.0f);
		glVertex3f(-0.3f, 0.2f, 0.0f);
		glVertex3f(-0.2f, 0.15f, 0.0f);
		glVertex3f(-0.18f, 0.37f, 0.0f);


		glColor3f(1.0f, 0.8f, 0.5f);
		glVertex3f(-0.2f, 0.15f, 0.0f);
		glVertex3f(-0.3f, 0.2f, 0.0f);
		glVertex3f(-0.15f, -0.1f, 0.0f);
		glVertex3f(-0.05f, -0.15f, 0.0f);

	}
	glEnd();

	//right
	glBegin(GL_QUADS);
	{
		glColor3f(255.0f / 255.0f, 55.0f / 255.0f, 128.0f / 255.0f);

		glVertex3f(0.18f, 0.5f, 0.0f);
		glVertex3f(0.3f, 0.2f, 0.0f);
		glVertex3f(0.2f, 0.15f, 0.0f);
		glVertex3f(0.18f, 0.37f, 0.0f);

		glColor3f(1.0f, 0.8f, 0.5f);
		glVertex3f(0.2f, 0.15f, 0.0f);
		glVertex3f(0.3f, 0.2f, 0.0f);
		glVertex3f(0.15f, 0.1f, 0.0f);
		glVertex3f(0.05f, 0.15f, 0.0f);

	}
	glEnd();


	//ears
	fRadx = 0.01f;
	fRady = 0.05f;
	//right ear
	fX = 0, fY = 0;
	glBegin(GL_LINE_LOOP);
	for (fAngle = 0.0f; fAngle <= 360.0f; fAngle += 0.1f)
	{
		fX = cos(RAD(fAngle)) * fRadx;
		fY = sin(RAD(fAngle)) * fRady;

		glColor3f(230.0f / 255.0f, 190.0f / 255.0f, 172.0f / 255.0f);

		glVertex3f(0.09f, 0.69f, 0.0f);
		glVertex3f(fX + 0.09, fY + 0.69f, 0.0f);

	}
	glEnd();

	//left ear
	fX = 0, fY = 0;
	glBegin(GL_LINE_LOOP);
	for (fAngle = 0.0f; fAngle <= 360.0f; fAngle += 0.1f)
	{
		fX = cos(RAD(fAngle)) * fRadx;
		fY = sin(RAD(fAngle)) * fRady;

		glColor3f(230.0f / 255.0f, 190.0f / 255.0f, 172.0f / 255.0f);

		glVertex3f(-0.09f, 0.69f, 0.0f);
		glVertex3f(fX - 0.09, fY + 0.69f, 0.0f);

	}
	glEnd();

	//thuglife glasses
	//left glass
	glBegin(GL_QUADS);
	{
		glColor3f(0.0f, 0.0f, 0.0f);

		glVertex3f(-0.02f, 0.73f, 0.0f);
		glVertex3f(-0.08f, 0.73f, 0.0f);
		glVertex3f(-0.08f, 0.68f, 0.0f);
		glVertex3f(-0.02f, 0.68f, 0.0f);

	}
	glEnd();
	//white squares
	glBegin(GL_QUADS);
	{
		glColor3f(1.0f, 1.0f, 1.0f);

		glVertex3f(-0.07f, 0.70f, 0.0f);
		glVertex3f(-0.08f, 0.70f, 0.0f);
		glVertex3f(-0.08f, 0.69f, 0.0f);
		glVertex3f(-0.07f, 0.69f, 0.0f);

	}
	glEnd();
	glBegin(GL_QUADS);
	{
		glColor3f(1.0f, 1.0f, 1.0f);

		glVertex3f(-0.05f, 0.70f, 0.0f);
		glVertex3f(-0.06f, 0.70f, 0.0f);
		glVertex3f(-0.06f, 0.69f, 0.0f);
		glVertex3f(-0.05f, 0.69f, 0.0f);

	}
	glEnd();
	glBegin(GL_QUADS);
	{
		glColor3f(1.0f, 1.0f, 1.0f);

		glVertex3f(-0.06f, 0.69f, 0.0f);
		glVertex3f(-0.07f, 0.69f, 0.0f);
		glVertex3f(-0.07f, 0.68f, 0.0f);
		glVertex3f(-0.06f, 0.68f, 0.0f);

	}
	glEnd();
	glBegin(GL_QUADS);
	{
		glColor3f(1.0f, 1.0f, 1.0f);

		glVertex3f(-0.04f, 0.69f, 0.0f);
		glVertex3f(-0.05f, 0.69f, 0.0f);
		glVertex3f(-0.05f, 0.68f, 0.0f);
		glVertex3f(-0.04f, 0.68f, 0.0f);
	}
	glEnd();

	//right glass
	glBegin(GL_QUADS);
	{
		glColor3f(0.0f, 0.0f, 0.0f);

		glVertex3f(0.02f, 0.73f, 0.0f);
		glVertex3f(0.08f, 0.73f, 0.0f);
		glVertex3f(0.08f, 0.68f, 0.0f);
		glVertex3f(0.02f, 0.68f, 0.0f);

	}
	glEnd();
	//white squares
	glBegin(GL_QUADS);
	{
		glColor3f(1.0f, 1.0f, 1.0f);

		glVertex3f(0.03f, 0.70f, 0.0f);
		glVertex3f(0.02f, 0.70f, 0.0f);
		glVertex3f(0.02f, 0.69f, 0.0f);
		glVertex3f(0.03f, 0.69f, 0.0f);

	}
	glEnd();
	glBegin(GL_QUADS);
	{
		glColor3f(1.0f, 1.0f, 1.0f);

		glVertex3f(0.05f, 0.70f, 0.0f);
		glVertex3f(0.04f, 0.70f, 0.0f);
		glVertex3f(0.04f, 0.69f, 0.0f);
		glVertex3f(0.05f, 0.69f, 0.0f);

	}
	glEnd();
	glBegin(GL_QUADS);
	{
		glColor3f(1.0f, 1.0f, 1.0f);

		glVertex3f(0.04f, 0.69f, 0.0f);
		glVertex3f(0.03f, 0.69f, 0.0f);
		glVertex3f(0.03f, 0.68f, 0.0f);
		glVertex3f(0.04f, 0.68f, 0.0f);

	}
	glEnd();
	glBegin(GL_QUADS);
	{
		glColor3f(1.0f, 1.0f, 1.0f);

		glVertex3f(0.06f, 0.69f, 0.0f);
		glVertex3f(0.05f, 0.69f, 0.0f);
		glVertex3f(0.05f, 0.68f, 0.0f);
		glVertex3f(0.06f, 0.68f, 0.0f);
	}
	glEnd();
	//nose point
	glBegin(GL_QUADS);
	{
		glColor3f(0.0f, 0.0f, 0.0f);

		glVertex3f(0.02f, 0.73f, 0.0f);
		glVertex3f(-0.02f, 0.73f, 0.0f);
		glVertex3f(-0.02f, 0.72f, 0.0f);
		glVertex3f(0.02f, 0.72f, 0.0f);
	}
	glEnd();


	//shoes
	//left
	glBegin(GL_POLYGON);
	{
		glColor3f(0.0f, 0.0f, 0.0f);

		glVertex3f(-0.1f, -0.6f, 0.0f);
		glVertex3f(-0.18f, -0.6f, 0.0f);
		glVertex3f(-0.25f, -0.65f, 0.0f);
		glVertex3f(-0.25f, -0.70f, 0.0f);
		glVertex3f(-0.1f, -0.70f, 0.0f);

	}
	glEnd();

	//right
	glBegin(GL_POLYGON);
	{
		glColor3f(0.0f, 0.0f, 0.0f);

		glVertex3f(0.1f, -0.6f, 0.0f);
		glVertex3f(0.1f, -0.70f, 0.0f);
		glVertex3f(0.25f, -0.70f, 0.0f);
		glVertex3f(0.25f, -0.65f, 0.0f);
		glVertex3f(0.18f, -0.6f, 0.0f);
	}
	glEnd();




}

void characterSanket(void)
{
	// -----------------main face ---------------
	float angle = 0.0f;
	float x = 0.0f;
	float y = 0.0f;

	float angleRadian = 0.0f;

	glBegin(GL_LINES); // Face 

	for (angle = 0.0f; angle <= 360.0f; angle = angle + 0.10f)
	{
		float r = 0.08f;
		angleRadian = angle * (SSH_PI / 180.0f);
		x = cos(angleRadian) * r;
		y = sin(angleRadian) * r;

		glColor3f(239.0f / 255.0f, 207.0f / 255.0f, 145.0f / 255.0f);
		glVertex3f(0.0f, 0.53f, 0.0f);
		glColor3f(239.0f / 255.0f, 207.0f / 255.0f, 145.0f / 255.0f);
		glVertex3f(x + (0.0f), y + (0.53f), 0.0f);
	}

	glEnd();

	// 

	glBegin(GL_LINES); 	// left eye

	for (angle = 0.0f; angle <= 360.0f; angle = angle + 0.10f)
	{
		float r = 0.01;
		angleRadian = angle * (SSH_PI / 180.0f);
		x = cos(angleRadian) * r;
		y = sin(angleRadian) * r;

		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-0.03f, 0.55f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(x + (-0.03f), y + (0.55f), 0.0f);
	}

	glEnd();

	glBegin(GL_LINES); 	// right eye

	for (angle = 0.0f; angle <= 360.0f; angle = angle + 0.10f)
	{
		float r = 0.01;
		angleRadian = angle * (SSH_PI / 180.0f);
		x = cos(angleRadian) * r;
		y = sin(angleRadian) * r;

		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.03f, 0.55f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(x + (0.03f), y + (0.55f), 0.0f);
	}

	glEnd();

	glBegin(GL_TRIANGLES); // lips
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.03f, 0.5f, 0.0f);
	glVertex3f(0.03f, 0.5f, 0.0f);
	glVertex3f(0.0f, 0.48f, 0.0f);

	glEnd();



	glBegin(GL_QUADS);  // left Hand Part1

	glColor3f(239.0f / 255.0f, 207.0f / 255.0f, 145.0f / 255.0f);
	glVertex3f(-0.13f, 0.3f, 0.0f);
	glVertex3f(-0.15f, 0.35f, 0.0f);
	glVertex3f(-0.25f, 0.15f, 0.0f);
	glVertex3f(-0.2f, 0.15f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);  // left Hand Part2
	glVertex3f(-0.2f, 0.0f, 0.0f);
	glVertex3f(-0.2f, 0.15f, 0.0f);
	glVertex3f(-0.25f, 0.15f, 0.0f);
	glVertex3f(-0.25f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);  // right Hand Part1

	glColor3f(239.0f / 255.0f, 207.0f / 255.0f, 145.0f / 255.0f);
	glVertex3f(0.13f, 0.3f, 0.0f);
	glVertex3f(0.15f, 0.35f, 0.0f);
	glVertex3f(0.25f, 0.15f, 0.0f);
	glVertex3f(0.2f, 0.15f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);  // right Hand Part2
	glVertex3f(0.12f, 0.25f, 0.0f);
	glVertex3f(0.25f, 0.15f, 0.0f);
	glVertex3f(0.2f, 0.15f, 0.0f);
	glVertex3f(0.1f, 0.2f, 0.0f);

	glEnd();


	glBegin(GL_QUADS);   // Right Leg

	glColor3f(239.0f / 255.0f, 207.0f / 255.0f, 145.0f / 255.0f);
	glVertex3f(-0.05f, -0.54f, 0.0f);
	glVertex3f(-0.05f, -0.85f, 0.0f);
	glVertex3f(-0.1f, -0.85f, 0.0f);
	glVertex3f(-0.1f, -0.53f, 0.0f);

	glEnd();


	glBegin(GL_QUADS);   // left Leg

	glColor3f(239.0f / 255.0f, 207.0f / 255.0f, 145.0f / 255.0f);
	glVertex3f(0.05f, -0.54f, 0.0f);
	glVertex3f(0.05f, -0.85f, 0.0f);
	glVertex3f(0.1f, -0.85f, 0.0f);
	glVertex3f(0.1f, -0.53f, 0.0f);

	glEnd();


	glBegin(GL_QUADS); // Right Leg Shoes

	glColor3f(234.0f / 255.0f, 54.0f / 255.0f, 128.0f / 255.0f);

	glVertex3f(-0.05f, -0.85f, 0.0f);
	glVertex3f(-0.1f, -0.9f, 0.0f);
	glVertex3f(-0.12f, -0.9f, 0.0f);
	glVertex3f(-0.1f, -0.85f, 0.0f);

	glEnd();

	glBegin(GL_QUADS); // Right Leg Shoes chi Dandi

	glColor3f(234.0f / 255.0f, 54.0f / 255.0f, 128.0f / 255.0f);

	glVertex3f(-0.05f, -0.85f, 0.0f);
	glVertex3f(-0.05f, -0.9f, 0.0f);
	glVertex3f(-0.06f, -0.9f, 0.0f);
	glVertex3f(-0.06f, -0.85f, 0.0f);

	glEnd();

	glBegin(GL_QUADS); // Left Leg Shoes

	glColor3f(234.0f / 255.0f, 54.0f / 255.0f, 128.0f / 255.0f);

	glVertex3f(0.05f, -0.85f, 0.0f);
	glVertex3f(0.1f, -0.9f, 0.0f);
	glVertex3f(0.12f, -0.9f, 0.0f);
	glVertex3f(0.1f, -0.85f, 0.0f);

	glEnd();

	glBegin(GL_QUADS); // Left Leg Shoes chi Dandi

	glColor3f(234.0f / 255.0f, 54.0f / 255.0f, 128.0f / 255.0f);

	glVertex3f(0.05f, -0.85f, 0.0f);
	glVertex3f(0.05f, -0.9f, 0.0f);
	glVertex3f(0.06f, -0.9f, 0.0f);
	glVertex3f(0.06f, -0.85f, 0.0f);

	glEnd();


	//glBegin(GL_LINE_LOOP);  // Skirt
	glBegin(GL_POLYGON);

	glColor3f(234.0f / 255.0f, 54.0f / 255.0f, 128.0f / 255.0f);
	glVertex3f(-0.15f, 0.0f, 0.0f);
	glVertex3f(-0.15f, -0.5f, 0.0f);
	glVertex3f(-0.1f, -0.53f, 0.0f);
	glVertex3f(-0.05f, -0.54f, 0.0f);
	glVertex3f(0.0f, -0.55f, 0.0f);
	glVertex3f(0.05f, -0.54f, 0.0f);
	glVertex3f(0.1f, -0.53f, 0.0f);
	glVertex3f(0.15f, -0.5f, 0.0f);
	glVertex3f(0.15f, 0.0f, 0.0f);

	glEnd();


	glBegin(GL_QUADS);  // Skirt Chya Varcha P1

	glColor3f(234.0f / 255.0f, 54.0f / 255.0f, 128.0f / 255.0f);
	glVertex3f(0.15f, 0.0f, 0.0f);
	glVertex3f(0.1f, 0.1f, 0.0f);
	glVertex3f(-0.1f, 0.1f, 0.0f);
	glVertex3f(-0.15f, 0.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);  // Skirt Chya Varcha P2

	glColor3f(234.0f / 255.0f, 54.0f / 255.0f, 128.0f / 255.0f);
	glVertex3f(0.1f, 0.1f, 0.0f);
	glVertex3f(0.1f, 0.2f, 0.0f);
	glVertex3f(-0.1f, 0.2f, 0.0f);
	glVertex3f(-0.1f, 0.1f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);  // Skirt Chya Varcha P3

	glColor3f(234.0f / 255.0f, 54.0f / 255.0f, 128.0f / 255.0f);
	glVertex3f(0.1f, 0.2f, 0.0f);
	glVertex3f(0.15f, 0.35f, 0.0f);
	glVertex3f(-0.15f, 0.35f, 0.0f);
	glVertex3f(-0.1f, 0.2f, 0.0f);

	glEnd();



	glBegin(GL_QUADS);  // Neck P1

	glColor3f(239.0f / 255.0f, 207.0f / 255.0f, 145.0f / 255.0f);
	glVertex3f(0.1f, 0.35f, 0.0f);
	glVertex3f(0.02f, 0.4f, 0.0f);
	glVertex3f(-0.02f, 0.4f, 0.0f);
	glVertex3f(-0.1f, 0.35f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);  // Neck P2

	glColor3f(239.0f / 255.0f, 207.0f / 255.0f, 145.0f / 255.0f);
	glVertex3f(0.02f, 0.4f, 0.0f);
	glVertex3f(0.02f, 0.46f, 0.0f);
	glVertex3f(-0.02f, 0.46f, 0.0f);
	glVertex3f(-0.02f, 0.4f, 0.0f);

	glEnd();

	//----------------------hairs-----------------
	glBegin(GL_POLYGON); // model hairs left

	glColor3f(143.0f / 255.0f, 64.0f / 255.0f, 58.0f / 255.0f);
	//glColor3f(1.0f, 0.0f, 0.0f);

	glVertex3f(0.0f, 0.6f, 0.0f);
	glVertex3f(0.0f, 0.63f, 0.0f);
	glVertex3f(-0.08f, 0.58f, 0.0f);
	glVertex3f(-0.1f, 0.55f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);
	glVertex3f(-0.1f, 0.4f, 0.0f);
	glVertex3f(-0.05f, 0.6f, 0.0f);
	glVertex3f(-0.1f, 0.55f, 0.0f);
	glVertex3f(-0.1f, 0.5f, 0.0f);

	glEnd();

	glBegin(GL_POLYGON); // model hairs right

	glColor3f(143.0f / 255.0f, 64.0f / 255.0f, 58.0f / 255.0f);

	glVertex3f(0.0f, 0.6f, 0.0f);
	glVertex3f(0.0f, 0.63f, 0.0f);
	glVertex3f(0.08f, 0.58f, 0.0f);
	glVertex3f(0.1f, 0.55f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);
	glVertex3f(0.1f, 0.0f, 0.0f);
	glVertex3f(0.05f, 0.6f, 0.0f);
	glVertex3f(0.1f, 0.55f, 0.0f);
	glVertex3f(0.1f, 0.5f, 0.0f);

	glEnd();

	//-------------------Guitar-----------------------------
	glBegin(GL_QUADS);
	glColor3f(204.0f / 255.0f, 24.0f / 255.0f, 31.0f / 255.0f);

	glVertex3f(-0.05f, 0.15f, 0.0f);
	glVertex3f(-0.1f, 0.2f, 0.0f);
	glVertex3f(-0.3f, 0.15f, 0.0f);
	glVertex3f(-0.16f, -0.05f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);

	glVertex3f(-0.1f, 0.2f, 0.0f);
	glVertex3f(-0.05f, 0.15f, 0.0f);
	glVertex3f(0.25f, 0.3f, 0.0f);
	glVertex3f(0.2f, 0.35f, 0.0f);

	glEnd();

	glBegin(GL_TRIANGLES);

	glVertex3f(0.2f, 0.3f, 0.0f);
	glVertex3f(0.28f, 0.3f, 0.0f);
	glVertex3f(0.2f, 0.38f, 0.0f);

	glEnd();

	glBegin(GL_LINES);	// string 1
	glColor3f(1.0f, 1.0f, 1.0f);

	glVertex3f(0.22f, 0.31f, 0.0f);
	glVertex3f(-0.18f, 0.10f, 0.0f);

	glEnd();

	glBegin(GL_LINES);	// string 2
	glColor3f(1.0f, 1.0f, 1.0f);

	glVertex3f(0.21f, 0.32f, 0.0f);
	glVertex3f(-0.19f, 0.11f, 0.0f);

	glEnd();

	glBegin(GL_LINES);	// string 3
	glColor3f(1.0f, 1.0f, 1.0f);

	glVertex3f(0.2f, 0.33f, 0.0f);
	glVertex3f(-0.2f, 0.12f, 0.0f);

	glEnd();

	//---------------------------------------------------
	glBegin(GL_TRIANGLES);	// guitar string strip
	glColor3f(1.0f, 1.0f, 1.0f);

	glVertex3f(-0.2f, 0.12f, 0.0f);
	glVertex3f(-0.18f, 0.10f, 0.0f);
	glVertex3f(-0.23f, 0.08f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES);	// guitar string strip2
	glColor3f(1.0f, 1.0f, 1.0f);

	glVertex3f(0.2f, 0.33f, 0.0f);
	glVertex3f(0.22f, 0.31f, 0.0f);
	glVertex3f(0.23f, 0.33f, 0.0f);
	glEnd();

	//-------------------Fingers on Guitar---------------------
	glBegin(GL_TRIANGLES);	// right side f1
	glColor3f(239.0f / 255.0f, 207.0f / 255.0f, 145.0f / 255.0f);

	glVertex3f(0.1f, 0.2f, 0.0f);
	glVertex3f(0.07f, 0.23f, 0.0f);
	glVertex3f(0.11f, 0.22f, 0.0f);

	glEnd();

	glBegin(GL_TRIANGLES);	// right side f2
	glColor3f(239.0f / 255.0f, 207.0f / 255.0f, 145.0f / 255.0f);

	glVertex3f(0.11f, 0.22f, 0.0f);
	glVertex3f(0.09f, 0.23f, 0.0f);
	glVertex3f(0.12f, 0.25f, 0.0f);

	glEnd();
}

void scene_4_GuitarGuy(void)
{
	void DrawGuitarGuy(void);
	void DrawGlasses(void);
	void DrawGuitar(void);
	void DrawRightHandMovement(void);
	void DrawLeftHand(void);

	glLoadIdentity();
	glTranslatef(-0.0f, -0.43f, 0.0f);
	glScalef(0.5f, 0.40f, 0.0f);
	DrawGuitarGuy();

	glLoadIdentity();
	glTranslatef(-0.0f, -0.465f, 0.0f);
	glScalef(0.4f, 0.4f, 0.0f);
	DrawGlasses();

	glLoadIdentity();
	glTranslatef(-0.0f, -0.41f, 0.0f);
	glScalef(0.35f, 0.40f, 0.0f);
	glRotatef(310.0f, 0.0f, 0.0f, 1.0f);
	DrawGuitar();

	glLoadIdentity();
	glTranslatef(sRightHandBottomPosi.x, -0.43f, 0.0f);
	glScalef(0.5f, 0.50f, 0.0f);
	glRotatef(sRightHandAngle.z, 0.0f, 0.0f, 1.0f);
	DrawRightHandMovement();

	glLoadIdentity();
	glTranslatef(-0.0f, -0.43f, 0.0f);
	glTranslatef(0.0f, sLeftHandPosi.y, 0.0f);
	glScalef(0.5f, 0.40f, 0.0f);
	glRotatef(sLeftHandPosi.z, 0.0f, 0.0f, 1.0f);
	DrawLeftHand();

}

void scene_4_DrumSet(void)
{
	void Face(void);
	void Leg(void);
	void MiddleBody(void);
	void LeftArm(void);
	void RightArm(void);
	void Drum1(void);
	void Drum2(void);
	void DrumLine(void);
	void gline(void);
	void drumStand(void);
	void drumstick(void);
	void RightHand(void);
	void LeftHand(void);

	glLoadIdentity();

	glTranslatef(0.5f, -0.25f, 0.0f);
	glScalef(0.5f, 0.5f, 0.0f);


	Face();
	Leg();
	MiddleBody();
	LeftArm();
	RightArm();
	RightHand();
	LeftHand();

	glLoadIdentity();
	glTranslatef(0.5f, -0.3f, 0.5f);
	glScalef(0.4f, 0.45f, 0.0f);
	DrumLine();
	drumStand();

	// Central Drum

	glLoadIdentity();
	glTranslatef(0.5f, -0.3f, 0.5f);
	glScalef(0.4f, 0.45f, 0.0f);
	gline();

	glLoadIdentity();
	glTranslatef(0.5f, -0.45f, 0.5f);
	glScalef(0.4f, 0.45f, 0.0f);
	Drum2();


	glLoadIdentity();
	glTranslatef(0.5f, -0.35f, 0.5f);
	glScalef(0.4f, 0.45f, 0.0f);
	Drum1();

	// RHS Drum
	glLoadIdentity();
	glTranslatef(0.65f, -0.3f, 0.5f);
	glScalef(0.4f, 0.45f, 0.0f);
	DrumLine();
	drumStand();

	glLoadIdentity();
	glTranslatef(0.65f, -0.45f, 0.5f);
	glScalef(0.4f, 0.45f, 0.0f);
	Drum2();

	glLoadIdentity();
	glTranslatef(0.65f, -0.35f, 0.5f);
	glScalef(0.4f, 0.45f, 0.0f);
	Drum1();


	// LHS Drum
	glLoadIdentity();
	glTranslatef(0.35f, -0.3f, 0.5f);
	glScalef(0.4f, 0.45f, 0.0f);
	DrumLine();
	drumStand();

	glLoadIdentity();
	glTranslatef(0.35f, -0.45f, 0.5f);
	glScalef(0.4f, 0.45f, 0.0f);
	Drum2();

	glLoadIdentity();
	glTranslatef(0.35f, -0.35f, 0.5f);
	glScalef(0.4f, 0.45f, 0.0f);
	Drum1();


	glLoadIdentity();
	glTranslatef(0.5f, -0.25f, 0.5f);
	glScalef(0.5f, 0.5f, 0.0f);
	drumstick();
}

void DrawGuitar(void)
{


	//guitar circles 
	float fAngle = 0.0f;
	float fRadx = 0.12f;
	float fRady = 0.16f;
	float fX = 0, fY = 0;

	//bottom
	glColor3f(204.0f / 255.0f, 116.0f / 255.0f, 98.0f / 255.0f);
	glBegin(GL_LINE_LOOP);
	for (fAngle = 0.0f; fAngle <= 360.0f; fAngle += 0.1f)
	{
		fX = cos(RAD(fAngle)) * fRadx;
		fY = sin(RAD(fAngle)) * fRady;


		glVertex3f(0.0f, -0.2f, 0.0f);
		glVertex3f(fX, fY - 0.2f, 0.0f);

	}
	glEnd();




	fAngle = 0.0f;
	fRadx = 0.09f;
	fRady = 0.08f;
	fX = 0, fY = 0;


	//top
	glBegin(GL_LINE_LOOP);
	for (fAngle = 0.0f; fAngle <= 360.0f; fAngle += 0.1f)
	{
		fX = cos(RAD(fAngle)) * fRadx;
		fY = sin(RAD(fAngle)) * fRady;


		glVertex3f(0.0f, -0.02f, 0.0f);
		glVertex3f(fX, fY - 0.02f, 0.0f);

	}
	glEnd();


	fAngle = 0.0f;
	fRadx = 0.05f;
	fRady = 0.05f;
	fX = 0, fY = 0;

	glColor3f(212.0f / 255.0f, 132.0f / 255.0f, 110.0f / 255.0f);

	//outer inner
	glBegin(GL_LINE_LOOP);
	for (fAngle = 0.0f; fAngle <= 360.0f; fAngle += 0.1f)
	{
		fX = cos(RAD(fAngle)) * fRadx;
		fY = sin(RAD(fAngle)) * fRady;


		glVertex3f(0.0f, -0.04f, 0.0f);
		glVertex3f(fX, fY - 0.04f, 0.0f);

	}
	glEnd();

	fAngle = 0.0f;
	fRadx = 0.03f;
	fRady = 0.03f;
	fX = 0, fY = 0;

	glColor3f(100.0f / 255.0f, 85.0f / 255.0f, 78.0f / 255.0f);
	//inner inner
	glBegin(GL_LINE_LOOP);
	for (fAngle = 0.0f; fAngle <= 360.0f; fAngle += 0.1f)
	{
		fX = cos(RAD(fAngle)) * fRadx;
		fY = sin(RAD(fAngle)) * fRady;


		glVertex3f(0.0f, -0.04f, 0.0f);
		glVertex3f(fX, fY - 0.04f, 0.0f);

	}
	glEnd();

	//bottom circle string hold
	glBegin(GL_QUADS);
	{
		glColor3f(0.0f, 0.0f, 0.0f);

		glVertex3f(0.05f, -0.2f, 0.0f);
		glVertex3f(-0.05f, -0.2f, 0.0f);
		glVertex3f(-0.04f, -0.23f, 0.0f);
		glVertex3f(0.04f, -0.23f, 0.0f);
	}
	glEnd();

	//guitar handle
	glColor3f(0.5f, 0.5f, 0.5f);

	glBegin(GL_POLYGON);
	{
		glVertex3f(0.02f, 0.4f, 0.0f);
		glVertex3f(-0.02f, 0.4f, 0.0f);
		glVertex3f(-0.02f, -0.00f, 0.0f);
		glVertex3f(0.02f, -0.00f, 0.0f);
	}
	glEnd();


	//top string holder
	glBegin(GL_POLYGON);
	{
		glVertex3f(0.025f, 0.5f, 0.0f);
		glVertex3f(-0.025f, 0.5f, 0.0f);
		glVertex3f(-0.025f, 0.44f, 0.0f);
		glVertex3f(-0.02f, 0.4f, 0.0f);
		glVertex3f(0.02f, 0.4f, 0.0f);
		glVertex3f(0.025f, 0.44f, 0.0f);
	}
	glEnd();
}

void DrawGlasses(void)
{
	//thuglife glasses
	//left glass
	glBegin(GL_QUADS);
	{
		glColor3f(0.0f, 0.0f, 0.0f);

		glVertex3f(-0.02f, 0.73f, 0.0f);
		glVertex3f(-0.08f, 0.73f, 0.0f);
		glVertex3f(-0.08f, 0.68f, 0.0f);
		glVertex3f(-0.02f, 0.68f, 0.0f);

	}
	glEnd();
	//white squares
	glBegin(GL_QUADS);
	{
		glColor3f(1.0f, 1.0f, 1.0f);

		glVertex3f(-0.07f, 0.70f, 0.0f);
		glVertex3f(-0.08f, 0.70f, 0.0f);
		glVertex3f(-0.08f, 0.69f, 0.0f);
		glVertex3f(-0.07f, 0.69f, 0.0f);

	}
	glEnd();
	glBegin(GL_QUADS);
	{
		glColor3f(1.0f, 1.0f, 1.0f);

		glVertex3f(-0.05f, 0.70f, 0.0f);
		glVertex3f(-0.06f, 0.70f, 0.0f);
		glVertex3f(-0.06f, 0.69f, 0.0f);
		glVertex3f(-0.05f, 0.69f, 0.0f);

	}
	glEnd();
	glBegin(GL_QUADS);
	{
		glColor3f(1.0f, 1.0f, 1.0f);

		glVertex3f(-0.06f, 0.69f, 0.0f);
		glVertex3f(-0.07f, 0.69f, 0.0f);
		glVertex3f(-0.07f, 0.68f, 0.0f);
		glVertex3f(-0.06f, 0.68f, 0.0f);

	}
	glEnd();
	glBegin(GL_QUADS);
	{
		glColor3f(1.0f, 1.0f, 1.0f);

		glVertex3f(-0.04f, 0.69f, 0.0f);
		glVertex3f(-0.05f, 0.69f, 0.0f);
		glVertex3f(-0.05f, 0.68f, 0.0f);
		glVertex3f(-0.04f, 0.68f, 0.0f);
	}
	glEnd();

	//right glass
	glBegin(GL_QUADS);
	{
		glColor3f(0.0f, 0.0f, 0.0f);

		glVertex3f(0.02f, 0.73f, 0.0f);
		glVertex3f(0.08f, 0.73f, 0.0f);
		glVertex3f(0.08f, 0.68f, 0.0f);
		glVertex3f(0.02f, 0.68f, 0.0f);

	}
	glEnd();
	//white squares
	glBegin(GL_QUADS);
	{
		glColor3f(1.0f, 1.0f, 1.0f);

		glVertex3f(0.03f, 0.70f, 0.0f);
		glVertex3f(0.02f, 0.70f, 0.0f);
		glVertex3f(0.02f, 0.69f, 0.0f);
		glVertex3f(0.03f, 0.69f, 0.0f);

	}
	glEnd();
	glBegin(GL_QUADS);
	{
		glColor3f(1.0f, 1.0f, 1.0f);

		glVertex3f(0.05f, 0.70f, 0.0f);
		glVertex3f(0.04f, 0.70f, 0.0f);
		glVertex3f(0.04f, 0.69f, 0.0f);
		glVertex3f(0.05f, 0.69f, 0.0f);

	}
	glEnd();
	glBegin(GL_QUADS);
	{
		glColor3f(1.0f, 1.0f, 1.0f);

		glVertex3f(0.04f, 0.69f, 0.0f);
		glVertex3f(0.03f, 0.69f, 0.0f);
		glVertex3f(0.03f, 0.68f, 0.0f);
		glVertex3f(0.04f, 0.68f, 0.0f);

	}
	glEnd();
	glBegin(GL_QUADS);
	{
		glColor3f(1.0f, 1.0f, 1.0f);

		glVertex3f(0.06f, 0.69f, 0.0f);
		glVertex3f(0.05f, 0.69f, 0.0f);
		glVertex3f(0.05f, 0.68f, 0.0f);
		glVertex3f(0.06f, 0.68f, 0.0f);
	}
	glEnd();
	//nose point
	glBegin(GL_QUADS);
	{
		glColor3f(0.0f, 0.0f, 0.0f);

		glVertex3f(0.02f, 0.73f, 0.0f);
		glVertex3f(-0.02f, 0.73f, 0.0f);
		glVertex3f(-0.02f, 0.72f, 0.0f);
		glVertex3f(0.02f, 0.72f, 0.0f);
	}
	glEnd();


}

void DrawGuitarGuy(void)
{
	//shirt
	glBegin(GL_QUADS);
	{
		glColor3f(170.0f / 255.0f, 51.0f / 255.0f, 106.0f / 255.0f);

		glVertex3f(0.1f, 0.5f, 0.0f);
		glVertex3f(-0.1f, 0.5f, 0.0f);
		glVertex3f(-0.1f, 0.0f, 0.0f);
		glVertex3f(0.1f, 0.0f, 0.0f);
	}
	glEnd();



	//pants
	/*glBegin(GL_QUADS);
	{
		glColor3f(1.0f, 1.0f, 1.0f);

		glVertex3f(0.1f, 0.0f, 0.0f);
		glVertex3f(-0.1f, 0.0f, 0.0f);
		glVertex3f(-0.1f, -0.6f, 0.0f);
		glVertex3f(0.1f, -0.6f, 0.0f);

	}
	glEnd();*/

	glBegin(GL_QUADS);
	{
		glColor3f(1.0f, 1.0f, 1.0f);

		glVertex3f(0.1f, 0.0f, 0.0f);
		glVertex3f(-0.1f, 0.0f, 0.0f);
		glVertex3f(-0.1f, -0.1f, 0.0f);
		glVertex3f(0.1f, -0.1f, 0.0f);

	}
	glEnd();

	glBegin(GL_QUADS);
	{
		glColor3f(1.0f, 1.0f, 1.0f);

		glVertex3f(-0.01f, -0.1f, 0.0f);
		glVertex3f(-0.1f, -0.1f, 0.0f);
		glVertex3f(-0.1f, -0.6f, 0.0f);
		glVertex3f(-0.06f, -0.6f, 0.0f);


	}
	glEnd();

	glBegin(GL_QUADS);
	{
		glColor3f(1.0f, 1.0f, 1.0f);

		glVertex3f(0.1f, -0.1f, 0.0f);
		glVertex3f(0.01f, -0.1f, 0.0f);
		glVertex3f(0.06f, -0.6f, 0.0f);
		glVertex3f(0.1f, -0.6f, 0.0f);
	}
	glEnd();

	//neck
	glBegin(GL_QUADS);
	{
		glColor3f(255.0f / 255.0f, 232.0f / 255.0f, 201.0f / 255.0f);

		glVertex3f(0.04f, 0.53f, 0.0f);
		glVertex3f(-0.04f, 0.53f, 0.0f);
		glVertex3f(-0.04f, 0.5f, 0.0f);
		glVertex3f(0.04f, 0.5f, 0.0f);

	}
	glEnd();

	//bow
	glBegin(GL_TRIANGLES);
	{
		glColor3f(0.1f, 0.1f, 0.1f);

		glVertex3f(0.0f, 0.5f, 0.0f);
		glVertex3f(-0.06f, 0.52f, 0.0f);
		glVertex3f(-0.06f, 0.48f, 0.0f);
	}
	glEnd();

	glBegin(GL_TRIANGLES);
	{
		glColor3f(0.1f, 0.1f, 0.1f);

		glVertex3f(0.0f, 0.5f, 0.0f);
		glVertex3f(0.06f, 0.48f, 0.0f);
		glVertex3f(0.06f, 0.52f, 0.0f);

	}
	glEnd();

	//head
	float fAngle = 0.0f;
	float fRadx = 0.08f;
	float fRady = 0.1f;
	float fX = 0, fY = 0;

	glColor3f(255.0f / 255.0f, 232.0f / 255.0f, 201.0f / 255.0f);
	glBegin(GL_LINE_LOOP);
	for (fAngle = 0.0f; fAngle <= 360.0f; fAngle += 0.1f)
	{
		fX = cos(RAD(fAngle)) * fRadx;
		fY = sin(RAD(fAngle)) * fRady;


		glVertex3f(0.0f, 0.62f, 0.0f);
		glVertex3f(fX, fY + 0.62f, 0.0f);

	}
	glEnd();

	//hairs
	fRadx = 0.08f;
	fRady = 0.11f;
	glBegin(GL_POLYGON);
	{
		glColor3f(0.1f, 0.1f, 0.1f);
		for (fAngle = 20.0f; fAngle <= 160.0f; fAngle += 0.1f)
		{
			fX = cos(RAD(fAngle)) * fRadx;
			fY = sin(RAD(fAngle)) * fRady;

			glVertex3f(fX, fY + 0.62f, 0.0f);

		}

		/*glVertex3f(-0.05f, 0.8f, 0.0f);
		glVertex3f(-0.05f, 0.75f, 0.0f);

		glVertex3f(0.0f, 0.8f, 0.0f);
		glVertex3f(0.0f, 0.75f, 0.0f);

		glVertex3f(0.05f, 0.8f, 0.0f);
		glVertex3f(0.05f, 0.75f, 0.0f);*/

	}
	glEnd();




	//right hand top
	glBegin(GL_QUADS);
	{
		//glColor3f(255.0f / 255.0f, 232.0f / 255.0f, 201.0f / 255.0f);
		glColor3f(170.0f / 255.0f, 51.0f / 255.0f, 106.0f / 255.0f);

		glVertex3f(0.14f, 0.4f, 0.0f);
		glVertex3f(0.1f, 0.4f, 0.0f);
		glVertex3f(0.11f, 0.1f, 0.0f);
		glVertex3f(0.15f, 0.1f, 0.0f);
	}
	glEnd();
	glBegin(GL_TRIANGLES);
	{
		//glColor3f(255.0f / 255.0f, 232.0f / 255.0f, 201.0f / 255.0f);
		glColor3f(170.0f / 255.0f, 51.0f / 255.0f, 106.0f / 255.0f);

		glVertex3f(0.14f, 0.4f, 0.0f);
		glVertex3f(0.1f, 0.5f, 0.0f);
		glVertex3f(0.1f, 0.4f, 0.0f);

	}
	glEnd();

	//left hand shoulder
	glBegin(GL_TRIANGLES);
	{
		//glColor3f(255.0f / 255.0f, 232.0f / 255.0f, 201.0f / 255.0f);
		glColor3f(170.0f / 255.0f, 51.0f / 255.0f, 106.0f / 255.0f);

		glVertex3f(-0.1f, 0.5f, 0.0f);
		glVertex3f(-0.14f, 0.4f, 0.0f);
		glVertex3f(-0.1f, 0.4f, 0.0f);

	}
	glEnd();


	//shoe
	glBegin(GL_QUADS);
	{
		glColor3f(0.1f, 0.1f, 0.1f);

		glVertex3f(-0.05f, -0.60f, 0.0f);
		glVertex3f(-0.11f, -0.60f, 0.0f);
		glVertex3f(-0.11f, -0.65f, 0.0f);
		glVertex3f(-0.05f, -0.65f, 0.0f);

	}
	glEnd();
	glBegin(GL_QUADS);
	{
		glColor3f(0.1f, 0.1f, 0.1f);

		glVertex3f(0.11f, -0.60f, 0.0f);
		glVertex3f(0.05f, -0.60f, 0.0f);
		glVertex3f(0.05f, -0.65f, 0.0f);
		glVertex3f(0.11f, -0.65f, 0.0f);
	}
	glEnd();

}

void DrawRightHandMovement(void)
{

	//right hand movement over strings
	glBegin(GL_QUADS);
	{
		glColor3f(170.0f / 255.0f, 51.0f / 255.0f, 106.0f / 255.0f);

		glVertex3f(0.20f, 0.22f, 0.0f);
		glVertex3f(0.16f, 0.22f, 0.0f);
		glVertex3f(0.11f, 0.1f, 0.0f);
		glVertex3f(0.15f, 0.1f, 0.0f);
	}
	glEnd();
}

void DrawLeftHand(void)
{
	// left hand over guitar base
	glBegin(GL_QUADS);
	{
		//glColor3f(255.0f / 255.0f, 232.0f / 255.0f, 201.0f / 255.0f);
		glColor3f(170.0f / 255.0f, 51.0f / 255.0f, 106.0f / 255.0f);

		glVertex3f(-0.1f, 0.4f, 0.0f);
		glVertex3f(-0.14f, 0.4f, 0.0f);

		glVertex3f(-0.15f, 0.0f, 0.0f);
		glVertex3f(-0.11f, 0.0f, 0.0f);

	}
	glEnd();



}

void DrawRoad(void)
{
	//main road
	glColor3f(52.0f / 255.0f, 52.0f / 255.0f, 52.0f / 255.0f);
	glBegin(GL_QUADS);
	{
		glVertex3f(1.0f, 1.0f, 0.0f);
		glVertex3f(0.2f, 1.0f, 0.0f);
		glVertex3f(0.2f, -1.0f, 0.0f);
		glVertex3f(1.0f, -1.0f, 0.0f);
	}
	glEnd();

	//road strips
	glColor3f(1.0f, 1.0f, 1.0f);
	glBegin(GL_QUADS);
	{
		glVertex3f(0.62f, 1.0f, 0.0f);
		glVertex3f(0.58f, 1.0f, 0.0f);
		glVertex3f(0.58f, 0.8f, 0.0f);
		glVertex3f(0.62f, 0.8f, 0.0f);
	}
	glEnd();

	glBegin(GL_QUADS);
	{
		glVertex3f(0.62f, 0.4f, 0.0f);
		glVertex3f(0.58f, 0.4f, 0.0f);
		glVertex3f(0.58f, 0.0f, 0.0f);
		glVertex3f(0.62f, 0.0f, 0.0f);
	}
	glEnd();

	glBegin(GL_QUADS);
	{
		glVertex3f(0.62f, -0.4f, 0.0f);
		glVertex3f(0.58f, -0.4f, 0.0f);
		glVertex3f(0.58f, -0.8f, 0.0f);
		glVertex3f(0.62f, -0.8f, 0.0f);
	}
	glEnd();


}

void DrawFootPath(void)
{
	//footpath
	glColor3f(128.0f / 255.0f, 128.0f / 255.0f, 128.0f / 255.0f);
	glBegin(GL_QUADS);
	{
		glVertex3f(0.2f, 1.0f, 0.0f);
		glVertex3f(0.0f, 1.0f, 0.0f);
		glVertex3f(0.0f, -1.0f, 0.0f);
		glVertex3f(0.2f, -1.0f, 0.0f);
	}
	glEnd();

	//shade
	glColor3f(115.f / 255.0f, 115.0f / 255.0f, 120.0f / 255.0f);
	glBegin(GL_QUADS);
	{
		glVertex3f(0.05f, 1.0f, 0.0f);
		glVertex3f(0.0f, 1.0f, 0.0f);
		glVertex3f(0.0f, -1.0f, 0.0f);
		glVertex3f(0.05f, -1.0f, 0.0f);
	}
	glEnd();
}

void DrawSea(void)
{

	glBegin(GL_QUADS);
	{

		glColor3f(77.0f / 255.0f, 96.0f / 255.0f, 167.0f / 255.0f);
		glVertex3f(0.0f, 1.0f, 0.0f);

		//glColor3f(6.0f / 255.0f, 7.0f / 255.0f, 45.0f / 255.0f);
		glColor3f(7.0f / 255.0f, 8.0f / 255.0f, 100.0f / 255.0f);
		glVertex3f(-1.0f, 1.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 0.0f);


		glColor3f(77.0f / 255.0f, 96.0f / 255.0f, 167.0f / 255.0f);
		glVertex3f(0.0f, -1.0f, 0.0f);
	}
	glEnd();

	glBegin(GL_QUADS);
	{
		glColor3f(44.0f / 255.0f, 76.0f / 255.0f, 144.0f / 255.0f);
		glVertex3f(-0.2f, 1.0f, 0.0f);

		glColor3f(8.0f / 255.0f, 10.0f / 255.0f, 116.0f / 255.0f);
		glVertex3f(-0.6f, 1.0f, 0.0f);
		glVertex3f(-0.6f, -1.0f, 0.0f);

		glColor3f(44.0f / 255.0f, 76.0f / 255.0f, 144.0f / 255.0f);
		glVertex3f(-0.2f, -1.0f, 0.0f);
	}
	glEnd();
}

void DrawCar(void)
{
	//chasi middle
	glColor3f(222.0f / 255.0f, 55.0f / 255.0f, 255.0f / 255.0f);
	glBegin(GL_QUADS);
	{
		glVertex3f(0.94f, 0.2f, 0.0f);
		glVertex3f(0.64f, 0.2f, 0.0f);
		glVertex3f(0.64f, -0.2f, 0.0f);
		glVertex3f(0.94f, -0.2f, 0.0f);
	}
	glEnd();
	//chaso front
	glBegin(GL_POLYGON);
	{
		glVertex3f(0.90f, 0.30f, 0.0f);
		glVertex3f(0.68f, 0.30f, 0.0f);

		glVertex3f(0.64f, 0.29f, 0.0f);
		glVertex3f(0.64f, 0.2f, 0.0f);

		glVertex3f(0.94f, 0.2f, 0.0f);
		glVertex3f(0.94f, 0.29f, 0.0f);


	}
	glEnd();
	//chasi back
	glBegin(GL_POLYGON);
	{
		glVertex3f(0.94f, -0.2f, 0.0f);
		glVertex3f(0.64f, -0.2f, 0.0f);

		glVertex3f(0.64f, -0.23f, 0.0f);
		glVertex3f(0.66f, -0.25f, 0.0f);

		glVertex3f(0.92f, -0.25f, 0.0f);
		glVertex3f(0.94f, -0.23f, 0.0f);
	}
	glEnd();

	//windshields
	//front
	glColor3f(0.1f, 0.1f, 0.1f);
	glBegin(GL_QUADS);
	{
		glVertex3f(0.92f, 0.19f, 0.0f);
		glVertex3f(0.66f, 0.19f, 0.0f);
		glVertex3f(0.68f, 0.1f, 0.0f);
		glVertex3f(0.90f, 0.1f, 0.0f);
	}
	glEnd();

	//left
	glColor3f(0.1f, 0.1f, 0.1f);
	glBegin(GL_QUADS);
	{
		glVertex3f(0.67f, 0.1f, 0.0f);
		glVertex3f(0.65f, 0.19f, 0.0f);
		glVertex3f(0.65f, -0.19f, 0.0f);
		glVertex3f(0.67f, -0.1f, 0.0f);
	}
	glEnd();

	//right
	glColor3f(0.1f, 0.1f, 0.1f);
	glBegin(GL_QUADS);
	{
		glVertex3f(0.93f, 0.19f, 0.0f);
		glVertex3f(0.91f, 0.1f, 0.0f);
		glVertex3f(0.91f, -0.1f, 0.0f);
		glVertex3f(0.93f, -0.19f, 0.0f);
	}
	glEnd();

	//back
	glColor3f(0.1f, 0.1f, 0.1f);
	glBegin(GL_QUADS);
	{
		glVertex3f(0.92f, -0.19f, 0.0f);
		glVertex3f(0.90f, -0.1f, 0.0f);
		glVertex3f(0.68f, -0.1f, 0.0f);
		glVertex3f(0.66f, -0.19f, 0.0f);
	}
	glEnd();

	//headlight left
	glBegin(GL_QUADS);
	{
		glColor3f(52.0f / 255.0f, 52.0f / 255.0f, 52.0f / 255.0f);
		glVertex3f(0.78f, 0.5f, 0.0f);
		glVertex3f(0.63f, 0.5f, 0.0f);

		glColor3f(0.8f, 0.8f, 0.0f);
		glVertex3f(0.68f, 0.3f, 0.0f);
		glVertex3f(0.72f, 0.3f, 0.0f);
	}
	glEnd();

	//headlight right
	glBegin(GL_QUADS);
	{
		glColor3f(52.0f / 255.0f, 52.0f / 255.0f, 52.0f / 255.0f);
		glVertex3f(0.95f, 0.5f, 0.0f);
		glVertex3f(0.80f, 0.5f, 0.0f);

		glColor3f(0.8f, 0.8f, 0.0f);
		glVertex3f(0.86f, 0.3f, 0.0f);
		glVertex3f(0.90f, 0.3f, 0.0f);
	}
	glEnd();

	//tail light
	glBegin(GL_QUADS);
	{
		//glColor3f(1.0f, 0.1f, 0.0f);
		glColor3f(sCarBrake.r, sCarBrake.g, sCarBrake.b);
		glVertex3f(0.93f, -0.23f, 0.0f);
		glVertex3f(0.88f, -0.23f, 0.0f);
		glVertex3f(0.88f, -0.24f, 0.0f);
		glVertex3f(0.93f, -0.24f, 0.0f);
	}
	glEnd();

	glBegin(GL_QUADS);
	{
		// glColor3f(1.0f, 0.1f, 0.0f);
		glColor3f(sCarBrake.r, sCarBrake.g, sCarBrake.b);
		glVertex3f(0.70f, -0.23f, 0.0f);
		glVertex3f(0.65f, -0.23f, 0.0f);
		glVertex3f(0.65f, -0.24f, 0.0f);
		glVertex3f(0.70f, -0.24f, 0.0f);
	}
	glEnd();
}

void theatreBuilding(void)
{


	glBegin(GL_QUADS); // Building for Theatre
	glColor3f(255.0f / 255.0f, 230.0f / 255.0f, 145.0f / 255.0f);

	glVertex3f(0.8f, -0.2f, 0.0f);
	glVertex3f(0.8f, 0.5f, 0.0f);
	glVertex3f(-0.8f, 0.5f, 0.0f);
	glVertex3f(-0.8f, -0.2f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); //  Theatre Front Dark Area
	glColor3f(214.0f / 255.0f, 138.0f / 255.0f, 79.0f / 255.0f);

	glVertex3f(0.35f, -0.2f, 0.0f);
	glVertex3f(0.35f, 0.5f, 0.0f);
	glVertex3f(-0.35f, 0.5f, 0.0f);
	glVertex3f(-0.35f, -0.2f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); //  Theatre rIGHT UPPER WINDOW
	glColor3f(212.0f / 255.0f, 191.0f / 255.0f, 120.0f / 255.0f);

	glVertex3f(0.7f, 0.2f, 0.0f);
	glVertex3f(0.7f, 0.4f, 0.0f);
	glVertex3f(0.5f, 0.4f, 0.0f);
	glVertex3f(0.5f, 0.2f, 0.0f);
	glEnd();

	glLineWidth(2.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.6f, 0.4f, 0.0f);
	glVertex3f(0.6, 0.2f, 0.0f);
	glEnd();

	glLineWidth(2.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.5f, 0.3f, 0.0f);
	glVertex3f(0.7, 0.3f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); //  Theatre rIGHT Down WINDOW
	glColor3f(212.0f / 255.0f, 191.0f / 255.0f, 120.0f / 255.0f);

	glVertex3f(0.7f, -0.1f, 0.0f);
	glVertex3f(0.7f, 0.1f, 0.0f);
	glVertex3f(0.5f, 0.1f, 0.0f);
	glVertex3f(0.5f, -0.1f, 0.0f);
	glEnd();

	glLineWidth(2.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.6f, 0.1f, 0.0f);
	glVertex3f(0.6, -0.1f, 0.0f);
	glEnd();

	glLineWidth(2.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.5f, 0.0f, 0.0f);
	glVertex3f(0.7, 0.0f, 0.0f);
	glEnd();


	glBegin(GL_QUADS); //  Theatre LEFT UPPER WINDOW
	glColor3f(212.0f / 255.0f, 191.0f / 255.0f, 120.0f / 255.0f);

	glVertex3f(-0.7f, 0.2f, 0.0f);
	glVertex3f(-0.7f, 0.4f, 0.0f);
	glVertex3f(-0.5f, 0.4f, 0.0f);
	glVertex3f(-0.5f, 0.2f, 0.0f);
	glEnd();

	glLineWidth(2.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.6f, 0.4f, 0.0f);
	glVertex3f(-0.6, 0.2f, 0.0f);
	glEnd();

	glLineWidth(2.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.5f, 0.3f, 0.0f);
	glVertex3f(-0.7, 0.3f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); //  Theatre LEFT Down WINDOW
	glColor3f(212.0f / 255.0f, 191.0f / 255.0f, 120.0f / 255.0f);

	glVertex3f(-0.7f, -0.1f, 0.0f);
	glVertex3f(-0.7f, 0.1f, 0.0f);
	glVertex3f(-0.5f, 0.1f, 0.0f);
	glVertex3f(-0.5f, -0.1f, 0.0f);
	glEnd();

	glLineWidth(2.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.6f, 0.1f, 0.0f);
	glVertex3f(-0.6, -0.1f, 0.0f);
	glEnd();

	glLineWidth(2.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.5f, 0.0f, 0.0f);
	glVertex3f(-0.7, 0.0f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); // Theatre Ceiling
	glColor3f(212.0f / 255.0f, 191.0f / 255.0f, 120.0f / 255.0f);

	glVertex3f(0.85f, 0.5f, 0.0f);
	glVertex3f(0.85f, 0.55f, 0.0f);
	glVertex3f(-0.85f, 0.55f, 0.0f);
	glVertex3f(-0.85f, 0.5f, 0.0f);
	glEnd();

	glBegin(GL_TRIANGLES); // Theatre Ceiling Upper Triangle
	glColor3f(255.0f / 255.0f, 230.0f / 255.0f, 145.0f / 255.0f);

	glVertex3f(0.0f, 0.9f, 0.0f);
	glVertex3f(-0.8f, 0.55f, 0.0f);
	glVertex3f(0.8f, 0.55f, 0.0f);

	glEnd();


	glBegin(GL_QUADS); //  Theatre Door BG
	glColor3f(1.0f, 1.0f, 1.0f);

	glVertex3f(0.1f, -0.2f, 0.0f);
	glVertex3f(0.1f, 0.0f, 0.0f);
	glVertex3f(-0.1f, 0.0f, 0.0f);
	glVertex3f(-0.1f, -0.2f, 0.0f);
	glEnd();

	glLineWidth(3.0f);
	glBegin(GL_LINE_LOOP); // Theatre Door BG Border
	glColor3f(196.0f / 255.0f, 110.0f / 255.0f, 34.0f / 255.0f);
	glVertex3f(0.1f, -0.2f, 0.0f);
	glVertex3f(0.1f, 0.0f, 0.0f);
	glVertex3f(-0.1f, 0.0f, 0.0f);
	glVertex3f(-0.1f, -0.2f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); //  Theatre Door Left
	glColor3f(212.0f / 255.0f, 191.0f / 255.0f, 120.0f / 255.0f);

	glVertex3f(-0.02f, -0.18f, 0.0f);
	glVertex3f(-0.02f, -0.02f, 0.0f);
	glVertex3f(-0.1f, 0.0f, 0.0f);
	glVertex3f(-0.1f, -0.2f, 0.0f);
	glEnd();

	glLineWidth(2.0f);
	glBegin(GL_LINE_LOOP); //  Theatre Door Left Border
	glColor3f(196.0f / 255.0f, 110.0f / 255.0f, 34.0f / 255.0f);
	glVertex3f(-0.02f, -0.18f, 0.0f);
	glVertex3f(-0.02f, -0.02f, 0.0f);
	glVertex3f(-0.1f, 0.0f, 0.0f);
	glVertex3f(-0.1f, -0.2f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); //  Theatre Door Right
	glColor3f(212.0f / 255.0f, 191.0f / 255.0f, 120.0f / 255.0f);

	glVertex3f(0.02f, -0.18f, 0.0f);
	glVertex3f(0.02f, -0.02f, 0.0f);
	glVertex3f(0.1f, 0.0f, 0.0f);
	glVertex3f(0.1f, -0.2f, 0.0f);
	glEnd();

	glLineWidth(2.0f);
	glBegin(GL_LINE_LOOP); //  Theatre Door Right Border
	glColor3f(196.0f / 255.0f, 110.0f / 255.0f, 34.0f / 255.0f);
	glVertex3f(0.02f, -0.18f, 0.0f);
	glVertex3f(0.02f, -0.02f, 0.0f);
	glVertex3f(0.1f, 0.0f, 0.0f);
	glVertex3f(0.1f, -0.2f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); //  Theatre Door Step
	glColor3f(212.0f / 255.0f, 191.0f / 255.0f, 120.0f / 255.0f);

	glVertex3f(0.2f, -0.22f, 0.0f);
	glVertex3f(0.2f, -0.2f, 0.0f);
	glVertex3f(-0.2f, -0.2f, 0.0f);
	glVertex3f(-0.2f, -0.22f, 0.0f);
	glEnd();



	glBegin(GL_QUADS); //  Theatre Door Right Side Pillar 1
	glColor3f(255.0f / 255.0f, 230.0f / 255.0f, 145.0f / 255.0f);


	glVertex3f(0.3f, -0.2f, 0.0f);

	glVertex3f(0.3f, 0.5f, 0.0f);
	glVertex3f(0.25f, 0.5f, 0.0f);

	glVertex3f(0.25f, -0.2f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); // Theatre Door Right Side Pillar 1 DOWNSTEP
	glColor3f(255.0f / 255.0f, 230.0f / 255.0f, 145.0f / 255.0f);


	glVertex3f(0.31f, -0.2f, 0.0f);

	glVertex3f(0.31f, -0.18f, 0.0f);
	glVertex3f(0.24f, -0.18f, 0.0f);

	glVertex3f(0.24f, -0.2f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); //  Theatre Door Right Side Pillar 1 UPSTEP
	glColor3f(255.0f / 255.0f, 230.0f / 255.0f, 145.0f / 255.0f);


	glVertex3f(0.31f, 0.5f, 0.0f);

	glVertex3f(0.24f, 0.5f, 0.0f);
	glVertex3f(0.24f, 0.48f, 0.0f);

	glVertex3f(0.31f, 0.48f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); //  Theatre Door Right Side Pillar 2
	glColor3f(255.0f / 255.0f, 230.0f / 255.0f, 145.0f / 255.0f);


	glVertex3f(0.2f, -0.2f, 0.0f);

	glVertex3f(0.2f, 0.5f, 0.0f);
	glVertex3f(0.15f, 0.5f, 0.0f);

	glVertex3f(0.15f, -0.2f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); //  Theatre Door Right Side Pillar 2 UPSTEP
	glColor3f(255.0f / 255.0f, 230.0f / 255.0f, 145.0f / 255.0f);


	glVertex3f(0.21f, 0.5f, 0.0f);

	glVertex3f(0.14f, 0.5f, 0.0f);
	glVertex3f(0.14f, 0.48f, 0.0f);

	glVertex3f(0.21f, 0.48f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); // Theatre Door Right Side Pillar 2 DOWNSTEP
	glColor3f(255.0f / 255.0f, 230.0f / 255.0f, 145.0f / 255.0f);


	glVertex3f(0.21f, -0.2f, 0.0f);

	glVertex3f(0.21f, -0.18f, 0.0f);
	glVertex3f(0.14f, -0.18f, 0.0f);

	glVertex3f(0.14f, -0.2f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); //  Theatre Door Right Side Pillar 3
	glColor3f(255.0f / 255.0f, 230.0f / 255.0f, 145.0f / 255.0f);


	glVertex3f(0.1f, 0.1f, 0.0f);

	glVertex3f(0.1f, 0.5f, 0.0f);
	glVertex3f(0.05f, 0.5f, 0.0f);

	glVertex3f(0.05f, 0.1f, 0.0f);
	glEnd();



	glBegin(GL_QUADS); //  Theatre Door Left Side Pillar 1
	glColor3f(255.0f / 255.0f, 230.0f / 255.0f, 145.0f / 255.0f);


	glVertex3f(-0.3f, -0.2f, 0.0f);

	glVertex3f(-0.3f, 0.5f, 0.0f);
	glVertex3f(-0.25f, 0.5f, 0.0f);

	glVertex3f(-0.25f, -0.2f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); //  Theatre Door Left Side Pillar 1 DOWNSTEP
	glColor3f(255.0f / 255.0f, 230.0f / 255.0f, 145.0f / 255.0f);


	glVertex3f(-0.31f, -0.2f, 0.0f);

	glVertex3f(-0.31f, -0.18f, 0.0f);
	glVertex3f(-0.24f, -0.18f, 0.0f);

	glVertex3f(-0.24f, -0.2f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); // Theatre Door Left Side Pillar 1 UPSTEP
	glColor3f(255.0f / 255.0f, 230.0f / 255.0f, 145.0f / 255.0f);


	glVertex3f(-0.31f, 0.5f, 0.0f);

	glVertex3f(-0.24f, 0.5f, 0.0f);
	glVertex3f(-0.24f, 0.48f, 0.0f);

	glVertex3f(-0.31f, 0.48f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); //  Theatre Door Left Side Pillar 2
	glColor3f(255.0f / 255.0f, 230.0f / 255.0f, 145.0f / 255.0f);


	glVertex3f(-0.2f, -0.2f, 0.0f);

	glVertex3f(-0.2f, 0.5f, 0.0f);
	glVertex3f(-0.15f, 0.5f, 0.0f);

	glVertex3f(-0.15f, -0.2f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); // Theatre Door Left Side Pillar 2 UPSTEP
	glColor3f(255.0f / 255.0f, 230.0f / 255.0f, 145.0f / 255.0f);


	glVertex3f(-0.21f, 0.5f, 0.0f);

	glVertex3f(-0.14f, 0.5f, 0.0f);
	glVertex3f(-0.14f, 0.48f, 0.0f);

	glVertex3f(-0.21f, 0.48f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); // Theatre Door Left Side Pillar 2 DOWNSTEP
	glColor3f(255.0f / 255.0f, 230.0f / 255.0f, 145.0f / 255.0f);


	glVertex3f(-0.21f, -0.2f, 0.0f);

	glVertex3f(-0.21f, -0.18f, 0.0f);
	glVertex3f(-0.14f, -0.18f, 0.0f);

	glVertex3f(-0.14f, -0.2f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); //  Theatre Door Left Side Pillar 3
	glColor3f(255.0f / 255.0f, 230.0f / 255.0f, 145.0f / 255.0f);


	glVertex3f(-0.1f, 0.1f, 0.0f);

	glVertex3f(-0.1f, 0.5f, 0.0f);
	glVertex3f(-0.05f, 0.5f, 0.0f);

	glVertex3f(-0.05f, 0.1f, 0.0f);
	glEnd();

	//--------------------NAMING THE THEATRE----------------------

	//---------------------AMC---------------------------------

	glBegin(GL_TRIANGLES); // A
	glColor3f(212.0f / 255.0f, 191.0f / 255.0f, 120.0f / 255.0f);

	glVertex3f(-0.15f, 0.8f, 0.0f);
	glVertex3f(-0.2f, 0.75f, 0.0f);
	glVertex3f(-0.1f, 0.75f, 0.0f);

	glEnd();

	glBegin(GL_TRIANGLES); // left M
	glColor3f(212.0f / 255.0f, 191.0f / 255.0f, 120.0f / 255.0f);

	glVertex3f(-0.05f, 0.8f, 0.0f);
	glVertex3f(-0.05f, 0.75f, 0.0f);
	glVertex3f(0.0f, 0.75f, 0.0f);

	glEnd();

	glBegin(GL_TRIANGLES); // Right M
	glColor3f(212.0f / 255.0f, 191.0f / 255.0f, 120.0f / 255.0f);

	glVertex3f(0.05f, 0.8f, 0.0f);
	glVertex3f(0.05f, 0.75f, 0.0f);
	glVertex3f(0.0f, 0.75f, 0.0f);

	glEnd();

	glBegin(GL_QUADS); // C 
	glColor3f(212.0f / 255.0f, 191.0f / 255.0f, 120.0f / 255.0f);

	glVertex3f(0.1f, 0.75f, 0.0f);
	glVertex3f(0.2f, 0.75f, 0.0f);
	glVertex3f(0.2f, 0.8f, 0.0f);
	glVertex3f(0.1f, 0.8f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glColor3f(255.0f / 255.0f, 230.0f / 255.0f, 145.0f / 255.0f);
	glVertex3f(0.2f, 0.77f, 0.0f);
	glVertex3f(0.2f, 0.78f, 0.0f);
	glVertex3f(0.17f, 0.78f, 0.0f);
	glVertex3f(0.17f, 0.77f, 0.0f);

	glEnd();

	//--------------------NATYAGRUH---------------

	//-------------------N-------------------------
	glLineWidth(2.0f);
	glBegin(GL_LINES);  // N
	glColor3f(212.0f / 255.0f, 191.0f / 255.0f, 120.0f / 255.0f);
	glVertex3f(-0.4f, 0.6f, 0.0f);
	glVertex3f(-0.4, 0.65f, 0.0f);
	glEnd();

	glLineWidth(2.0f);
	glBegin(GL_LINES);  // N
	glColor3f(212.0f / 255.0f, 191.0f / 255.0f, 120.0f / 255.0f);
	glVertex3f(-0.4, 0.65f, 0.0f);
	glVertex3f(-0.35, 0.6f, 0.0f);
	glEnd();

	glLineWidth(2.0f);
	glBegin(GL_LINES);  // N
	glColor3f(212.0f / 255.0f, 191.0f / 255.0f, 120.0f / 255.0f);
	glVertex3f(-0.35, 0.6f, 0.0f);
	glVertex3f(-0.35, 0.65f, 0.0f);
	glEnd();

	//-----------------A----------------------------
	glLineWidth(2.0f);
	glBegin(GL_LINE_LOOP);  //A
	glColor3f(212.0f / 255.0f, 191.0f / 255.0f, 120.0f / 255.0f);
	glVertex3f(-0.3, 0.6f, 0.0f);
	glVertex3f(-0.25, 0.6f, 0.0f);
	glVertex3f(-0.27, 0.65f, 0.0f);
	glEnd();


	//-----------------T----------------------------
	glLineWidth(2.0f);
	glBegin(GL_LINES);  //T
	glColor3f(212.0f / 255.0f, 191.0f / 255.0f, 120.0f / 255.0f);
	glVertex3f(-0.2, 0.65f, 0.0f);
	glVertex3f(-0.15, 0.65f, 0.0f);
	glEnd();

	glLineWidth(2.0f);
	glBegin(GL_LINES);  //T
	glColor3f(212.0f / 255.0f, 191.0f / 255.0f, 120.0f / 255.0f);
	glVertex3f(-0.18, 0.6f, 0.0f);
	glVertex3f(-0.18, 0.65f, 0.0f);
	glEnd();

	//-----------------Y----------------------------
	glLineWidth(2.0f);
	glBegin(GL_LINES);  //Y
	glColor3f(212.0f / 255.0f, 191.0f / 255.0f, 120.0f / 255.0f);
	glVertex3f(-0.1, 0.65f, 0.0f);
	glVertex3f(-0.08, 0.63f, 0.0f);
	glEnd();

	glLineWidth(2.0f);
	glBegin(GL_LINES);  //Y
	glColor3f(212.0f / 255.0f, 191.0f / 255.0f, 120.0f / 255.0f);
	glVertex3f(-0.08, 0.63f, 0.0f);
	glVertex3f(-0.05, 0.65f, 0.0f);
	glEnd();

	glLineWidth(2.0f);
	glBegin(GL_LINES);  //Y
	glColor3f(212.0f / 255.0f, 191.0f / 255.0f, 120.0f / 255.0f);
	glVertex3f(-0.08, 0.63f, 0.0f);
	glVertex3f(-0.08, 0.6f, 0.0f);
	glEnd();

	//-----------------A----------------------------
	glLineWidth(2.0f);
	glBegin(GL_LINE_LOOP);  //A
	glColor3f(212.0f / 255.0f, 191.0f / 255.0f, 120.0f / 255.0f);
	glVertex3f(0.0, 0.6f, 0.0f);
	glVertex3f(0.05, 0.6f, 0.0f);
	glVertex3f(0.03, 0.65f, 0.0f);
	glEnd();

	//-----------------G----------------------------
	glLineWidth(2.0f);
	glBegin(GL_LINES);  //G
	glColor3f(212.0f / 255.0f, 191.0f / 255.0f, 120.0f / 255.0f);
	glVertex3f(0.15, 0.65f, 0.0f);
	glVertex3f(0.1, 0.65f, 0.0f);
	glEnd();

	glLineWidth(2.0f);
	glBegin(GL_LINES);  //G
	glColor3f(212.0f / 255.0f, 191.0f / 255.0f, 120.0f / 255.0f);
	glVertex3f(0.1, 0.65f, 0.0f);
	glVertex3f(0.1, 0.6f, 0.0f);
	glEnd();

	glLineWidth(2.0f);
	glBegin(GL_LINES);  //G
	glColor3f(212.0f / 255.0f, 191.0f / 255.0f, 120.0f / 255.0f);
	glVertex3f(0.1, 0.6f, 0.0f);
	glVertex3f(0.15, 0.6f, 0.0f);
	glEnd();

	glLineWidth(2.0f);
	glBegin(GL_LINES);  //G
	glColor3f(212.0f / 255.0f, 191.0f / 255.0f, 120.0f / 255.0f);
	glVertex3f(0.15, 0.6f, 0.0f);
	glVertex3f(0.15, 0.62f, 0.0f);
	glEnd();

	glLineWidth(2.0f);
	glBegin(GL_LINES);  //G
	glColor3f(212.0f / 255.0f, 191.0f / 255.0f, 120.0f / 255.0f);
	glVertex3f(0.14, 0.62f, 0.0f);
	glVertex3f(0.16, 0.62f, 0.0f);
	glEnd();


	//-------------------R------------------
	glLineWidth(2.0f);
	glBegin(GL_LINE_LOOP);  //R
	glColor3f(212.0f / 255.0f, 191.0f / 255.0f, 120.0f / 255.0f);
	glVertex3f(0.2, 0.6f, 0.0f);
	glVertex3f(0.2, 0.65f, 0.0f);
	glVertex3f(0.25, 0.65f, 0.0f);
	glVertex3f(0.25, 0.62f, 0.0f);
	glVertex3f(0.2, 0.62f, 0.0f);
	glEnd();

	glLineWidth(2.0f);
	glBegin(GL_LINES);  //R
	glColor3f(212.0f / 255.0f, 191.0f / 255.0f, 120.0f / 255.0f);
	glVertex3f(0.2, 0.62f, 0.0f);
	glVertex3f(0.25, 0.6f, 0.0f);
	glEnd();


	//------------------U----------------------
	glLineWidth(2.0f);
	glBegin(GL_LINES);  //U
	glColor3f(212.0f / 255.0f, 191.0f / 255.0f, 120.0f / 255.0f);
	glVertex3f(0.3, 0.65f, 0.0f);
	glVertex3f(0.3, 0.6f, 0.0f);
	glEnd();

	glLineWidth(2.0f);
	glBegin(GL_LINES);  //U
	glColor3f(212.0f / 255.0f, 191.0f / 255.0f, 120.0f / 255.0f);
	glVertex3f(0.3, 0.6f, 0.0f);
	glVertex3f(0.35, 0.6f, 0.0f);
	glEnd();

	glLineWidth(2.0f);
	glBegin(GL_LINES);  //U
	glColor3f(212.0f / 255.0f, 191.0f / 255.0f, 120.0f / 255.0f);
	glVertex3f(0.35, 0.6f, 0.0f);
	glVertex3f(0.35, 0.65f, 0.0f);
	glEnd();

	//------------------H----------------------
	glLineWidth(2.0f);
	glBegin(GL_LINES);  //H
	glColor3f(212.0f / 255.0f, 191.0f / 255.0f, 120.0f / 255.0f);
	glVertex3f(0.45, 0.6f, 0.0f);
	glVertex3f(0.45, 0.65f, 0.0f);
	glEnd();

	glLineWidth(2.0f);
	glBegin(GL_LINES);  //H
	glColor3f(212.0f / 255.0f, 191.0f / 255.0f, 120.0f / 255.0f);
	glVertex3f(0.4, 0.62f, 0.0f);
	glVertex3f(0.45, 0.62f, 0.0f);
	glEnd();

	glLineWidth(2.0f);
	glBegin(GL_LINES);  //H
	glColor3f(212.0f / 255.0f, 191.0f / 255.0f, 120.0f / 255.0f);
	glVertex3f(0.4, 0.6f, 0.0f);
	glVertex3f(0.4, 0.65f, 0.0f);
	glEnd();


}

void backgroundScene_2(void)
{
	glBegin(GL_QUADS);  // Base Green

	glColor3f(90.0f / 255.0f, 255.0f / 255.0f, 50.0f / 255.0f);
	glVertex3f(1.0f, -1.0f, 0.0f);
	glVertex3f(1.0f, -0.2f, 0.0f);
	glVertex3f(-1.0f, -0.2f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);  // Road Vertical
	glColor3f(99.0f / 255.0f, 99.0f / 255.0f, 99.0f / 255.0f);
	glVertex3f(0.35f, -1.0f, 0.0f);
	glVertex3f(0.15f, -0.2f, 0.0f);
	glVertex3f(-0.15f, -0.2f, 0.0f);
	glVertex3f(-0.35f, -1.0f, 0.0f);

	glEnd();

	glLineWidth(2.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.0f, -0.2f, 0.0f);
	glVertex3f(0.0f, -1.0f, 0.0f);

	glEnd();

	glBegin(GL_QUADS);
	{
		{
			// First Trapazium for tree 1

			glColor3f(0.1f, 0.6f, 0.0f);
			glVertex3f(-0.65f, -0.5f, 0.0f);
			glColor3f(0.1f, 0.7f, 0.0f);
			glVertex3f(-0.75f, -0.5f, 0.0f);
			glColor3f(0.1f, 0.8f, 0.0f);
			glVertex3f(-0.85f, -0.6f, 0.0f);
			glColor3f(0.1f, 0.9f, 0.0f);
			glVertex3f(-0.55f, -0.6f, 0.0f);
		}
		{
			// 2 Trapazium for tree 1

			glColor3f(0.1f, 0.6f, 0.0f);
			glVertex3f(-0.65f, -0.6f, 0.0f);
			glColor3f(0.1f, 0.7f, 0.0f);
			glVertex3f(-0.75f, -0.6f, 0.0f);
			glColor3f(0.1f, 0.8f, 0.0f);
			glVertex3f(-0.85f, -0.7f, 0.0f);
			glColor3f(0.1f, 0.9f, 0.0f);
			glVertex3f(-0.55f, -0.7f, 0.0f);
		}
		{
			// 3 Trapazium for tree 1

			glColor3f(0.1f, 0.6f, 0.0f);
			glVertex3f(-0.65f, -0.7f, 0.0f);
			glColor3f(0.1f, 0.7f, 0.0f);
			glVertex3f(-0.75f, -0.7f, 0.0f);
			glColor3f(0.1f, 0.8f, 0.0f);
			glVertex3f(-0.85f, -0.8f, 0.0f);
			glColor3f(0.1f, 0.9f, 0.0f);
			glVertex3f(-0.55f, -0.8f, 0.0f);
		}

		{
			// Base of tree 1

			glColor3f(0.5f, 0.2f, 0.0f);
			glVertex3f(-0.68f, -0.8f, 0.0f);
			glColor3f(0.7f, 0.3f, 0.0f);
			glVertex3f(-0.72f, -0.8f, 0.0f);
			glColor3f(0.7f, 0.3f, 0.1f);
			glVertex3f(-0.72f, -0.9f, 0.0f);
			glColor3f(0.8f, 0.4f, 0.0f);
			glVertex3f(-0.68f, -0.9f, 0.0f);
		}


	}
	{
		{
			// First Trapazium for tree 2

			glColor3f(0.1f, 0.6f, 0.0f);
			glVertex3f(0.65f, -0.5f, 0.0f);
			glColor3f(0.1f, 0.7f, 0.0f);
			glVertex3f(0.75f, -0.5f, 0.0f);
			glColor3f(0.1f, 0.8f, 0.0f);
			glVertex3f(0.85f, -0.6f, 0.0f);
			glColor3f(0.1f, 0.9f, 0.0f);
			glVertex3f(0.55f, -0.6f, 0.0f);
		}
		{
			// 2 Trapazium for tree 2

			glColor3f(0.1f, 0.6f, 0.0f);
			glVertex3f(0.65f, -0.6f, 0.0f);
			glColor3f(0.1f, 0.7f, 0.0f);
			glVertex3f(0.75f, -0.6f, 0.0f);
			glColor3f(0.1f, 0.8f, 0.0f);
			glVertex3f(0.85f, -0.7f, 0.0f);
			glColor3f(0.1f, 0.9f, 0.0f);
			glVertex3f(0.55f, -0.7f, 0.0f);
		}
		{
			// 3 Trapazium for tree 2

			glColor3f(0.1f, 0.6f, 0.0f);
			glVertex3f(0.65f, -0.7f, 0.0f);
			glColor3f(0.1f, 0.7f, 0.0f);
			glVertex3f(0.75f, -0.7f, 0.0f);
			glColor3f(0.1f, 0.8f, 0.0f);
			glVertex3f(0.85f, -0.8f, 0.0f);
			glColor3f(0.1f, 0.9f, 0.0f);
			glVertex3f(0.55f, -0.8f, 0.0f);
		}

		{
			// Base of tree 2

			glColor3f(0.5f, 0.2f, 0.0f);
			glVertex3f(0.68f, -0.8f, 0.0f);
			glColor3f(0.7f, 0.3f, 0.0f);
			glVertex3f(0.72f, -0.8f, 0.0f);
			glColor3f(0.7f, 0.3f, 0.1f);
			glVertex3f(0.72f, -0.9f, 0.0f);
			glColor3f(0.8f, 0.4f, 0.0f);
			glVertex3f(0.68f, -0.9f, 0.0f);
		}


	}
	glEnd();

	glBegin(GL_TRIANGLES);
	{
		// First triangle of Tree 1 


		glColor3f(0.1f, 0.6f, 0.0f);
		glVertex3f(-0.7f, -0.4f, 0.0f);
		glColor3f(0.1f, 0.7f, 0.0f);
		glVertex3f(-0.8f, -0.50f, 0.0f);
		glColor3f(0.1f, 0.8f, 0.0f);
		glVertex3f(-0.6f, -0.50f, 0.0f);

	}
	{
		// First triangle of Tree 2 


		glColor3f(0.1f, 0.6f, 0.0f);
		glVertex3f(0.7f, -0.4f, 0.0f);
		glColor3f(0.1f, 0.7f, 0.0f);
		glVertex3f(0.8f, -0.50f, 0.0f);
		glColor3f(0.1f, 0.8f, 0.0f);
		glVertex3f(0.6f, -0.50f, 0.0f);

	}

	glEnd();

}

void backgroundScene_3(void)
{

	glBegin(GL_QUADS); // BG
	glColor3f(111.0f / 255.0f, 115.0f / 255.0f, 194.0f / 255.0f);
	glVertex3f(1.0f, -0.5f, 0.0f);
	glColor3f(132.0f / 255.0f, 135.0f / 255.0f, 194.0f / 255.0f);
	glVertex3f(1.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 0.0f);
	glColor3f(111.0f / 255.0f, 115.0f / 255.0f, 194.0f / 255.0f);
	glVertex3f(-1.0f, -0.5f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); // BG down Left side shade
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.4f, -1.0f, 0.0f);
	glColor3f(188.0f / 255.0f, 188.0f / 255.0f, 237.0f / 255.0f);
	glVertex3f(-0.2f, -0.5f, 0.0f);
	glVertex3f(-1.0f, -0.5f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); // BG down Right side shade
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.4f, -1.0f, 0.0f);
	glColor3f(188.0f / 255.0f, 188.0f / 255.0f, 237.0f / 255.0f);
	glVertex3f(0.2f, -0.5f, 0.0f);
	glVertex3f(1.0f, -0.5f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); // Red Carpet
	glColor3f(255.0f / 255.0f, 62.0f / 255.0f, 62.0f / 255.0f);
	glVertex3f(0.5f, -1.0f, 0.0f);
	glColor3f(255.0f / 255.0f, 32.0f / 255.0f, 32.0f / 255.0f);
	glVertex3f(0.2f, -0.5f, 0.0f);
	glVertex3f(-0.2f, -0.5f, 0.0f);
	glColor3f(255.0f / 255.0f, 62.0f / 255.0f, 62.0f / 255.0f);
	glVertex3f(-0.5f, -1.0f, 0.0f);
	glEnd();

	if (doorClose == TRUE)
	{
		glBegin(GL_QUADS); // Door
		glColor3f(247.0f / 255.0f, 138.0f / 255.0f, 43.0f / 255.0f);
		glVertex3f(0.2f, 0.2f, 0.0f);
		glVertex3f(0.0f, 0.2f, 0.0f);
		glVertex3f(0.0f, -0.5f, 0.0f);
		glVertex3f(0.2f, -0.5f, 0.0f);
		glEnd();

		glBegin(GL_QUADS); // Door
		glColor3f(247.0f / 255.0f, 138.0f / 255.0f, 43.0f / 255.0f);
		glVertex3f(-0.2f, 0.2f, 0.0f);
		glVertex3f(0.0f, 0.2f, 0.0f);
		glVertex3f(0.0f, -0.5f, 0.0f);
		glVertex3f(-0.2f, -0.5f, 0.0f);
		glEnd();

		glLineWidth(5.0f);
		glBegin(GL_LINES); // Between Door Line
		glColor3f(117.0f / 255.0f, 62.0f / 255.0f, 21.0f / 255.0f);
		glVertex3f(0.0f, -0.5f, 0.0f);
		glVertex3f(0.0f, 0.2f, 0.0f);
		glEnd();

		glLineWidth(3.0f);
		glBegin(GL_LINE_LOOP); // Right Door handle
		glColor3f(117.0f / 255.0f, 62.0f / 255.0f, 21.0f / 255.0f);
		glVertex3f(0.05f, -0.2f, 0.0f);
		glVertex3f(0.06f, -0.2f, 0.0f);
		glVertex3f(0.06f, -0.25f, 0.0f);
		glVertex3f(0.05f, -0.25f, 0.0f);
		glEnd();

		glLineWidth(3.0f);
		glBegin(GL_LINE_LOOP); // Left Door handle
		glColor3f(117.0f / 255.0f, 62.0f / 255.0f, 21.0f / 255.0f);
		glVertex3f(-0.05f, -0.2f, 0.0f);
		glVertex3f(-0.06f, -0.2f, 0.0f);
		glVertex3f(-0.06f, -0.25f, 0.0f);
		glVertex3f(-0.05f, -0.25f, 0.0f);
		glEnd();
	}
	else
	{
		glBegin(GL_QUADS); // White background
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.2f, 0.2f, 0.0f);
		glVertex3f(0.2f, -0.5f, 0.0f);
		glVertex3f(-0.2f, -0.5f, 0.0f);
		glVertex3f(-0.2f, 0.2f, 0.0f);
		glEnd();

		glBegin(GL_QUADS); // Door
		glColor3f(247.0f / 255.0f, 138.0f / 255.0f, 43.0f / 255.0f);
		glVertex3f(0.2f, 0.2f, 0.0f);
		glVertex3f(0.1f, 0.1f, 0.0f);
		glVertex3f(0.1f, -0.4f, 0.0f);
		glVertex3f(0.2f, -0.5f, 0.0f);
		glEnd();

		glBegin(GL_QUADS); // Door
		glColor3f(247.0f / 255.0f, 138.0f / 255.0f, 43.0f / 255.0f);
		glVertex3f(-0.2f, 0.2f, 0.0f);
		glVertex3f(-0.1f, 0.1f, 0.0f);
		glVertex3f(-0.1f, -0.4f, 0.0f);
		glVertex3f(-0.2f, -0.5f, 0.0f);
		glEnd();

		glLineWidth(3.0f);
		glBegin(GL_LINE_LOOP); // Right Door handle
		glColor3f(117.0f / 255.0f, 62.0f / 255.0f, 21.0f / 255.0f);
		glVertex3f(0.11f, -0.245f, 0.0f);
		glVertex3f(0.12f, -0.25f, 0.0f);
		glVertex3f(0.12f, -0.2f, 0.0f);
		glVertex3f(0.11f, -0.205f, 0.0f);
		glEnd();

		glLineWidth(3.0f);
		glBegin(GL_LINE_LOOP); // Left Door handle
		glColor3f(117.0f / 255.0f, 62.0f / 255.0f, 21.0f / 255.0f);
		glVertex3f(-0.11f, -0.245f, 0.0f);
		glVertex3f(-0.12f, -0.25f, 0.0f);
		glVertex3f(-0.12f, -0.2f, 0.0f);
		glVertex3f(-0.11f, -0.205f, 0.0f);
		glEnd();
	}	

	glLineWidth(10.0f);
	glBegin(GL_LINE_STRIP); //  Door Border
	glColor3f(117.0f / 255.0f, 62.0f / 255.0f, 21.0f / 255.0f);
	glVertex3f(0.2f, -0.5f, 0.0f);
	glVertex3f(0.2f, 0.2f, 0.0f);
	glVertex3f(-0.2f, 0.2f, 0.0f);
	glVertex3f(-0.2f, -0.5f, 0.0f);
	glEnd();

	//--------------------NAMING ----------------------

	// -----------------Screen 3 ---------------------

	//--------------------------S------------------------------


	glLineWidth(5.0f);
	glBegin(GL_LINES); // 
	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(-0.3f, 0.5f, 0.0f);
	glVertex3f(-0.25f, 0.5f, 0.0f);
	
	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(-0.25f, 0.5f, 0.0f);
	glVertex3f(-0.25f, 0.52f, 0.0f);

	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(-0.25f, 0.52f, 0.0f);
	glVertex3f(-0.3f, 0.52f, 0.0f);

	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(-0.3f, 0.52f, 0.0f);
	glVertex3f(-0.3f, 0.55f, 0.0f);

	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(-0.3f, 0.55f, 0.0f);
	glVertex3f(-0.25f, 0.55f, 0.0f);
	glEnd();
	//--------------------------C------------------------------
	glLineWidth(5.0f);
	glBegin(GL_LINES); // C
	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(-0.15f, 0.5f, 0.0f);
	glVertex3f(-0.2f, 0.5f, 0.0f);
	glEnd();

	glBegin(GL_LINES); // C
	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(-0.2f, 0.5f, 0.0f);
	glVertex3f(-0.2f, 0.55f, 0.0f);
	glEnd();

	glBegin(GL_LINES); // C
	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(-0.2f, 0.55f, 0.0f);
	glVertex3f(-0.15f, 0.55f, 0.0f);
	glEnd();

	//-------------------R------------------
	glLineWidth(5.0f);
	glBegin(GL_LINE_LOOP);  //R
	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(-0.1, 0.5f, 0.0f);
	glVertex3f(-0.1, 0.55f, 0.0f);
	glVertex3f(-0.05, 0.55f, 0.0f);
	glVertex3f(-0.05, 0.52f, 0.0f);
	glVertex3f(-0.1, 0.52f, 0.0f);
	glEnd();

	glLineWidth(2.0f);
	glBegin(GL_LINES);  //R
	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(-0.1, 0.52f, 0.0f);
	glVertex3f(-0.05, 0.5f, 0.0f);
	glEnd();

	//--------------------------E------------------------------
	glLineWidth(5.0f);
	glBegin(GL_LINES); // 
	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(0.0f, 0.5f, 0.0f);
	glVertex3f(0.05f, 0.5f, 0.0f);
	glEnd();

	glBegin(GL_LINES); // 
	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(0.0f, 0.52f, 0.0f);
	glVertex3f(0.05f, 0.52f, 0.0f);
	glEnd();

	glBegin(GL_LINES); // 
	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(0.0f, 0.55f, 0.0f);
	glVertex3f(0.05f, 0.55f, 0.0f);
	glEnd();

	glBegin(GL_LINES); // 
	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(0.0f, 0.5f, 0.0f);
	glVertex3f(0.0f, 0.55f, 0.0f);
	glEnd();

	//--------------------------E------------------------------
	glLineWidth(5.0f);
	glBegin(GL_LINES); // 
	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(0.1f, 0.5f, 0.0f);
	glVertex3f(0.15f, 0.5f, 0.0f);
	glEnd();

	glBegin(GL_LINES); // 
	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(0.1f, 0.52f, 0.0f);
	glVertex3f(0.15f, 0.52f, 0.0f);
	glEnd();

	glBegin(GL_LINES); // 
	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(0.1f, 0.55f, 0.0f);
	glVertex3f(0.15f, 0.55f, 0.0f);
	glEnd();

	glBegin(GL_LINES); // 
	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(0.1f, 0.5f, 0.0f);
	glVertex3f(0.1f, 0.55f, 0.0f);
	glEnd();

	//-------------------N-------------------------
	glLineWidth(5.0f);
	glBegin(GL_LINES);  // N
	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(0.2f, 0.5f, 0.0f);
	glVertex3f(0.2, 0.55f, 0.0f);
	glEnd();

	glLineWidth(5.0f);
	glBegin(GL_LINES);  // N
	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(0.2, 0.55f, 0.0f);
	glVertex3f(0.25, 0.5f, 0.0f);
	glEnd();

	glLineWidth(5.0f);
	glBegin(GL_LINES);  // N
	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(0.25, 0.5f, 0.0f);
	glVertex3f(0.25, 0.55f, 0.0f);
	glEnd();

	glPointSize(3.0f);
	glBegin(GL_POINTS);
	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(0.27f, 0.5f, 0.0f);
	glEnd();

	//--------------------------3------------------------------
	glLineWidth(5.0f);
	glBegin(GL_LINE_STRIP); // 
	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(0.3f, 0.55f, 0.0f);
	
	glVertex3f(0.35f, 0.55f, 0.0f);

	glVertex3f(0.33f, 0.52f, 0.0f);
	glVertex3f(0.35f, 0.52f, 0.0f);

	glVertex3f(0.35f, 0.5f, 0.0f);

	glVertex3f(0.3f, 0.5f, 0.0f);
	glEnd();

	// -----------------Welcome ---------------------

	//--------------------------W------------------------------

	glLineWidth(5.0f);

	glBegin(GL_LINES); // 
	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(-0.3f, 0.65f, 0.0f);
	glVertex3f(-0.3f, 0.7f, 0.0f);
	
	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(-0.3f, 0.65f, 0.0f);
	glVertex3f(-0.28f, 0.7f, 0.0f);

	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(-0.28f, 0.7f, 0.0f);
	glVertex3f(-0.25f, 0.65f, 0.0f);

	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(-0.25f, 0.65f, 0.0f);
	glVertex3f(-0.25f, 0.7f, 0.0f);
	glEnd();

	//--------------------------C------------------------------
	glLineWidth(5.0f);
	glBegin(GL_LINES); // C
	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(-0.15f, 0.65f, 0.0f);
	glVertex3f(-0.2f, 0.65f, 0.0f);
	
	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(-0.15f, 0.67f, 0.0f);
	glVertex3f(-0.2f, 0.67f, 0.0f);
	
	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(-0.15f, 0.7f, 0.0f);
	glVertex3f(-0.2f, 0.7f, 0.0f);
	
	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(-0.2f, 0.65f, 0.0f);
	glVertex3f(-0.2f, 0.7f, 0.0f);
	glEnd();

	//--------------------------L------------------------------
	glLineWidth(5.0f);
	glBegin(GL_LINES); // C
	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(-0.1f, 0.65f, 0.0f);
	glVertex3f(-0.1f, 0.7f, 0.0f);
	glEnd();
	glLineWidth(5.0f);
	glBegin(GL_LINES); // C
	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(-0.1f, 0.65f, 0.0f);
	glVertex3f(-0.05f, 0.65f, 0.0f);
	glEnd();

	//--------------------------E------------------------------
	glLineWidth(5.0f);
	
	glBegin(GL_LINES); // 
	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(0.0f, 0.65f, 0.0f);
	glVertex3f(0.05f, 0.65f, 0.0f);

	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(0.0f, 0.7f, 0.0f);
	glVertex3f(0.05f, 0.7f, 0.0f);

	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(0.0f, 0.65f, 0.0f);
	glVertex3f(0.0f, 0.7f, 0.0f);
	glEnd();

	//-------------------O------------------
	glLineWidth(5.0f);
	glBegin(GL_LINE_LOOP);  //O
	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(0.1, 0.65f, 0.0f);
	glVertex3f(0.1, 0.7f, 0.0f);
	glVertex3f(0.15, 0.7f, 0.0f);
	glVertex3f(0.15, 0.65f, 0.0f);
	glEnd();

	//-------------------M-------------------------
	glLineWidth(5.0f);
	glBegin(GL_LINES);  // M
	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(0.2f, 0.65f, 0.0f);
	glVertex3f(0.2, 0.7f, 0.0f);

	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(0.2, 0.7f, 0.0f);
	glVertex3f(0.22, 0.65f, 0.0f);

	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(0.22, 0.65f, 0.0f);
	glVertex3f(0.25, 0.7f, 0.0f);

	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(0.25, 0.7f, 0.0f);
	glVertex3f(0.25, 0.65f, 0.0f);
	glEnd();

	//--------------------------E------------------------------
	glLineWidth(5.0f);
	glBegin(GL_LINES); // 
	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(0.3f, 0.65f, 0.0f);
	glVertex3f(0.35f, 0.65f, 0.0f);

	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(0.3f, 0.67f, 0.0f);
	glVertex3f(0.35f, 0.67f, 0.0f);

	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(0.3f, 0.7f, 0.0f);
	glVertex3f(0.35f, 0.7f, 0.0f);

	glColor3f(255.0f / 255.0f, 0.0f / 255.0f, 28.0f / 255.0f);
	glVertex3f(0.3f, 0.65f, 0.0f);
	glVertex3f(0.3f, 0.7f, 0.0f);
	glEnd();
}

void musicModel1(void)
{
	glLineWidth(3.0f);
	glScalef(0.1f, 0.08f, 0.0f);
	glBegin(GL_LINES);

	glVertex3f(0.2f, 0.0f, 0.0f);
	glVertex3f(-0.2f, 0.0f, 0.0f);
	glEnd();

	glLineWidth(2.0f);
	glScalef(1.1f, 3.0f, 0.0);
	glBegin(GL_LINES);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glVertex3f(0.2f, -0.3f, 0.0f);

	glVertex3f(-0.2f, 0.0f, 0.0f);
	glVertex3f(-0.2f, -0.3f, 0.0f);
	glEnd();

}

void musicSymbol1(void)
{
	void musicModel1(void);

	void BottomCircle(void);

	glLoadIdentity();
	glTranslatef(0.0f, M1T, -0.0f);
	glTranslatef(-0.8f, 0.0f, 0.0f);
	//	glScalef(M1S, M1S, M1S);
	musicModel1();

	glLoadIdentity();
	glTranslatef(0.1f, (M1T - 0.3f), -0.0f);
	glTranslatef(-0.88f, 0.23f, 0.0f);
	glScalef(0.12f, 0.12f, 0.0f);
	//glScalef((1.1f + M1S), (0.8f + M1S),(0.8f + M1S));
	//glScalef(M1S, M1S, M1S);
	BottomCircle();

	glLoadIdentity();
	glTranslatef(-0.3f, (M1T - 0.3f), -0.0f);
	glTranslatef(-0.530f, 0.23f, 0.0f);
	glScalef(0.12f, 0.12f, 0.0f);
	//glScalef(1.1f, 0.8f, 0.8f);
	//glScalef(M1S, M1S, M1S);
	BottomCircle();

}
void musicSymbol2(void)
{

	void musicModel2(void);
	void BottomCircle(void);

	glLoadIdentity();
	glTranslatef(0.0f, M2T, -0.0f);
	glTranslatef(-0.7f, 0.0f, 0.0f);
	glScalef(0.3f, 0.3f, 0.0f);
	musicModel2();


	glLoadIdentity();
	glTranslatef(-0.1f, (M2T - 0.3f), -0.0f);
	glTranslatef(-0.61f, 0.21f, 0.0f);
	glScalef(0.1f, 0.1f, 0.0f);
	//glScalef(1.1f, 0.8f, 0.8f);
	BottomCircle();

	glLoadIdentity();
	glTranslatef(0.0f, (M2T - 1.0f), -0.0f);
	glTranslatef(-0.7f, 0.1f, 0.0f);
	glScalef(0.3f, 0.3f, 0.0f);
	musicModel2();


	glLoadIdentity();
	glTranslatef(-0.1f, (M2T - 1.3f), -0.0f);
	glTranslatef(-0.61f, 0.31f, 0.0f);
	glScalef(0.1f, 0.1f, 0.0f);
	//glScalef(1.1f, 0.8f, 0.8f);
	BottomCircle();
}

void musicModel2(void)
{
	glBegin(GL_POLYGON);
	glVertex3f(0.0f, 0.0f, 0.0f);

	glVertex3f(0.0f, -0.1f, 0.0f);
	glVertex3f(0.15f, -0.1f, 0.0f);
	glVertex3f(0.2f, -0.15f, 0.0f);
	glVertex3f(0.2f, -0.1f, 0.0f);
	glVertex3f(0.15f, -0.05f, 0.0f);
	glVertex3f(0.0f, -0.05f, 0.0f);

	glEnd();

	glBegin(GL_LINES);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, -0.3f, 0.0f);

	glEnd();


}

void BottomCircle(void)
{
	//glScalef(1.1f, 0.8f, 0.8f);

	glBegin(GL_TRIANGLE_STRIP);
	for (float fAngle = 0.0f; fAngle <= 360.0f; fAngle++)
	{

		angleRadian = fAngle * PS_PI / 180.0f;

		x_1 = RADIUS * cos(angleRadian);
		y_1 = RADIUS * sin(angleRadian);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(x_1, y_1, 0.0f);

	}
	glEnd();
}

void Bai(void)
{
	glTranslatef(1.0f, 0.0f, -3.0f);
	glBegin(GL_POLYGON);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.05f, 0.0f, 0.0f);
	glVertex3f(-0.15f, -0.6f, 0.0f);
	glVertex3f(0.15f, -0.6f, 0.0f);
	glVertex3f(0.05f, 0.0f, 0.0f);
	glEnd();

	glTranslatef(0.0f, 0.1f, 0.0f);
	glBegin(GL_TRIANGLE_STRIP);
	for (float fAngle = 0.0f; fAngle <= 360.0f; fAngle++)
	{

		angleRadian = fAngle * PS_PI / 180.0f;

		x_1 = RADIUS * cos(angleRadian);
		y_1 = RADIUS * sin(angleRadian);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(x_1, y_1, 0.0f);

	}
	glEnd();

}
void Pipani(void)
{
	// Pipani wala manus
	glScalef(0.8f, 1.0f, 0.0f);
	glBegin(GL_POLYGON);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-0.05f, 0.0f, 0.0f);
	glVertex3f(-0.1f, -0.4f, 0.0f);

	glVertex3f(0.1f, -0.4f, 0.0f);
	glVertex3f(0.05f, 0.0f, 0.0f);
	glEnd();
	glTranslatef(0.0f, 0.1f, 0.0f);

	glBegin(GL_TRIANGLE_STRIP);
	glColor3f(1.0f, 1.0f, 1.0f);
	for (float fAngle = 0.0f; fAngle <= 360.0f; fAngle++)
	{

		angleRadian = fAngle * PS_PI / 180.0f;

		x_1 = RADIUS * cos(angleRadian);
		y_1 = RADIUS * sin(angleRadian);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(x_1, y_1, 0.0f);

	}
	glEnd();

	if (check == TRUE)
	{
		glRotatef(180.0f, 0.0f, 1.0f, 0.0f);
	}
	else
	{
		glRotatef(0.0f, 0.0f, 1.0f, 0.0f);
	}
	// Hands
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.07f, -0.2f, 0.0f);
	glVertex3f(0.2f, -0.02f, 0.0f);

	glVertex3f(0.07f, -0.2f, 0.0f);
	glVertex3f(0.25f, -0.02f, 0.0f);

	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(0.35f, 0.05f, 0.0f);
	glVertex3f(0.1f, 0.0f, 0.0f);
	glVertex3f(0.1f, -0.03f, 0.0f);
	glVertex3f(0.35f, -0.05f, 0.0f);

	glEnd();

}
void Zanj(void)
{
	// Zanj wala manus

	glScalef(0.8f, 1.0f, 0.0f);
	glBegin(GL_POLYGON);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-0.05f, 0.0f, 0.0f);
	glVertex3f(-0.1f, -0.4f, 0.0f);

	glVertex3f(0.1f, -0.4f, 0.0f);
	glVertex3f(0.05f, 0.0f, 0.0f);
	glEnd();
	glTranslatef(0.0f, 0.1f, 0.0f);

	glBegin(GL_TRIANGLE_STRIP);
	glColor3f(1.0f, 1.0f, 1.0f);
	for (float fAngle = 0.0f; fAngle <= 360.0f; fAngle++)
	{

		angleRadian = fAngle * PS_PI / 180.0f;

		x_1 = RADIUS * cos(angleRadian);
		y_1 = RADIUS * sin(angleRadian);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(x_1, y_1, 0.0f);

	}
	glEnd();


}
void rightHandOfZanj(void)
{
	glTranslatef(-Bx, 0.0f, 0.0f);
	glScalef(0.8f, 1.0f, 0.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.07f, -0.2f, 0.0f);
	glVertex3f(0.2f, 0.08f, 0.0f);

	glVertex3f(0.2f, 0.08f, 0.0f);
	glVertex3f(0.1f, 0.2f, 0.0f);

	//Zanj
	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(0.1f, 0.25f, 0.0f);
	glVertex3f(0.1f, 0.1f, 0.0f);
	glEnd();
}
void leftHandOfZanj(void)
{

	glTranslatef(Bx, 0.0f, 0.0f);
	glScalef(0.8f, 1.0f, 0.0f);
	glLineWidth(4.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.07f, -0.2f, 0.0f);
	glVertex3f(-0.2f, 0.08f, 0.0f);

	glVertex3f(-0.2f, 0.08f, 0.0f);
	glVertex3f(-0.1f, 0.2f, 0.0f);

	// Zanj
	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(-0.1f, 0.25f, 0.0f);
	glVertex3f(-0.1f, 0.1f, 0.0f);

	glEnd();


}

// Strings to display
char stringsToRender[20][1024] = {
	"C A M E R A   G R O U P",
	"\n",
	"G R O U P  L E A D E R",
	"Kishor Ekbote",
	"\n",
	"P R O J E C T  L E A D E R",
	"Captain Prasad Bhalkikar",
	"\n",
	"G R O U P   M E M B E R S",
	"Prasad Bhalkikar",
	"Aditya Kulkarni",
	"Prajakta Bartakke",
	"Paras Satpute",
	"Vinit Surve",
	"Dipak Bhor",
	"Sanket Shete",
	"\n",
	"THE END"
};

//void endCredits(void) {
//	glColor3f(1.0f, 1.0f, 1.0f);
//
//	for (int i = 0; i < 20; i++) {
//		GLfloat computedYCoordinate = yCoordinate - (i * 0.1f);
//		renderBitmapChar(-0.2f, computedYCoordinate, stringsToRender[i]);
//	}
//}

//void renderBitmapChar(float x, float y, char* string)
//{
//	// code
//	char* c;
//	glRasterPos2f(x, y);
//
//	for (c = string; *c != '\0'; c++)
//	{
//		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, *c);
//	}
//}

void Face(void)
{
	// Face
	glBegin(GL_POLYGON);
	glColor3f(1.12f, 0.51f, 0.40f);
	glVertex2f(0.02f, 0.24f);
	glVertex2f(0.04f, 0.26f);
	glVertex2f(0.06f, 0.28f);
	glVertex2f(0.065f, 0.3f);
	glVertex2f(0.07f, 0.32f);
	glVertex2f(0.06f, 0.31f);
	glVertex2f(0.065f, 0.32f);
	glVertex2f(0.06f, 0.32f);
	glVertex2f(0.04f, 0.325f);
	glVertex2f(0.04f, 0.33f);
	glVertex2f(0.02f, 0.335f);
	glVertex2f(0.0f, 0.34f);
	glVertex2f(-0.02f, 0.35f);
	glVertex2f(-0.03f, 0.34f);
	glVertex2f(-0.04f, 0.32f);
	glVertex2f(-0.06f, 0.321f);
	glVertex2f(-0.07f, 0.32f);
	glVertex2f(-0.07f, 0.3f);
	glVertex2f(-0.06f, 0.28f);
	glVertex2f(-0.055f, 0.27f);
	glVertex2f(-0.045f, 0.26f);
	glVertex2f(-0.04f, 0.258f);
	glVertex2f(-0.02f, 0.24f);
	glEnd();
	//LHS Ear
	glBegin(GL_POLYGON);
	glColor3f(1.12f, 0.51f, 0.40f);
	glVertex2f(-0.06f, 0.3f);
	glVertex2f(-0.075f, 0.305f);
	glVertex2f(-0.079f, 0.31f);
	glVertex2f(-0.08f, 0.3f);
	glVertex2f(-0.075f, 0.29f);
	glVertex2f(-0.07f, 0.28f);
	glVertex2f(-0.065f, 0.27f);
	glVertex2f(-0.06f, 0.265f);
	glEnd();
	//RHS Ear
	glBegin(GL_POLYGON);
	glColor3f(1.12f, 0.51f, 0.40f);
	glVertex2f(0.06f, 0.3f);
	glVertex2f(0.07f, 0.305f);
	glVertex2f(0.075f, 0.31f);
	glVertex2f(0.079f, 0.295f);
	glVertex2f(0.075f, 0.29f);
	glVertex2f(0.07f, 0.28f);
	glVertex2f(0.065f, 0.27f);
	glVertex2f(0.06f, 0.265f);
	glEnd();
	//Lip
	glBegin(GL_POLYGON);
	glColor3f(204.0f / 255.0f, 0.f, 0.0f);
	glVertex2f(0.02f, 0.26f);
	glVertex2f(0.021f, 0.265f);
	glVertex2f(0.02f, 0.27f);
	glVertex2f(0.02f, 0.269f);
	glVertex2f(0.0195f, 0.25f);
	glVertex2f(0.01f, 0.25f);
	glVertex2f(0.0f, 0.25f);
	glVertex2f(-0.01f, 0.255f);
	glVertex2f(-0.015f, 0.265f);
	glVertex2f(-0.02f, 0.265f);
	glEnd();

	// Hair RHS
	glBegin(GL_POLYGON);
	glColor3f(57.0f / 255.0f, 57.0f / 255.0f, 57.0f / 255.0f);
	glVertex2f(0.075f, 0.30f);
	glVertex2f(0.07f, 0.33f);
	glVertex2f(0.065f, 0.338f);
	glVertex2f(0.06f, 0.34f);
	glVertex2f(0.06f, 0.34f);
	glVertex2f(0.05f, 0.35f);
	glVertex2f(0.04f, 0.36f);
	glVertex2f(0.03f, 0.37f);
	glVertex2f(0.02f, 0.375f);
	glVertex2f(0.01f, 0.38f);
	glVertex2f(0.0f, 0.375f);
	glVertex2f(-0.02f, 0.34f);
	glEnd();

	// Hair LHS
	glBegin(GL_POLYGON);
	glColor3f(57.0f / 255.0f, 57.0f / 255.0f, 57.0f / 255.0f);
	glVertex2f(0.0f, 0.36f);
	glVertex2f(-0.03f, 0.36f);
	glVertex2f(-0.04f, 0.35f);
	glVertex2f(-0.05f, 0.34f);
	glVertex2f(-0.06f, 0.32f);
	glVertex2f(-0.065f, 0.325f);
	glVertex2f(-0.07f, 0.325f);
	glVertex2f(-0.065f, 0.325f);
	glVertex2f(-0.06f, 0.31f);
	glVertex2f(-0.05f, 0.31f);
	glVertex2f(-0.045f, 0.333f);
	glVertex2f(-0.04f, 0.32f);
	glEnd();

	// Eye LHS
	glBegin(GL_POLYGON);
	glColor3f(240.0f / 255.0f, 240.0f / 255.0f, 240.0f / 255.0f);
	glVertex2f(-0.02f, 0.31f);
	glVertex2f(-0.021f, 0.315f);
	glVertex2f(-0.025f, 0.32f);
	glVertex2f(-0.03f, 0.32f);
	glVertex2f(-0.035f, 0.32f);
	glVertex2f(-0.04f, 0.315f);
	glVertex2f(-0.045f, 0.31f);
	glVertex2f(-0.05f, 0.305f);
	glVertex2f(-0.04f, 0.30f);
	glEnd();
	glPointSize(5.0);
	glBegin(GL_POINTS);
	glColor3f(61.0f / 255.0f, 61.0f / 255.0f, 61.0f / 255.0f);
	glVertex2f(-0.03f, 0.31f);
	glEnd();
	// Eye RHS
	glBegin(GL_POLYGON);
	glColor3f(240.0f / 255.0f, 240.0f / 255.0f, 240.0f / 255.0f);
	glVertex2f(0.045f, 0.31f);
	glVertex2f(0.04f, 0.315f);
	glVertex2f(0.035f, 0.32f);
	glVertex2f(0.03f, 0.315f);
	glVertex2f(0.02f, 0.31f);
	glVertex2f(0.0195f, 0.31f);
	glVertex2f(0.02f, 0.32f);
	glVertex2f(0.021f, 0.31f);
	glVertex2f(0.025f, 0.3f);
	glVertex2f(0.03f, 0.3f);
	glEnd();
	glPointSize(5.0);
	glBegin(GL_POINTS);
	glColor3f(61.0f / 255.0f, 61.0f / 255.0f, 61.0f / 255.0f);
	glVertex2f(0.03f, 0.31f);
	glEnd();

	// Nose
	glBegin(GL_POLYGON);
	glColor3f(191.0f / 255.0f, 72.0f / 255.0f, 102.0f / 255.0f);
	glVertex2f(0.01f, 0.28f);
	glVertex2f(0.005f, 0.285f);
	glVertex2f(0.005f, 0.29f);
	glVertex2f(0.005f, 0.31f);
	glVertex2f(-0.005f, 0.31f);
	glVertex2f(-0.005f, 0.29f);
	glVertex2f(-0.005f, 0.285f);
	glVertex2f(-0.01f, 0.28f);
	glEnd();

	// Neck
	glBegin(GL_POLYGON);
	glColor3f(1.12f, 0.51f, 0.40f);
	glVertex3f(0.02f, 0.22f, 0.0f);
	glVertex3f(0.02f, 0.24f, 0.0f);
	glVertex3f(-0.02f, 0.24f, 0.0f);
	glVertex3f(-0.02f, 0.22f, 0.0f);
	glEnd();
}

void Leg(void)
{
	// LHS Leg Pant
	glBegin(GL_POLYGON);
	glColor3f(0.35f, 0.35f, 1.0f);
	glVertex2f(0.0f, -0.1f);
	glVertex2f(-0.08f, -0.1f);
	glVertex2f(-0.1f, -0.36f);
	glVertex2f(-0.04, -0.36f);
	glEnd();
	// RHS Leg Pant
	glBegin(GL_POLYGON);
	glColor3f(0.35f, 0.35f, 1.0f);
	glVertex2f(0.08f, -0.1f);
	glVertex2f(0.0f, -0.1f);
	glVertex2f(0.04, -0.36f);
	glVertex2f(0.1f, -0.36f);
	glEnd();
	// LHS Leg
	glBegin(GL_POLYGON);
	glColor3f(1.12f, 0.51f, 0.40f);
	glVertex2f(-0.05f, -0.36f);
	glVertex2f(-0.09f, -0.36f);
	glVertex2f(-0.09f, -0.42f);
	glVertex2f(-0.05f, -0.42f);
	glEnd();
	// RHS Leg
	glBegin(GL_POLYGON);
	glColor3f(1.12f, 0.51f, 0.40f);
	glVertex2f(0.09f, -0.36f);
	glVertex2f(0.05f, -0.36f);
	glVertex2f(0.05f, -0.42f);
	glVertex2f(0.09f, -0.42f);
	glEnd();
}

void MiddleBody(void)
{

	// Abdomain
	glBegin(GL_POLYGON);
	glColor3f(0.84f, 0.15f, 0.69f);
	glVertex2f(0.08f, -0.12f);
	glVertex2f(0.1f, 0.12f);
	glVertex2f(-0.1f, 0.12f);
	glVertex2f(-0.08f, -0.12f);
	glEnd();

	// Chest
	glBegin(GL_POLYGON);
	glColor3f(0.84f, 0.15f, 0.69f);
	glVertex2f(0.1f, 0.12f);
	glVertex2f(0.06f, 0.22f);
	glVertex2f(-0.06f, 0.22f);
	glVertex2f(-0.1f, 0.12f);
	glEnd();
	//Sholder
	glBegin(GL_TRIANGLES);
	glColor3f(0.84f, 0.15f, 0.69f);
	// triangle 11 - RHS Sholder
	glVertex3f(0.1f, 0.12f, 0.0f);
	glVertex3f(0.1f, 0.2f, 0.0f);
	glVertex3f(0.06f, 0.22f, 0.0f);
	// triangle 13 RHS Sholder
	glVertex3f(0.1f, 0.12f, 0.0f);
	glVertex3f(0.12f, 0.18f, 0.0f);
	glVertex3f(0.1f, 0.2f, 0.0f);
	// triangle 12- LHS Sholder
	glVertex3f(-0.1f, 0.12f, 0.0f);
	glVertex3f(-0.1f, 0.2f, 0.0f);
	glVertex3f(-0.06f, 0.22f, 0.0f);
	// triangle 14 LHS Sholder
	glVertex3f(-0.1f, 0.12f, 0.0f);
	glVertex3f(-0.12f, 0.18f, 0.0f);
	glVertex3f(-0.1f, 0.2f, 0.0f);
	glEnd();

}

void LeftArm(void)
{
	// LHS Arm
	glBegin(GL_POLYGON);
	glColor3f(0.84f, 0.15f, 0.69f);
	glVertex2f(-0.12f, 0.18f);
	glVertex2f(-0.18f, 0.06f);
	glVertex2f(-0.14f, 0.04f);
	glVertex2f(-0.1f, 0.12f);
	glEnd();
}

void RightArm(void)
{
	// RHS Arm
	glBegin(GL_POLYGON);
	glColor3f(0.84f, 0.15f, 0.69f);
	glVertex2f(0.12f, 0.18f);
	glVertex2f(0.1f, 0.12f);
	glVertex2f(0.14f, 0.04f);
	glVertex2f(0.18f, 0.06f);
	glEnd();
}

void Drum1(void)
{
	// Circle Variable Declarations
	float angle = 0.0f;
	float x = 0.0f;
	float y = 0.0f;
	float r = 0.14f;
	float angle_rad = 0.0f;

	//Circle loop
	glBegin(GL_LINE_STRIP);

	for (angle = 0.0f; angle <= 360.0f; angle = angle + 0.1f)
	{
		angle_rad = angle * (DBB_PI / 180.0f); // Convert angle degree to radian.

		x = cos(angle_rad) * r;
		y = sin(angle_rad) * r;

		glLineWidth(3.0); // You can change the width value as needed
		glColor3f(132.0f / 255.0f, 132.0f / 255.0f, 132.0f / 255.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glColor3f(211.0f / 255.0f, 211.0f / 255.0f, 211.0f / 255.0f); // White Color
		glVertex3f(x, y, 0.0f);
	}
	glLineWidth(5.0f);
	glEnd();
}

void Drum2(void)
{
	// Circle Variable Declarations
	float angle = 0.0f;
	float x = 0.0f;
	float y = 0.0f;
	float r = 0.14f;
	float angle_rad = 0.0f;

	//Circle loop
	glBegin(GL_LINE_STRIP);

	for (angle = 0.0f; angle <= 360.0f; angle = angle + 0.1f)
	{
		angle_rad = angle * (DBB_PI / 180.0f); // Convert angle degree to radian.

		x = cos(angle_rad) * r;
		y = sin(angle_rad) * r;

		glLineWidth(3.0); // You can change the width value as needed
		glColor3f(132.0f / 255.0f, 132.0f / 255.0f, 132.0f / 255.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		//glColor3f(211.0f / 255.0f, 211.0f / 255.0f, 211.0f / 255.0f); // White Color
		glColor3f(128.0f / 255.0f, 128.0f / 255.0f, 128.0f / 255.0f); // White Color
		glVertex3f(x, y, 0.0f);
	}
	glLineWidth(5.0f);
	glEnd();

}

void DrumLine(void)
{
	glLineWidth(2.0);
	glBegin(GL_POLYGON);
	glColor3f(159.0f / 255.0f, 0.0f, 0.0f);
	glVertex2f(0.14f, -0.16f);
	glVertex2f(-0.14f, -0.16f);
	glVertex2f(-0.14f, -0.3f);
	glVertex2f(0.14f, -0.3f);

	glEnd();
}

void gline(void)
{
	glLineWidth(2.0);
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex2f(0.6f, -0.41f);
	glVertex2f(-0.6f, -0.41f);
	glEnd();
}

void drumStand(void)
{
	glLineWidth(2.0);
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex2f(0.1f, -0.2f);
	glVertex2f(0.2f, -0.4f);

	glVertex2f(-0.1f, -0.2f);
	glVertex2f(-0.2f, -0.4f);
	glEnd();
}

void drumstick(void)
{
	glLineWidth(3.0);
	glBegin(GL_TRIANGLES);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex2f(0.0f, -0.2f);
	glVertex2f(0.18f, -0.05f);
	glVertex2f(0.17f, -0.04f);
	glEnd();

	glLineWidth(3.0);
	glBegin(GL_TRIANGLES);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex2f(0.0f, -0.2f);
	glVertex2f(-0.18f, -0.05f);
	glVertex2f(-0.17f, -0.04f);
	glEnd();
}

void RightHand(void)
{
	// RHS Hand
	glBegin(GL_POLYGON);

	glColor3f(1.12f, 0.51f, 0.40f);
	glVertex2f(0.18f, 0.06f);
	glVertex2f(0.14f, 0.04f);
	glVertex2f(0.14f, -0.06f);
	glVertex2f(0.16f, -0.06f);
	glEnd();
}

void LeftHand(void)
{
	// Left Hand
	glBegin(GL_POLYGON);

	glColor3f(1.12f, 0.51f, 0.40f);
	glVertex2f(-0.14f, 0.04f);
	glVertex2f(-0.18f, 0.06f);
	glVertex2f(-0.18f, -0.06f);
	glVertex2f(-0.16f, -0.06f);

	glEnd();
}

void stageFront(void)
{
	glBegin(GL_POLYGON);

	//	rgb(204, 119, 34)
	glColor3f(204.0f / 255.0f, 119.0f / 255.0f, 34.0f / 255.0f);
	//glColor3f(139.0f / 255.0f, 69.0f / 255.0f, 19.0f / 255.0f);


	glVertex3f(1.0f, 0.0f, 0.0f);
	glVertex3f(1.0f, 0.07f, 0.0f);
	glVertex3f(-1.0f, 0.07f, 0.0f);
	glVertex3f(-1.0f, 0.0f, 0.0f);

	glEnd();
}

void drawStage(void)
{
	glBegin(GL_POLYGON);

	//rgb(139, 69, 19)

	//glColor3f(204.0f / 255.0f, 119.0f / 255.0f, 34.0f / 255.0f);
	glColor3f(139.0f / 255.0f, 69.0f / 255.0f, 19.0f / 255.0f);

	glVertex3f(0.5f, 2.0f, 0.0f);
	glVertex3f(-0.5f, 2.0f, 0.0f);
	glVertex3f(-0.75f, 0.0f, 0.0f);
	glVertex3f(0.75f, 0.0f, 0.0f);

	glEnd();



}

void stageBg(void)
{
	glBegin(GL_POLYGON);

	//glColor3f(136.0f / 255.0f, 8.0f / 255.0f, 8.0f / 255.0f);

	//glColor3f(212.0f / 255.0f, 0.0f / 255.0f, 0.0f / 255.0f);

	glColor3f(136.0f / 255.0f, 8.0f / 255.0f, 8.0f / 255.0f);
	glVertex3f(1.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, 0.0f);

	glEnd();

}

void happyBirthday(void)
{
	void LetterA(void);
	void LetterH(void);
	void LetterI(void);
	void LetterT(void);
	void LetterY(void);
	void LetterP(void);
	void LetterR(void);
	void LetterD(void);
	void LetterS(void);
	void LetterB(void);
	void LetterL(void);

	//HAPPY
	glLoadIdentity();
	glTranslatef(-0.2f, sPositionHappy.y, -1.0f);
	LetterH();
	glLoadIdentity();
	glTranslatef(-0.1f, sPositionHappy.y, -1.0f);
	LetterA();
	glLoadIdentity();
	glTranslatef(0.0f, sPositionHappy.y, -1.0f);
	LetterP();
	glLoadIdentity();
	glTranslatef(0.1f, sPositionHappy.y, -1.0f);
	LetterP();
	glLoadIdentity();
	glTranslatef(0.2f, sPositionHappy.y, -1.0f);
	LetterY();

	// Birthday
	glLoadIdentity();
	glTranslatef(-0.3f, sPositionBirthday.y, -1.0f);
	LetterB();
	glLoadIdentity();
	glTranslatef(-0.23f, sPositionBirthday.y, -1.0f);
	LetterI();
	glLoadIdentity();
	glTranslatef(-0.15f, sPositionBirthday.y, -1.0f);
	LetterR();
	glLoadIdentity();
	glTranslatef(-0.07f, sPositionBirthday.y, -1.0f);
	LetterT();
	glLoadIdentity();
	glTranslatef(0.03f, sPositionBirthday.y, -1.0f);
	LetterH();
	glLoadIdentity();
	glTranslatef(0.13f, sPositionBirthday.y, -1.0f);
	LetterD();
	glLoadIdentity();
	glTranslatef(0.22f, sPositionBirthday.y, -1.0f);
	LetterA();
	glLoadIdentity();
	glTranslatef(0.3f, sPositionBirthday.y, -1.0f);
	LetterY();

	//DIPALI
	glLoadIdentity();
	glTranslatef(-0.2f, sPositionDipali.y, -1.0f);
	LetterD();
	glLoadIdentity();
	glTranslatef(-0.12f, sPositionDipali.y, -1.0f);
	LetterI();
	glLoadIdentity();
	glTranslatef(-0.05f, sPositionDipali.y, -1.0f);
	LetterP();
	glLoadIdentity();
	glTranslatef(0.03f, sPositionDipali.y, -1.0f);
	LetterA();
	glLoadIdentity();
	glTranslatef(0.1f, sPositionDipali.y, -1.0f);
	LetterL();
	glLoadIdentity();
	glTranslatef(0.2f, sPositionDipali.y, -1.0f);
	LetterI();
}

void LetterA(void)
{


	glScalef(ScaleX, ScaleY, ScaleZ);

	glBegin(GL_QUADS);
	// '\'
	glColor3f(lettersR, lettersG, lettersB);
	glVertex3f(0.9f, -1.0f, 0.0f);
	glVertex3f(0.5f, -1.0f, 0.0f);
	glVertex3f(0.0f, 0.2f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	// '/'
	glVertex3f(0.0f, 0.2f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-0.9f, -1.0f, 0.0f);
	glVertex3f(-0.5f, -1.0f, 0.0f);

	// '-'
	glVertex3f(0.5f, -0.1f, 0.0f);
	glVertex3f(-0.5f, -0.1f, 0.0f);
	glVertex3f(-0.5f, -0.5f, 0.0f);
	glVertex3f(0.5f, -0.5f, 0.0f);
	glEnd();

}
void LetterH(void)
{

	//H	
	glScalef(ScaleX, ScaleY, ScaleZ);

	glBegin(GL_QUADS);
	glColor3f(lettersR, lettersG, lettersB);

	glVertex3f(0.9f, 1.0f, 0.0f);
	glVertex3f(0.4f, 1.0f, 0.0f);
	glVertex3f(0.4f, -1.0f, 0.0f);
	glVertex3f(0.9f, -1.0f, 0.0f);

	glVertex3f(-0.9f, 1.0f, 0.0f);
	glVertex3f(-0.4f, 1.0f, 0.0f);
	glVertex3f(-0.4f, -1.0f, 0.0f);
	glVertex3f(-0.9f, -1.0f, 0.0f);

	glVertex3f(0.5f, 0.3f, 0.0f);
	glVertex3f(-0.5f, 0.3f, 0.0f);
	glVertex3f(-0.5f, -0.3f, 0.0f);
	glVertex3f(0.5f, -0.3f, 0.0f);
	glEnd();
}
void LetterI(void)
{


	glScalef(ScaleX, ScaleY, ScaleZ);

	glBegin(GL_QUADS);
	glColor3f(lettersR, lettersG, lettersB);

	glVertex3f(0.2f, 1.0f, 0.0f);
	glVertex3f(-0.2f, 1.0f, 0.0f);
	glVertex3f(-0.2f, -1.0f, 0.0f);
	glVertex3f(0.2f, -1.0f, 0.0f);
	glEnd();

}
void LetterT(void)
{


	glScalef(ScaleX, ScaleY, ScaleZ);

	glBegin(GL_QUADS);
	glColor3f(lettersR, lettersG, lettersB);

	glVertex3f(0.2f, 1.0f, 0.0f);
	glVertex3f(-0.2f, 1.0f, 0.0f);
	glVertex3f(-0.2f, -1.0f, 0.0f);
	glVertex3f(0.2f, -1.0f, 0.0f);

	glVertex3f(0.8f, 1.0f, 0.0f);
	glVertex3f(-0.8f, 1.0f, 0.0f);
	glVertex3f(-0.8f, 0.5f, 0.0f);
	glVertex3f(0.8f, 0.5f, 0.0f);
	glEnd();
}
void LetterY(void)
{


	glScalef(ScaleX, ScaleY, ScaleZ);

	glBegin(GL_QUADS);
	glColor3f(lettersR, lettersG, lettersB);

	glVertex3f(0.8f, 1.0f, 0.0f);
	glVertex3f(0.4f, 1.0f, 0.0f);
	glVertex3f(0.0f, 0.1f, 0.0f);
	glVertex3f(0.2f, -0.3f, 0.0f);

	glVertex3f(-0.8f, 1.0f, 0.0f);
	glVertex3f(-0.4f, 1.0f, 0.0f);
	glVertex3f(-0.0f, 0.1f, 0.0f);
	glVertex3f(-0.2f, -0.3f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glVertex3f(0.2f, -0.3f, 0.0f);
	glVertex3f(0.0f, 0.1f, 0.0f);
	glVertex3f(-0.2f, -0.3f, 0.0f);
	glVertex3f(-0.2f, -1.0f, 0.0f);
	glVertex3f(0.2f, -1.0f, 0.0f);
	glEnd();
}
void LetterP(void)
{


	glScalef(ScaleX, ScaleY, ScaleZ);

	glBegin(GL_QUADS);
	glColor3f(lettersR, lettersG, lettersB);

	glVertex3f(0.5f, 1.0f, 0.0f);
	glVertex3f(0.2f, 1.0f, 0.0f);
	glVertex3f(0.2f, -0.2f, 0.0f);
	glVertex3f(0.5f, -0.2f, 0.0f);

	glVertex3f(-0.75f, 1.0f, 0.0f);
	glVertex3f(-0.4f, 1.0f, 0.0f);
	glVertex3f(-0.4f, -1.0f, 0.0f);
	glVertex3f(-0.75f, -1.0f, 0.0f);

	glVertex3f(0.2f, 0.2f, 0.0f);
	glVertex3f(-0.4f, 0.2f, 0.0f);
	glVertex3f(-0.4f, -0.2f, 0.0f);
	glVertex3f(0.2f, -0.2f, 0.0f);

	glVertex3f(0.2f, 1.0f, 0.0f);
	glVertex3f(-0.4f, 1.0f, 0.0f);
	glVertex3f(-0.4f, 0.6f, 0.0f);
	glVertex3f(0.2f, 0.6f, 0.0f);
	glEnd();

}
void LetterD(void)
{


	glScalef(ScaleX, ScaleY, ScaleZ);

	glBegin(GL_QUADS);
	glColor3f(lettersR, lettersG, lettersB);

	glVertex3f(-0.75f, 1.0f, 0.0f);
	glVertex3f(-0.4f, 1.0f, 0.0f);
	glVertex3f(-0.4f, -1.0f, 0.0f);
	glVertex3f(-0.75f, -1.0f, 0.0f);

	glVertex3f(-0.4f, 1.0f, 0.0f);
	glVertex3f(-0.4f, 0.7f, 0.0f);
	glVertex3f(0.35f, 0.7f, 0.0f);
	glVertex3f(0.5f, 1.0f, 0.0f);

	glVertex3f(0.8f, 0.6f, 0.0f);
	glVertex3f(0.5f, 1.0f, 0.0f);
	glVertex3f(0.35f, 0.7f, 0.0f);
	glVertex3f(0.55f, 0.5f, 0.0f);


	glVertex3f(-0.4f, -1.0f, 0.0f);
	glVertex3f(-0.4f, -0.7f, 0.0f);
	glVertex3f(0.35f, -0.7f, 0.0f);
	glVertex3f(0.5f, -1.0f, 0.0f);

	glVertex3f(0.8f, -0.6f, 0.0f);
	glVertex3f(0.5f, -1.0f, 0.0f);
	glVertex3f(0.35f, -0.7f, 0.0f);
	glVertex3f(0.55f, -0.5f, 0.0f);

	glVertex3f(0.8f, 0.6f, 0.0f);
	glVertex3f(0.55f, 0.5f, 0.0f);
	glVertex3f(0.55f, -0.5f, 0.0f);
	glVertex3f(0.8f, -0.6f, 0.0f);


	glEnd();

}
void LetterB(void)
{


	glScalef(ScaleX, ScaleY, ScaleZ);

	glBegin(GL_QUADS);
	glColor3f(lettersR, lettersG, lettersB);

	glVertex3f(-0.75f, 1.0f, 0.0f);
	glVertex3f(-0.4f, 1.0f, 0.0f);
	glVertex3f(-0.4f, -1.0f, 0.0f);
	glVertex3f(-0.75f, -1.0f, 0.0f);

	glVertex3f(-0.4f, 1.0f, 0.0f);
	glVertex3f(-0.4f, 0.7f, 0.0f);
	glVertex3f(0.35f, 0.7f, 0.0f);
	glVertex3f(0.5f, 1.0f, 0.0f);

	glVertex3f(0.8f, 0.5f, 0.0f);
	glVertex3f(0.5f, 1.0f, 0.0f);
	glVertex3f(0.35f, 0.7f, 0.0f);
	glVertex3f(0.55f, 0.4f, 0.0f);

	glVertex3f(0.6f, 0.2f, 0.0f);
	glVertex3f(-0.4f, 0.2f, 0.0f);
	glVertex3f(-0.4f, -0.2f, 0.0f);
	glVertex3f(0.6f, -0.2f, 0.0f);

	glVertex3f(-0.4f, -1.0f, 0.0f);
	glVertex3f(-0.4f, -0.7f, 0.0f);
	glVertex3f(0.35f, -0.7f, 0.0f);
	glVertex3f(0.5f, -1.0f, 0.0f);

	glVertex3f(0.8f, -0.5f, 0.0f);
	glVertex3f(0.5f, -1.0f, 0.0f);
	glVertex3f(0.35f, -0.7f, 0.0f);
	glVertex3f(0.55f, -0.4f, 0.0f);

	glEnd();

	glBegin(GL_POLYGON);
	glVertex3f(0.8f, 0.5f, 0.0f);
	glVertex3f(0.55f, 0.4f, 0.0f);
	glVertex3f(0.55f, 0.1f, 0.0f);
	glVertex3f(0.6f, 0.1f, 0.0f);
	glVertex3f(0.75f, 0.2f, 0.0f);
	glEnd();

	glBegin(GL_POLYGON);
	glVertex3f(0.8f, -0.5f, 0.0f);
	glVertex3f(0.55f, -0.4f, 0.0f);
	glVertex3f(0.55f, -0.1f, 0.0f);
	glVertex3f(0.6f, -0.1f, 0.0f);
	glVertex3f(0.75f, -0.2f, 0.0f);
	glEnd();

}
void LetterR(void)
{


	glScalef(ScaleX, ScaleY, ScaleZ);

	glBegin(GL_QUADS);
	glColor3f(lettersR, lettersG, lettersB);

	glVertex3f(0.5f, 1.0f, 0.0f);
	glVertex3f(0.2f, 1.0f, 0.0f);
	glVertex3f(0.2f, -0.2f, 0.0f);
	glVertex3f(0.5f, -0.2f, 0.0f);

	glVertex3f(-0.75f, 1.0f, 0.0f);
	glVertex3f(-0.4f, 1.0f, 0.0f);
	glVertex3f(-0.4f, -1.0f, 0.0f);
	glVertex3f(-0.75f, -1.0f, 0.0f);

	glVertex3f(0.2f, 0.2f, 0.0f);
	glVertex3f(-0.4f, 0.2f, 0.0f);
	glVertex3f(-0.4f, -0.2f, 0.0f);
	glVertex3f(0.2f, -0.2f, 0.0f);

	glVertex3f(0.2f, 1.0f, 0.0f);
	glVertex3f(-0.4f, 1.0f, 0.0f);
	glVertex3f(-0.4f, 0.6f, 0.0f);
	glVertex3f(0.2f, 0.6f, 0.0f);

	glVertex3f(0.0f, -0.2f, 0.0f);
	glVertex3f(-0.4f, -0.2f, 0.0f);
	glVertex3f(0.0f, -1.0f, 0.0f);
	glVertex3f(0.4f, -1.0f, 0.0f);

	glEnd();

}
void LetterL(void)
{
	glScalef(ScaleX, ScaleY, ScaleZ);

	glBegin(GL_QUADS);
	glColor3f(lettersR, lettersG, lettersB);

	glVertex3f(0.2f, 1.0f, 0.0f);
	glVertex3f(-0.2f, 1.0f, 0.0f);
	glVertex3f(-0.2f, -1.0f, 0.0f);
	glVertex3f(0.2f, -1.0f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glVertex3f(1.2f, -1.0f, 0.0f);
	glVertex3f(1.2f, -0.6f, 0.0f);
	glVertex3f(-0.2f, -0.6f, 0.0f);
	glVertex3f(-0.2f, -1.0f, 0.0f);
	glEnd();


}

void update(void)
{
	// Code

		//transformation of car
	if (scene_1 == TRUE)
	{
		if (sCarPosition.y < 0.9f)
		{
			sCarPosition.y = sCarPosition.y + 0.0002f;
		}
		else
		{
			sCarBrake.r = 1.0f;
			sCarBrake.g = 0.1f;
			sCarBrake.b = 0.0f;
			//sceneChange_1 = TRUE;
			scene_2 = TRUE;
			scene_1 = FALSE;
			//PlaySound(NULL, 0, 0);
			//PlaySound(MAKEINTRESOURCE(MYWAVE4), GetModuleHandle(NULL), SND_RESOURCE | SND_ASYNC);
		}
	}

	// transformation flag for scene 2

	if (scene_2 == TRUE)
	{
		if (sLinePosition.y < 0.9f)
		{
			sLinePosition.y = sLinePosition.y + 0.0002f;
		}
		else
		{
			scene_2 = FALSE;
			scene_3 = TRUE;
			PlaySound(NULL, 0, 0);
			PlaySound(MAKEINTRESOURCE(MYWAVE4), GetModuleHandle(NULL), SND_RESOURCE | SND_ASYNC);
		}
	}

	if (scene_3 == TRUE)
	{
		sGirlPositiontranslate.y = sGirlPositiontranslate.y + 0.000105f;
		if (sGirlPositiontranslate.y >= -0.4f)
		{
			sGirlPositiontranslate.y = -0.4f;
			doorClose = FALSE;
		}

		if (sLinePosition2.y < -0.35f)
		{
			sLinePosition2.y = sLinePosition2.y + 0.00009f;
		}
		else
		{
			scene_3 = FALSE;
			scene_4 = TRUE;

			PlaySound(NULL, 0, 0);
			PlaySound(MAKEINTRESOURCE(MYWAVE1), GetModuleHandle(NULL), SND_RESOURCE | SND_ASYNC);
		}
	}

	if (sDanceCharacterPositionLeft.x < 0.0f)
	{
		sDanceCharacterPositionLeft.x = sDanceCharacterPositionLeft.x + 0.0005f;
	}

	if (scene_4 == TRUE)
	{

		if (sPositionHappy.y > 0.5f)
		{
			sPositionHappy.y = sPositionHappy.y - 0.0062f;
		}
		if (sPositionBirthday.y > 0.3f)
		{
			sPositionBirthday.y = sPositionBirthday.y - 0.0062f;
		}
		if (sPositionDipali.y > 0.1f)
		{
			sPositionDipali.y = sPositionDipali.y - 0.0062f;
		}


		if (sLinePosition1.y > -0.65f)
		{
			sLinePosition1.y = sLinePosition1.y - 0.0016f;
		}
		else
		{
			scene_4 = FALSE;
			scene_5 = TRUE;

			PlaySound(NULL, 0, 0);
			PlaySound(MAKEINTRESOURCE(MYWAVE3), GetModuleHandle(NULL), SND_RESOURCE | SND_ASYNC | SND_LOOP);
		}
	}


	if (scene_5 == TRUE)
	{

		if (check2 == FALSE)
		{
			Bx = Bx + 0.0004f;
			if (Bx >= 0.09f)
				check2 = TRUE;
		}
		if (check2 == TRUE)
		{
			Bx = Bx - 0.0004f;
			if (Bx <= 0.0f)
			{
				check2 = FALSE;
				if (check == FALSE)
					check = TRUE;
				else
					check = FALSE;
			}
		}

		if (sLinePosition3.y < -0.14f)
		{
			sLinePosition3.y = sLinePosition3.y + 0.000001f;
		}
		else
		{
			scene_5 = FALSE;

			PlaySound(NULL, 0, 0);
		}
	

	}

	//yCoordinate += yCoordinateChange;
	//if (yCoordinate >= topYCoordinate) {
	//	yCoordinate = bottomYCoordinate;
	//}
	// for music icon

	if (T1Ch == TRUE)
		M1T = M1T + 0.25f;
	if (M1T >= 3.0f)
	{
		T1Ch = FALSE;
		M1T = -2.0f;
	}
	else if (M1T >= 1.0f)
	{
		T2Ch = TRUE;
	}

	//M1S = M1S + 0.001f;
	//if (M1S >= 2.5f)
	//{
	//	M1S = 1.0f;
	//}

	if (T2Ch == TRUE)
		M2T = M2T + 0.25f;
	if (M2T >= 4.0f)
	{
		T2Ch = FALSE;
		M2T = -2.0f;
	}
	else if (M2T >= 1.0f)
	{
		T1Ch = TRUE;
	}

	// for dance


	// scene 4 guitar guy
	


}


void uninitialized(void)
{
	//  function declarations

	void ToggleFullscreen(void);
	if (gbFullscreen == TRUE)
	{
		ToggleFullscreen();
		gbFullscreen = FALSE;
	}

	// make the hdc as current context

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;

	}

	// release the hdc

	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
	}

	// code
	// if application is exiting in fullscreen
	// destroy window
	if (ghwnd)
	{
		DestroyWindow(ghwnd);
		ghwnd = NULL;
	}

	// close the log file

	if (gpFile)
	{
		fprintf(gpFile, "Program ended successfully \n");
		fclose(gpFile);
		gpFile = NULL;
	}
}




// Header Files
// Common Header file
#include<windows.h>
#include<stdio.h>
#include<stdlib.h>
#include<math.h>

// OpenGL header file 
#include<gl/GL.h>
#include<gl/GLU.h>

// Standard header files
#include"OGL.h"

// Macros
#define WIN_WIDTH 800
#define WIN_HEIGHT 800 
#define RADIUS 0.1f
#define PS_PI 3.142f

// Link with OpneGL library
#pragma comment(lib,"OpenGL32.lib")
#pragma comment(lib,"glu32.lib")

// Glbobal function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// Global variable
FILE* gpFile = NULL;

HWND ghwnd = NULL;

DWORD dwStyle = 0;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

BOOL gbFullScreen = FALSE;
BOOL gbActive = FALSE;

// OpenGL relatef winddow 
HDC ghdc = NULL;
HGLRC ghrc = NULL;

float x = 0.0f;
float y = 0.0f;
float angleRadian;

float M1T = -1.5f;
float T1Ch = TRUE;
float M1S = 1.0f;

float M2T = -1.5f;
float T2Ch = FALSE;
// Entry-point
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// Function declaration
	int initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	// Local variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("PVSWindow");
	int iResult = 0;
	BOOL bDone = FALSE;
	float screen_width;
	float screen_length;

	// Code
	gpFile = fopen("log.txt", "w");
	if (gpFile == NULL)
	{
		MessageBox(
			NULL,
			TEXT("FAILED TO CREATE LOG FILE...."),
			TEXT("ERROR : 420"),
			MB_OK | MB_ICONERROR
		);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Program started successfully\n");
	}
	 
	// WNDCLASSEX Initialization
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	
	// Registaring above wndclass
	RegisterClassEx(&wndclass);

	// Create the window

	screen_width = GetSystemMetrics(0);
	screen_length = GetSystemMetrics(1);

	hwnd = CreateWindowEx(
		WS_EX_APPWINDOW,
		szAppName,
		TEXT("Paras Vijay Satpute."),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		(screen_width - WIN_WIDTH) / 2,
		(screen_length - WIN_HEIGHT) / 2,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);

	ghwnd = hwnd;

	iResult = initialize();
	if (iResult != 0)
	{
		MessageBox(
			NULL,
			TEXT("initiallzie function failed...."),
			TEXT("ERROR"),
			MB_OK | MB_ICONERROR
		); 
		DestroyWindow(hwnd);
	}

	// Show the window
	ShowWindow(hwnd, iCmdShow);

	// Paint/Redraw the window
	UpdateWindow(hwnd);

	// Game Loop
	while (bDone == FALSE)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = TRUE;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActive == TRUE)
			{
				// Display
				display();
				// Update 
				update();
					
			}
		}
	}
	
	// Uninitialize
	uninitialize();

	return((int)msg.wParam);
}

// Callback function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// Funtion Declaration
	void ToggleFullscreen(void);
	void resize(int,int);

	// Code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActive = TRUE;
		break;
	case WM_KILLFOCUS:
		gbActive = FALSE;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_KEYDOWN:
		switch (LOWORD(wParam))
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;
	case WM_CHAR:
		switch (LOWORD(wParam))
		{
		case 'F':
		case 'f':
			if (gbFullScreen == FALSE)
			{
				ToggleFullscreen();
				gbFullScreen = TRUE;
			}
			else
			{
				ToggleFullscreen();
				gbFullScreen = FALSE;
			}
			break;
		}
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}


void ToggleFullscreen(void)
{
	// Local variable declaration
	MONITORINFO mi = { sizeof(MONITORINFO) };

	// Code
	if (gbFullScreen == FALSE)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) &&
				GetMonitorInfo(MonitorFromWindow(ghwnd,
					MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED
				);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
}

int initialize(void)
{
	// Function declarations
	void resize(int, int);

	// Variable declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;

	// Code
	ZeroMemory(&pfd,sizeof(PIXELFORMATDESCRIPTOR));

	// Initialization of pfd
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);
	if (ghdc == NULL)
	{
		fprintf(gpFile, "GetDC faileds....\n");
		return(-1);
	}

	// 
	iPixelFormatIndex = ChoosePixelFormat(ghdc,&pfd);
	if (iPixelFormatIndex == 0)
	{
		fprintf(gpFile, "iPixelFormat failed\n");
		return(-2);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		fprintf(gpFile,"SetPixelFormat failed \n\n");
		return(-3);

	}

	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL)
	{
		fprintf(gpFile, "wglCreateContext is failed.... \n");
		return(-4);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(gpFile, "wglMakeCurrent is failed \n");
		return(-5);
	}

	// Set clear color of window to blue
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	resize(WIN_WIDTH, WIN_HEIGHT);

	return(0);
}

void resize(int width, int height)
{
	// Code
	if (height <= 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();


	
}

void display(void)
{
	// Function declaration
	void musicModel1(void);
	void musicModel2(void);
	void BottomCircle(void);

	// Variable declaration


	// Code
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f, M1T + 0.3f, -0.0f);
//	glScalef(M1S, M1S, M1S);
	musicModel1();

	glLoadIdentity();
	glTranslatef(0.1f, (M1T), -0.0f);
	//glScalef((1.1f + M1S), (0.8f + M1S),(0.8f + M1S));
	//glScalef(M1S, M1S, M1S);
	BottomCircle();

	glLoadIdentity();
	glTranslatef(-0.3f, (M1T), -0.0f);
	//glScalef(1.1f, 0.8f, 0.8f);
	//glScalef(M1S, M1S, M1S);
	BottomCircle();


	glLoadIdentity();
	glTranslatef(0.0f, M2T + 0.5, -0.0f);
	musicModel2();
	glLoadIdentity();
	glTranslatef(-0.1f, (M2T  + 0.2f), -0.0f);
	//glScalef(1.1f, 0.8f, 0.8f);
	BottomCircle();

	glLoadIdentity();
	glTranslatef(0.0f, (M2T - 0.5f), -0.0f);
	musicModel2();
	glLoadIdentity();
	glTranslatef(-0.1f, (M2T - 0.8f), -0.0f);
	//glScalef(1.1f, 0.8f, 0.8f);
	BottomCircle();

	SwapBuffers(ghdc);
}

void update(void)
{
	// Code
	if(T1Ch == TRUE)
	M1T = M1T + 0.003f;
	if (M1T >= 3.0f)
	{
		T1Ch = FALSE;
		M1T = -1.5f;
	}
	else if (M1T >= 1.0f)
	{
		T2Ch = TRUE;
	}

	//M1S = M1S + 0.001f;
	//if (M1S >= 2.5f)
	//{
	//	M1S = 1.0f;
	//}

	if (T2Ch == TRUE)
		M2T = M2T + 0.003f;
	if (M2T >= 4.0f)
	{
		T2Ch = FALSE;
		M2T = -1.5f;
	}
	else if (M2T >= 1.0f)
	{
		T1Ch = TRUE;
	}
}

void uninitialize(void)
{
	// Function declaration
	void ToggleFullscreen(void);

	// Code
	// If application is exiting in fullscreen then this code is execute
	if (gbFullScreen == TRUE)
	{
		ToggleFullscreen();
		gbFullScreen = FALSE;
	}

	// Make the hdc as current 
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);

	}	

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	// Release the hdc
	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	
	// Destroy window
	if (ghwnd)
	{
		DestroyWindow(ghwnd);
		ghwnd = NULL;
	}

	// Close the log file
	if (gpFile)
	{
		fclose(gpFile);
		gpFile = NULL;
	}
}

void musicModel1(void)
{
	glLineWidth(10.0f);
	glBegin(GL_LINES);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glVertex3f(-0.2f, 0.0f, 0.0f);
	glEnd();

	glLineWidth(2.0f);
	glBegin(GL_LINES);
	glVertex3f(0.2f, 0.0f, 0.0f);
	glVertex3f(0.2f, -0.3f, 0.0f);

	glVertex3f(-0.2f, 0.0f, 0.0f);
	glVertex3f(-0.2f, -0.3f, 0.0f);
	glEnd();

}

void musicModel2(void)
{
	glBegin(GL_POLYGON);
	glVertex3f(0.0f, 0.0f, 0.0f);

	glVertex3f(0.0f, -0.1f, 0.0f);
	glVertex3f(0.15f, -0.1f, 0.0f);
	glVertex3f(0.2f, -0.15f, 0.0f);
	glVertex3f(0.2f, -0.1f, 0.0f);
	glVertex3f(0.15f, -0.05f, 0.0f);
	glVertex3f(0.0f, -0.05f, 0.0f);

	glEnd();

	glBegin(GL_LINES);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, -0.3f, 0.0f);

	glEnd();
	

}

void BottomCircle(void)
{
	glScalef(1.1f, 0.8f, 0.8f);

	glBegin(GL_TRIANGLE_STRIP);
	for (float fAngle = 0.0f; fAngle <= 360.0f; fAngle++)
	{

		angleRadian = fAngle * PS_PI / 180.0f;

		x = RADIUS * cos(angleRadian);
		y = RADIUS * sin(angleRadian);
		glColor3f(1.0f, 1.0f, 1.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(x, y, 0.0f);

	}
	glEnd();
}
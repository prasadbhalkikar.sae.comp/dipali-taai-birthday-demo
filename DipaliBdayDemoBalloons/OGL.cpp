// Header files
#include <GL/freeglut.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <mmsystem.h>
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include"OGL.h"
#pragma comment(lib, "winmm.lib")

// Global variable declarations
FILE* gpFile = NULL;
bool bIsFullScreen = false;
#define PAB_PI 3.14159265359

float balloonTranslate1 = 2.0f;
// Entry-point function
int main(int argc, char* argv[])
{
	// function declarations
	void initialize(void);
	void resize(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);
	void uninitialize(void);

	// code
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
	int width = GetSystemMetrics(SM_CXMAXIMIZED);
	int height = GetSystemMetrics(SM_CYMAXIMIZED);
	glutInitWindowSize(800, 800);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("Happy Birthday Madam :)");

	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Creation of Log file Failed. Exitting..."), TEXT("File I/O"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log file Successfully Created. \n");
	}

	initialize();

	glutReshapeFunc(resize);
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutCloseFunc(uninitialize);

	glutMainLoop();

	return(0);
}

void initialize(void)
{
	// code
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	//PlaySound(MAKEINTRESOURCE(MYWAVE), GetModuleHandle(NULL), SND_RESOURCE | SND_ASYNC | SND_LOOP);

}

void resize(int width, int height)
{
	// code
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

float translateZ = 3.0f;


void balloonString()
{
	glBegin(GL_LINE_STRIP);

	glColor3f(1.0f, 1.0f, 1.0f);

	glVertex3f(0.1f, 0.2f, 0.0f);
	glVertex3f(0.07f, 0.15f, 0.0f);
	glVertex3f(0.07f, 0.14f, 0.0f);
	glVertex3f(0.08f, 0.08f, 0.0f);
	glVertex3f(0.09f, 0.06f, 0.0f);
	glVertex3f(0.095f, 0.04f, 0.0f);
	glVertex3f(0.1f, 0.0f, 0.0f);
	glVertex3f(0.1f, -0.06f, 0.0f);
	glVertex3f(0.1f, -0.1f, 0.0f);
	glVertex3f(0.07f, -0.15f, 0.0f);
	glVertex3f(0.05f, -0.18f, 0.0f);

	glEnd();
}

void circle(float R, float G, float B)
{
	float fAngle = 0.0f;
	float x = 0.0f, y = 0.0f;
	const float radius = 0.5f;
	float angleInRadians = 0.0f;
	// 135, 206, 235
	glBegin(GL_LINE_LOOP);

	for (fAngle = 0.0f; fAngle < 360.0f; fAngle = fAngle + 0.2f)
	{
		angleInRadians = fAngle * (PAB_PI / 180.0f);

		x = cos(angleInRadians) * radius;
		y = sin(angleInRadians) * radius;

		//glColor3f(135.0f / 255.0f, 206.0f / 255.0f, 235.0f / 255.0f);
		glColor3f(R / 255.0f, G / 255.0f, B / 255.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(x, y, 0.0f);

	}

	glEnd();
}


void drawBalloon(float R, float G, float B)
{
	glPushMatrix();
	{

		glPushMatrix();
		{
			//glScalef(0.2f, 0.2f, 1.0f);

			glScalef(0.8f, 1.0f, 1.0f);
			circle(R, G, B);
		}
		glPopMatrix();

		glPushMatrix();
		{
			glScalef(1.0f, 2.0f, 1.0f);

			glTranslatef(-0.075f, -0.42f, 0.0f);
			balloonString();
		}
		glPopMatrix();
	}
	glPopMatrix();


}

void balloon()
{
	glTranslatef(-2.0f, 5.0f, -15.0f);

	glPushMatrix();
	{
		glTranslatef(-5.0f, -15.0f, 0.0f);

		glPushMatrix();
		{
			glTranslatef(0.0f, balloonTranslate1, 0.0f);
			drawBalloon(0.0f, 0.0f, 255.0f);
		}
		glPopMatrix();

	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-2.5f, -15.0f, 0.0f);

		glPushMatrix();
		{
			glTranslatef(0.0f, balloonTranslate1, 0.0f);
			drawBalloon(255.0f, 0.0f, 0.0f);
		}
		glPopMatrix();

	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-7.5f, -15.0f, 0.0f);

		glPushMatrix();
		{
			glTranslatef(0.0f, balloonTranslate1, 0.0f);
			drawBalloon(128.0f, 0.0f, 128.0f);
		}
		glPopMatrix();

	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(7.5f, -15.0f, 0.0f);

		glPushMatrix();
		{
			glTranslatef(0.0f, balloonTranslate1, 0.0f);
			drawBalloon(255.0f, 0.0f, 128.0f);
		}
		glPopMatrix();

	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(9.5f, -15.0f, 0.0f);

		glPushMatrix();
		{
			glTranslatef(0.0f, balloonTranslate1, 0.0f);
			drawBalloon(255.0f, 128.0f, 128.0f);
		}
		glPopMatrix();

	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(11.5f, -15.0f, 0.0f);

		glPushMatrix();
		{
			glTranslatef(0.0f, balloonTranslate1, 0.0f);
			drawBalloon(128.0f, 255.0f, 255.0f);
		}
		glPopMatrix();

	}
	glPopMatrix();
}
int balloonset = 0;

void display(void)
{
	// function declarations
	void Tent();
	void update();
	void vinit();
	// code
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	balloon();
	
	glutSwapBuffers();
	glutPostRedisplay();

	// calling update
	update();
}


void update(void)
{
	balloonset++;

	balloonTranslate1 = balloonTranslate1 + 0.00199f;

}

void keyboard(unsigned char key, int x, int y)
{
	// code
	switch (key)
	{
	case 27:
		// uninitialize();
		glutLeaveMainLoop();
		break;
	case 'F':
	case 'f':
		if (bIsFullScreen == false)
		{
			glutFullScreen();
			bIsFullScreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bIsFullScreen = false;
		}
		break;

	default:
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	// code
	switch (button)
	{
	case GLUT_RIGHT_BUTTON:
		glutLeaveMainLoop();
		break;
	default:
		break;
	}
}

void uninitialize(void)
{
	// code
	if (gpFile)
	{
		fprintf(gpFile, "Log file Successfully Closed.\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}


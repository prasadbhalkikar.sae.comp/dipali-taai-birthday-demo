

// Windows Header Files
#include<windows.h> //win32 api sathi
#include<stdio.h> // For FileIO
#include<stdlib.h> // For exit()
#include<math.h>

// OpenGl Header Files
#include<gl/GL.h>

#include"OGL.h" // user defined 

// Macros
#define WIN_WIDTH 700
#define WIN_HEIGHT 700

#define SSH_PI 3.14159

// Link With OpenGL Library
#pragma comment(lib, "OpenGL32.lib")

// Global Function Declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// Global Variable Declarations For FILE-IO opearation
FILE* gpFILE = NULL;

// Global Variable Declarations for FullScreen
HWND ghwnd = NULL;
BOOL gbActive = FALSE; // by default maji window active nahi
DWORD dwStyle = 0;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
BOOL gbFullscreen = FALSE;

// OpenGL related Global Variables
HDC ghdc = NULL;
HGLRC ghrc = NULL;


// Entry Point Function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// Function Declarations
	int initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	// Local Variable Declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	int x = GetSystemMetrics(SM_CXSCREEN);
	int y = GetSystemMetrics(SM_CYSCREEN);
	
	int i_X = (x - WIN_WIDTH) / 2;
	int i_Y = (y - WIN_HEIGHT) / 2;

	TCHAR szAppName[] = TEXT("SSHWindow");
	int iResult = 0;
	BOOL bDone = FALSE;

	// code

	gpFILE = fopen("Log.txt", "w");
	if (gpFILE == NULL)
	{

		MessageBox(NULL, TEXT("Log File Cannot Be Open"), TEXT("Error"), MB_OK | MB_ICONERROR);
		exit(0);

	}
	fprintf(gpFILE, "Program Started Successfully\n");

	// WNDCLASSEX Initialization
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	// Register WNDCLASSEX 
	RegisterClassEx(&wndclass);

	// Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Dipali Bday Demo..."),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		i_X,  // X co-ordinate majya window cha(top left)
		i_Y,  // Y -------------majya ---------(same as above)
		WIN_WIDTH,  //  width ----maji
		WIN_HEIGHT,  // height window maji
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	// initialization 
	iResult = initialize();
	if (iResult != 0)
	{
		MessageBox(hwnd, TEXT("initialize() Failed."), TEXT("Error"), MB_OK | MB_ICONERROR);
		DestroyWindow(hwnd);

	}

	// Show the Window
	ShowWindow(hwnd, iCmdShow);

	SetForegroundWindow(hwnd); // maja window la foreground la ann. 

	SetFocus(hwnd);


	// GAME Loop
	while (bDone == FALSE)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = TRUE;

			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActive == TRUE)
			{
				// Render 
				display();

				// Update
				update();
			}

		}
	}

	// uninitialization
	uninitialize();

	return((int)msg.wParam);

}


// CALLBACK Function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{

	// Variable Declarations

	// code

	// Function Declaration
	void ToggleFullscreen(void);
	void resize(int, int);

	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActive = TRUE;
		break;

	case WM_KILLFOCUS:
		gbActive = FALSE;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_KEYDOWN:
		switch (LOWORD(wParam))
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;

	case WM_CHAR:
		switch (LOWORD(wParam))
		{
		case 'F':
		case 'f':
			if (gbFullscreen == FALSE)
			{
				ToggleFullscreen();
				gbFullscreen = TRUE;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = FALSE;
			}
			break;
		}
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		if (gpFILE)
		{
			fprintf(gpFILE, "Program Ended Successfully\n");

		}
		PostQuitMessage(0);
		break;

	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}


void ToggleFullscreen(void)
{
	// Local Variable Declarations
	MONITORINFO mi = { sizeof(MONITORINFO) };

	// code
	if (gbFullscreen == FALSE)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);

	}
	else
	{
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

}

int initialize(void)
{
	// Function Declarations

	// Code
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;


	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	// initialization of PIXELFORMATDESCRIPTOR
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);
	if (ghdc == NULL)
	{
		fprintf(gpFILE, "GetDC() Failed\n");
		return(-1);
	}

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		fprintf(gpFILE, "ChoosePixelFormat() Failed\n");
		return(-2);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		fprintf(gpFILE, "SetPixelFormat() Failed\n");
		return(-3);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		fprintf(gpFILE, "wglCreateContext() Failed\n");
		return(-4);
	}

	// make rendering context current

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(gpFILE, "wglMakeCurrent() Failed\n");
		return(-5);
	}

	// 
	glClearColor(255.0f/255.0f, 255.0f/255.0f, 255.0f/255.0f, 1.0f);

	return(0);
}

void resize(int width, int height)
{
	// Code
	if (height <= 0)
		height = 1;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, (GLsizei)width, (GLsizei)height); // viewport mhanje binoculor
}


void display(void)
{
	// Code
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glBegin(GL_QUADS); // Car Base
	glColor3f(255.0f/255.0f, 59.0f/255.0f, 139.0f/255.0f);
    glVertex3f(0.6f, -0.2f, 0.0f);
	glVertex3f(0.6f, 0.0f, 0.0f);
	glVertex3f(-0.6f, 0.0f, 0.0f);
	glVertex3f(-0.6f, -0.2f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); // Car Upper Base
	glColor3f(255.0f/255.0f, 59.0f/255.0f, 139.0f/255.0f);
    glVertex3f(0.35f, 0.0f, 0.0f);
	glVertex3f(0.25f, 0.2f, 0.0f);
	glVertex3f(-0.25f, 0.2f, 0.0f);
	glVertex3f(-0.35f, 0.0f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); // Back Widow
	glColor3f(188.0f/255.0f, 188.0f/255.0f, 237.0f/255.0f);
    glVertex3f(-0.02f, 0.0f, 0.0f);
	glVertex3f(-0.02f, 0.15f, 0.0f);
	glVertex3f(-0.25f, 0.15f, 0.0f);
	glVertex3f(-0.3f, 0.0f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); // Front Widow
	glColor3f(188.0f/255.0f, 188.0f/255.0f, 237.0f/255.0f);
    glVertex3f(0.02f, 0.0f, 0.0f);
	glVertex3f(0.02f, 0.15f, 0.0f);
	glVertex3f(0.25f, 0.15f, 0.0f);
	glVertex3f(0.3f, 0.0f, 0.0f);
	glEnd();

	glBegin(GL_QUADS); // Car Light
	glColor3f(246.0f/255.0f, 255.0f/255.0f, 91.0f/255.0f);
    glVertex3f(0.9f, -0.15f, 0.0f);
	glVertex3f(0.9f, 0.0f, 0.0f);
	glVertex3f(0.6f, -0.05f, 0.0f);
	glVertex3f(0.6f, -0.1f, 0.0f);
	glEnd();

	//---------------- Car Wheels-------------

	// back mud flaps
	float angle = 0.0f;
	float x = 0.0f;
	float y = 0.0f;
	
    float angleRadian = 0.0f;
    
	glBegin(GL_LINES);

	for(angle = 0.0f; angle <= 180.0f; angle = angle + 0.10f)
	{
		float r = 0.12f;
        angleRadian = angle * ( SSH_PI / 180.0f );
		x = cos(angleRadian) * r;
		y = sin(angleRadian) * r;

        glColor3f(1.0f,1.0f,1.0f);
        glVertex3f(-0.4f, -0.2f, 0.0f);
        glColor3f(1.0f,1.0f,1.0f);
		glVertex3f(x + (-0.4f), y + (-0.2f), 0.0f);
	}

	glEnd();

	// front mud flaps
     
	glBegin(GL_LINES);

	for(angle = 0.0f; angle <= 180.0f; angle = angle + 0.10f)
	{
		float r = 0.12f;
        angleRadian = angle * ( SSH_PI / 180.0f );
		x = cos(angleRadian) * r;
		y = sin(angleRadian) * r;

        glColor3f(1.0f,1.0f,1.0f);
        glVertex3f(0.4f, -0.2f, 0.0f);
        glColor3f(1.0f,1.0f,1.0f);
		glVertex3f(x + (0.4f), y + (-0.2f), 0.0f);
	}

	glEnd();

	//back wheels

	glBegin(GL_LINES); // outer circle

	for(angle = 0.0f; angle <= 360.0f; angle = angle + 0.10f)
	{
		float r = 0.1f;
        angleRadian = angle * ( SSH_PI / 180.0f );
		x = cos(angleRadian) * r;
		y = sin(angleRadian) * r;

        glColor3f(0.0f,0.0f,0.0f);
        glVertex3f(-0.4f, -0.2f, 0.0f);
        glColor3f(0.0f,0.0f,0.0f);
		glVertex3f(x + (-0.4f), y + (-0.2f), 0.0f);
	}

	glEnd();

	
	glBegin(GL_LINES); // inner circle 

	for(angle = 0.0f; angle <= 360.0f; angle = angle + 0.10f)
	{
		float r = 0.08f;
        angleRadian = angle * ( SSH_PI / 180.0f );
		x = cos(angleRadian) * r;
		y = sin(angleRadian) * r;

        glColor3f(1.0f,1.0f,1.0f);
        glVertex3f(-0.4f, -0.2f, 0.0f);
        glColor3f(1.0f,1.0f,1.0f);
		glVertex3f(x + (-0.4f), y + (-0.2f), 0.0f);
	}

	glEnd();


	//Front wheels

	glBegin(GL_LINES); // outer circle

	for(angle = 0.0f; angle <= 360.0f; angle = angle + 0.10f)
	{
		float r = 0.1f;
        angleRadian = angle * ( SSH_PI / 180.0f );
		x = cos(angleRadian) * r;
		y = sin(angleRadian) * r;

        glColor3f(0.0f,0.0f,0.0f);
        glVertex3f(0.4f, -0.2f, 0.0f);
        glColor3f(0.0f,0.0f,0.0f);
		glVertex3f(x + (0.4f), y + (-0.2f), 0.0f);
	}

	glEnd();

	
	glBegin(GL_LINES); // Inner Circle

	for(angle = 0.0f; angle <= 360.0f; angle = angle + 0.10f)
	{
		float r = 0.08f;
        angleRadian = angle * ( SSH_PI / 180.0f );
		x = cos(angleRadian) * r;
		y = sin(angleRadian) * r;

        glColor3f(1.0f,1.0f,1.0f);
        glVertex3f(0.4f, -0.2f, 0.0f);
        glColor3f(1.0f,1.0f,1.0f);
		glVertex3f(x + (0.4f), y + (-0.2f), 0.0f);
	}

	glEnd();
	

	glLineWidth(2.0f);
	glBegin(GL_LINE_LOOP);
	glColor3f(0.0f,0.0f,0.0f);
	glVertex3f(-0.01f, 0.0f, 0.0f);
	glVertex3f(-0.01f, 0.16f, 0.0f);
	glVertex3f(-0.25f, 0.16f, 0.0f);
	glVertex3f(-0.32f, -0.01f, 0.0f);
	glVertex3f(-0.01f, -0.01f, 0.0f);

	glEnd();

	glLineWidth(2.0f);
	glBegin(GL_LINE_LOOP);
	glColor3f(0.0f,0.0f,0.0f);
	glVertex3f(0.01f, 0.0f, 0.0f);
	glVertex3f(0.01f, 0.16f, 0.0f);
	glVertex3f(0.25f, 0.16f, 0.0f);
	glVertex3f(0.32f, -0.01f, 0.0f);
	glVertex3f(0.01f, -0.01f, 0.0f);

	glEnd();

	glBegin(GL_LINES);
	glColor3f(0.0f,0.0f,0.0f);
	glVertex3f(0.0f, 0.18f, 0.0f);
	glVertex3f(0.0f, -0.19f, 0.0f);
	
	glEnd();

	glBegin(GL_LINES);
	glColor3f(0.0f,0.0f,0.0f);
	glVertex3f(-0.25f, 0.18f, 0.0f);
	glVertex3f(0.25f, 0.18f, 0.0f);
	
	glEnd();

	glBegin(GL_LINES);
	glColor3f(0.0f,0.0f,0.0f);
	glVertex3f(-0.27f, -0.19f, 0.0f);
	glVertex3f(0.27f, -0.19f, 0.0f);
	
	glEnd();

	glBegin(GL_LINE_LOOP); // back Door handle 
	glColor3f(0.0f,0.0f,0.0f);
	glVertex3f(-0.01f, -0.05f, 0.0f);
	glVertex3f(-0.01f, -0.03f, 0.0f);
	glVertex3f(-0.03f, -0.04f, 0.0f);
	
	glEnd();

	glBegin(GL_LINE_LOOP); // Front Door handle 
	glColor3f(0.0f,0.0f,0.0f);
	glVertex3f(0.01f, -0.05f, 0.0f);
	glVertex3f(0.01f, -0.03f, 0.0f);
	glVertex3f(0.03f, -0.04f, 0.0f);
	
	glEnd();
	
	SwapBuffers(ghdc);

}

void update(void)
{
	// code

}

void uninitialize(void)
{
	// Function Declarations
	void ToggleFullscreen(void);

	// code
	// if application is exiting fullscreen than this If will be followed
	if (gbFullscreen == TRUE)
	{
		ToggleFullscreen();
		gbFullscreen = FALSE;
	}


	// make the os hdc as current context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	// delete rendering context
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	// release the hdc
	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// DestroyWindow 
	if (ghwnd)
	{
		DestroyWindow(ghwnd);
		ghwnd = NULL;
	}

	// close the log file
	if (gpFILE)
	{
		fprintf(gpFILE, "Program Ended Successfully\n");
		fclose(gpFILE);
		gpFILE = NULL;
	}

}


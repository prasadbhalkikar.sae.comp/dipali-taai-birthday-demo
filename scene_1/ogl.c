//windows header files
#include <Windows.h>
#include <stdio.h>
#include <stdlib.h>

//opengl Header files
#include <gl/GL.h>
#include <gl/GLU.h>
#include "ogl.h"
#include <math.h>


//MACROS
#define WIN_WIDTH  800
#define WIN_HEIGHT  600
#define PI 3.14159
#define RAD(x)  (x * (PI/180.0f))

//link with opengl library
#pragma comment(lib,"OpenGL32.lib")
#pragma comment(lib,"glu32.lib")

//global func decln
LRESULT CALLBACK wndProc(HWND, UINT, WPARAM, LPARAM);  //callback func called by os as many times as

//global var decln
FILE* gpFILE = NULL;


HWND gHwnd = NULL;  //global window handle
BOOL gbActive = FALSE;
DWORD gdwStyle = 0;  //unsigned long 32bit
WINDOWPLACEMENT gwpPrev = { sizeof(WINDOWPLACEMENT) }; //previous window placement
BOOL gbFullScreen = FALSE; //win32s BOOL not c++ bool

//opengl global variables
HDC ghdc = NULL;
HGLRC ghrc = NULL;  //handle to opengl rendering context

//transformation variables
struct sPosition
{
	float x;
	float y;
	float z;
};

struct cColor
{
	float r;
	float g;
	float b;
};

struct sPosition sCarPosition = {0.0f,-0.75f,0.0f};
struct cColor sCarBrake = { 0.0f,0.0f,0.0f };


//entry point func
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPreInstance, LPSTR lpszCmdLine, int iCmdShow) //WinMain => --WinMainCRTStartup => --mainCRTStartup  (crt0.c)  CRT=? C Runtime 0th file  icmdshow => argc
{
	//function declaration
	int initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);



	//local var decln
	WNDCLASSEX wndclass; //window class typw of window , EX dor extended
	HWND hwnd; //handle though it uint*  its uint only bcoz can't dereference it. opaque ptr
	MSG msg;  //struct 
	TCHAR szAppName[] = TEXT("MYWindow");
	
	int xDesktopWindowWidth, yDesktopWindowHeight, x , y;
	int iResult = 0;

	BOOL bDone = FALSE;

	//code
	
	gpFILE = fopen("Log.txt", "w");
	if (gpFILE == NULL)
	{
		MessageBox(NULL, TEXT("Log file can not be opened"), TEXT("ERROR"), MB_OK | MB_ICONERROR);
		exit(0);
	}
	fprintf(gpFILE, "Program for Icon + FileIO + FullScreen started successfully!!!!\n");


	//WNDCLASSEX wndclass initialization predefined classes => button,dialog,scrollbar,statuc,dynamic
	wndclass.cbSize = sizeof(WNDCLASSEX); //new one
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC; //redraw after resize (class style)
	wndclass.cbClsExtra = 0; //count of bytes of extra in class
	wndclass.cbWndExtra = 0; //count of bytes of extra in window
	wndclass.lpfnWndProc = wndProc; //callback func
	wndclass.hInstance = hInstance; //give currenr instance to handl 
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH); // gdi32.dll haNDLE OF Stock brush from os  (getstockobjext => brush,font,paint ret val HGDIIBJ => handle graphic device interface obj  ) 
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON)); // to give our icon to our window..make resource from given int which is in rc file
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName; //in custom class classname is mandatory to assign
	wndclass.lpszMenuName = NULL; //menu name
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));//sm = small icon to display icon on taskbar  (new one)

	//register WNDCLASSEX
	RegisterClassEx(&wndclass); //register to os => os creates unique immutable string for this class of type ATOM
	
	xDesktopWindowWidth = GetSystemMetrics(SM_CXSCREEN);
	yDesktopWindowHeight = GetSystemMetrics(SM_CYSCREEN);

	x = (xDesktopWindowWidth - 800) / 2;
	y = (yDesktopWindowHeight - 600) / 2;
	
	//create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,szAppName, TEXT("Vinit Surve RTR5"), WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, x, //(type of window name, Caption bar name, style of window => on over all windows, where to show window onn desktop x,
		y, WIN_WIDTH, WIN_HEIGHT, NULL,  // y,width,height set to defaults, whose child window is this default => os's child window or HWND_DESKTOP can be written                     
		NULL, hInstance, NULL); //handle to menu, instance of whose window is to create, window msg after create so can be pass to wndproc through this param

	//style of window => 6types = WS_OVERLAPPPED | WS_CAPTION | WS_SYSMENU (move/resize..sys bcoz for all windows) | WS_THICKFRAME |WS_NAXIMIZEBOX | WS_MINIMIZEBOX 
	gHwnd = hwnd;

	//initialization
	iResult = initialize();
	if (iResult != 0)
	{
		MessageBox(hwnd, TEXT("initialize() failed"), TEXT("ERROR"), MB_OK | MB_ICONERROR);
		DestroyWindow(hwnd);
	}


	//show the window
	ShowWindow(hwnd, iCmdShow);  //to show window (handle of window, how to display window => default icmshow)
	
	SetForegroundWindow(hwnd); //sets window on foreground irrespective of children/sibling window pos
	SetFocus(hwnd);   //keeps window highlighted



	//game loop
	while (bDone == FALSE)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = TRUE;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActive == TRUE)  //if window is focused
			{
				//render
				display();

				//update
				update();
			}
		}
	}

	//uninitialization
	uninitialize();

	return (int)msg.wParam;


}

//callback func
LRESULT CALLBACK wndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//func decln
	void ToggleFullScreen(void);
	void resize(int, int); //width,height

	// code
	switch (iMsg)
	{
	
	case WM_SETFOCUS: //send when window gets active
	{
		gbActive = TRUE;
		break;
	}
	case WM_KILLFOCUS:
	{
		gbActive = FALSE;
		break;
	}
	case WM_SIZE:
	{
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	}
	/*case WM_ERASEBKGND:
	{
		return 0;
	}*/
	case WM_KEYDOWN: //for non char key (special symbol)
	{
		switch (LOWORD(wParam))
		{
		case VK_ESCAPE: //virtual code escape key
			/*
			//MessageBox(hwnd, TEXT("WM_KEYDOWN_VK_ESCAPE Arrived"), TEXT("Message"), MB_OK);
			if (gbFullScreen == TRUE)
			{
				ToggleFullScreen();
				gbFullScreen = FALSE;
				fprintf(gpFILE, "Esc key pressed and exited from full screen successfully!!!!\n");
				break;
			}
			*/
			DestroyWindow(hwnd); //internally send WM_DESTROY message
		}
		break;
	}
	case WM_CHAR: //for character keys (A-Z, a-z)
	{
		switch (LOWORD(wParam))
		{
		case 'F':
		case 'f':
			if (gbFullScreen == FALSE)
			{
				ToggleFullScreen();
				gbFullScreen = TRUE;
				fprintf(gpFILE, "F/f key pressed and entered into full screen successfully!!!!\n");
				break;
			}
			else
			{
				ToggleFullScreen();
				gbFullScreen = FALSE;
				fprintf(gpFILE, "F/f key pressed and exited into full screen successfully!!!!\n");
				break;
			}
			break;
		}
		break;
	}
	case WM_CLOSE:
	{
		//optional if need to run program though window is closed
		DestroyWindow(hwnd);
		break;
	}
	case WM_DESTROY:  //compulsory to handle bcoz unhandled left window alive though prog terminates
	{
		PostQuitMessage(0);
		break;
	}
	default:
		break;
	}
	return (DefWindowProc(hwnd, iMsg, wParam, lParam)); //default window procedure of os which handles other msges than handles by wndproc

}

void ToggleFullScreen(void)
{
	//local var decln
	MONITORINFO mInfo = { sizeof(MONITORINFO) };

	//code
	if (gbFullScreen == FALSE)
	{
		gdwStyle = GetWindowLong(gHwnd, GWL_STYLE);

		if (gdwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(gHwnd, &gwpPrev) && GetMonitorInfo(MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mInfo))
			{
				SetWindowLong(gHwnd, GWL_STYLE, (gdwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(gHwnd, HWND_TOP, mInfo.rcMonitor.left, mInfo.rcMonitor.top, (mInfo.rcMonitor.right - mInfo.rcMonitor.left), (mInfo.rcMonitor.bottom - mInfo.rcMonitor.top), SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(FALSE);
	}
	else
	{
		SetWindowPlacement(gHwnd, &gwpPrev);
		SetWindowLong(gHwnd, GWL_STYLE, (gdwStyle | WS_OVERLAPPEDWINDOW));
		SetWindowPos(gHwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
}

int initialize(void)
{
	//func decln
	void resize(int, int);

	//code
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//initialization of pixelformat descriptor
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;   //draw on window woth real time
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cBlueBits = 8;
	pfd.cGreenBits = 8;
	pfd.cAlphaBits = 8;


	ghdc = GetDC(gHwnd);
	if (ghdc == NULL)
	{
		fprintf(gpFILE, "GetDC() failed\n");
		return -1;
	}

	// step 3 
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	// should return non-zero positive value on success
	if (0 == iPixelFormatIndex)
	{
		fprintf(gpFILE, "ChoosePizelFormat() failed\n");
		return -2;
	}

	// Step 4: set obtained pixel format
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		fprintf(gpFILE, "SetPixelFormat() failed\n");
		return -3;
	}


	//(windows opengl bridging api => WGL windows graphics library)
	//(tell WGL  to give me opengl compatible dc)
	//(tell WGL  to give me opengl compatible context from this dc)
	//step 5: create opengl contexet from device context

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		fprintf(gpFILE, "wglCreateContext() failed\n");
		return -4;
	}
	//(now ghdc will give up its control and gave it to ghrc)
	
	//step6: make rendering context current
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(gpFILE, "wglMakeCurrent() failed\n");
		return -5;
	}

	//Here opengl Starts
	//set the clear color of window to blue
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);  //ranges from 0.0 to 1.0 => 2^32 (pfd.cColorBits = 32) not make screen blue , its just tell to use bluee brush/clr
	//internally sets the color buffer bit

	//resizeing for viewvolume
	resize(WIN_WIDTH, WIN_HEIGHT);

	return 0;
}

void resize(int width, int height)
{
	//code
	if (height <= 0)
		height = 1;

	glMatrixMode(GL_PROJECTION); //for matrix calculation while resizing use GL_PROJECTION 
	glLoadIdentity();  //take identity matrix for beginning

	//gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f); //why n0t glFrustum() => imp to angle/perspective in perspective i.e frustum therefore separate api gluperspective() which internally calls to glFrustum()
	GLdouble gldHeight = (GLdouble)(tan(RAD(45.0/2.0)) * 0.1);
	GLdouble gldWidth = gldHeight * ((GLdouble)width / (GLdouble)height);
	
	glFrustum(-gldWidth,gldWidth,-gldHeight, gldHeight,0.1,100.0);

	glViewport(0, 0, (GLsizei)width, (GLsizei)height); //bioscope/Binoculor => focus on which are to be see in window => here we telling to focus on whole window  (mapping of viewing volume wrt window)

}

void display(void)
{

	//func declaration 
	void DrawRoad(void);
	void DrawFootPath(void);
	void DrawSea(void);
	void DrawCar(void);


	//code
	glClear(GL_COLOR_BUFFER_BIT); //clear the window with color whose bit is set
	glMatrixMode(GL_MODELVIEW);   //for matrix calculation while displaying use GL_MODELVIEW beacuse need to display something
	glLoadIdentity();			//take identity matrix for beginning

	glTranslatef(0.0f, 0.0f, -2.0f);

	DrawRoad();
	DrawFootPath();
	DrawSea();

	glTranslatef(sCarPosition.x, sCarPosition.y, sCarPosition.z);
	DrawCar();
	glLoadIdentity();


	SwapBuffers(ghdc); //as in pfd.dwFlags = PFD_DOUBLEBUFFER
	//ghrc is for=> opengl & ghdc is for=> OS
}

void DrawRoad(void)
{
	//main road
	glColor3f(52.0f / 255.0f, 52.0f / 255.0f, 52.0f / 255.0f);
	glBegin(GL_QUADS);
	{
		glVertex3f(1.0f, 1.0f, 0.0f);
		glVertex3f(0.2f, 1.0f, 0.0f);
		glVertex3f(0.2f, -1.0f, 0.0f);
		glVertex3f(1.0f, -1.0f, 0.0f);
	}
	glEnd();

	//road strips
	glColor3f(1.0f, 1.0f, 1.0f);
	glBegin(GL_QUADS);
	{
		glVertex3f(0.62f, 1.0f, 0.0f);
		glVertex3f(0.58f, 1.0f, 0.0f);
		glVertex3f(0.58f, 0.8f, 0.0f);
		glVertex3f(0.62f, 0.8f, 0.0f);
	}
	glEnd();

	glBegin(GL_QUADS);
	{
		glVertex3f(0.62f, 0.4f, 0.0f);
		glVertex3f(0.58f, 0.4f, 0.0f);
		glVertex3f(0.58f, 0.0f, 0.0f);
		glVertex3f(0.62f, 0.0f, 0.0f);
	}
	glEnd();

	glBegin(GL_QUADS);
	{
		glVertex3f(0.62f, -0.4f, 0.0f);
		glVertex3f(0.58f, -0.4f, 0.0f);
		glVertex3f(0.58f, -0.8f, 0.0f);
		glVertex3f(0.62f, -0.8f, 0.0f);
	}
	glEnd();


}

void DrawFootPath(void)
{
	//footpath
	glColor3f(128.0f / 255.0f, 128.0f / 255.0f, 128.0f / 255.0f);
	glBegin(GL_QUADS);
	{
		glVertex3f(0.2f, 1.0f, 0.0f);
		glVertex3f(0.0f, 1.0f, 0.0f);
		glVertex3f(0.0f, -1.0f, 0.0f);
		glVertex3f(0.2f, -1.0f, 0.0f);
	}
	glEnd();

	//shade
	glColor3f(115.f / 255.0f, 115.0f / 255.0f, 120.0f / 255.0f);
	glBegin(GL_QUADS);
	{
		glVertex3f(0.05f, 1.0f, 0.0f);
		glVertex3f(0.0f, 1.0f, 0.0f);
		glVertex3f(0.0f, -1.0f, 0.0f);
		glVertex3f(0.05f, -1.0f, 0.0f);
	}
	glEnd();
}

void DrawSea(void)
{
	
	glBegin(GL_POLYGON);
	{
		
		glColor3f(77.0f / 255.0f, 96.0f / 255.0f, 167.0f / 255.0f);
		glVertex3f(0.0f, 1.0f, 0.0f);

		glColor3f(44.0f / 255.0f, 76.0f / 255.0f, 144.0f / 255.0f);
		glVertex3f(-0.2f, 1.0f, 0.0f);

		glColor3f(8.0f / 255.0f, 10.0f / 255.0f, 116.0f / 255.0f);
		glVertex3f(-0.6f, 1.0f, 0.0f);

		//glColor3f(6.0f / 255.0f, 7.0f / 255.0f, 45.0f / 255.0f);
		glColor3f(7.0f / 255.0f, 8.0f / 255.0f, 100.0f / 255.0f);
		glVertex3f(-1.0f, 1.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 0.0f);

		glColor3f(8.0f / 255.0f, 10.0f / 255.0f, 116.0f / 255.0f);
		glVertex3f(-0.6f, -1.0f, 0.0f);
		
		glColor3f(44.0f / 255.0f, 76.0f / 255.0f, 144.0f / 255.0f);
		glVertex3f(-0.2f, -1.0f, 0.0f);

		glColor3f(77.0f / 255.0f, 96.0f / 255.0f, 167.0f / 255.0f);
		glVertex3f(0.0f, -1.0f, 0.0f);

	}
	glEnd();
}

void DrawCar(void)
{
	//chasi middle
	glColor3f(222.0f/255.0f, 55.0f/255.0f, 255.0f/255.0f);
	glBegin(GL_QUADS);
	{
		glVertex3f(0.94f, 0.2f, 0.0f);
		glVertex3f(0.64f,0.2f, 0.0f);
		glVertex3f(0.64f, -0.2f, 0.0f);
		glVertex3f(0.94f, -0.2f, 0.0f);
	}
	glEnd();
	//chaso front
	glBegin(GL_POLYGON);
	{
		glVertex3f(0.90f, 0.30f, 0.0f);
		glVertex3f(0.68f, 0.30f, 0.0f);

		glVertex3f(0.64f, 0.29f, 0.0f);
		glVertex3f(0.64f, 0.2f, 0.0f);

		glVertex3f(0.94f, 0.2f, 0.0f);
		glVertex3f(0.94f, 0.29f, 0.0f);
		
		
	}
	glEnd();
	//chasi back
	glBegin(GL_POLYGON);
	{
		glVertex3f(0.94f, -0.2f, 0.0f);
		glVertex3f(0.64f, -0.2f, 0.0f);

		glVertex3f(0.64f, -0.23f, 0.0f);
		glVertex3f(0.66f, -0.25f, 0.0f);

		glVertex3f(0.92f, -0.25f, 0.0f);
		glVertex3f(0.94f, -0.23f, 0.0f);
	}
	glEnd();

	//windshields
	//front
	glColor3f(0.1f, 0.1f, 0.1f);
	glBegin(GL_QUADS);
	{
		glVertex3f(0.92f, 0.19f, 0.0f);
		glVertex3f(0.66f, 0.19f, 0.0f);
		glVertex3f(0.68f, 0.1f, 0.0f);
		glVertex3f(0.90f, 0.1f, 0.0f);
	}
	glEnd();

	//left
	glColor3f(0.1f, 0.1f, 0.1f);
	glBegin(GL_QUADS);
	{
		glVertex3f(0.67f, 0.1f, 0.0f);
		glVertex3f(0.65f, 0.19f, 0.0f);
		glVertex3f(0.65f, -0.19f, 0.0f);
		glVertex3f(0.67f, -0.1f, 0.0f);
	}
	glEnd();

	//right
	glColor3f(0.1f, 0.1f, 0.1f);
	glBegin(GL_QUADS);
	{
		glVertex3f(0.93f, 0.19f, 0.0f);
		glVertex3f(0.91f, 0.1f, 0.0f);
		glVertex3f(0.91f, -0.1f, 0.0f);
		glVertex3f(0.93f, -0.19f, 0.0f);
	}
	glEnd();

	//back
	glColor3f(0.1f, 0.1f, 0.1f);
	glBegin(GL_QUADS);
	{
		glVertex3f(0.92f, -0.19f, 0.0f);
		glVertex3f(0.90f, -0.1f, 0.0f);
		glVertex3f(0.68f, -0.1f, 0.0f);
		glVertex3f(0.66f, -0.19f, 0.0f);
	}
	glEnd();

	//headlight left
	glBegin(GL_QUADS);
	{
		glColor3f(52.0f / 255.0f, 52.0f / 255.0f, 52.0f / 255.0f);
		glVertex3f(0.78f, 0.5f, 0.0f);
		glVertex3f(0.63f, 0.5f, 0.0f);

		glColor3f(0.8f, 0.8f, 0.0f);
		glVertex3f(0.68f, 0.3f, 0.0f);
		glVertex3f(0.72f, 0.3f, 0.0f);
	}
	glEnd();

	//headlight right
	glBegin(GL_QUADS);
	{
		glColor3f(52.0f / 255.0f, 52.0f / 255.0f, 52.0f / 255.0f);
		glVertex3f(0.95f, 0.5f, 0.0f);
		glVertex3f(0.80f, 0.5f, 0.0f);

		glColor3f(0.8f, 0.8f, 0.0f);
		glVertex3f(0.86f, 0.3f, 0.0f);
		glVertex3f(0.90f, 0.3f, 0.0f);
	}
	glEnd();

	//tail light
	glBegin(GL_QUADS);
	{
		//glColor3f(1.0f, 0.1f, 0.0f);
		glColor3f(sCarBrake.r, sCarBrake.g, sCarBrake.b);
		glVertex3f(0.93f, -0.23f, 0.0f);
		glVertex3f(0.88f, -0.23f, 0.0f);
		glVertex3f(0.88f, -0.24f, 0.0f);
		glVertex3f(0.93f, -0.24f, 0.0f);
	}
	glEnd();

	glBegin(GL_QUADS);
	{
		// glColor3f(1.0f, 0.1f, 0.0f);
		glColor3f(sCarBrake.r, sCarBrake.g, sCarBrake.b);
		glVertex3f(0.70f, -0.23f, 0.0f);
		glVertex3f(0.65f, -0.23f, 0.0f);
		glVertex3f(0.65f, -0.24f, 0.0f);
		glVertex3f(0.70f, -0.24f, 0.0f);
	}
	glEnd();
}

void update(void)
{
	//code

	//transformation of car
	if (sCarPosition.y <= 0.2f)
	{
		sCarPosition.y = sCarPosition.y + 0.00005f;
	}
	else
	{
		sCarBrake.r = 1.0f;
		sCarBrake.g = 0.1f;
		sCarBrake.b = 0.0f;
	}

}

void uninitialize(void)
{
	//func decln
	void ToggleFullScreen(void);

	//code
	//if application is exiting in fullscreen then execute below code
	if (gbFullScreen == TRUE)
	{
		ToggleFullScreen();
		gbFullScreen = FALSE;
	}

	//make the hdc as current dc
	if (wglGetCurrentContext() == ghrc) //if current context is ghrc  then make current context as NULL
	{
		wglMakeCurrent(NULL, NULL); //make current context NULL
	}
	//delete rendering context
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	//delete/release the hdc
	if (ghdc)
	{
		ReleaseDC(gHwnd, ghdc); //gHwnd => whose windows ghdc =>which ghdc
		ghdc = NULL;
	}

	//destroy window
	if (gHwnd)
	{
		DestroyWindow(gHwnd);
		gHwnd = NULL;
	}

	//close the log file
	if (gpFILE)
	{
		fprintf(gpFILE, "Program for base code terminated successfully!!!!\n");
		fclose(gpFILE);
		gpFILE = NULL;
	}
}



//OpenGL32.lib => import lib
// OpenGL32.dll => code



//this code = glutInit, glutsize, glut

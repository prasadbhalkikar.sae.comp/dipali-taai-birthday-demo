// BY DEFAULT CODE IS IN UNICODE

/*
*** window with***

WM_DESTROY
centering
icon
Full screen
active/inactive window
game loop
FILE IO
keypress handling
WM_CHAR
*/


// full screen mhnje kay ?
// caption bar,task bar kahich nahi disnar 
//windows header files
#include<windows.h>				// win32
#include<stdio.h>				// file IO
#include<stdlib.h>				// exit

#include"OGL.h"

//OpenGL header files, GLUT nahi vaprat ae, native library vaprtoy
#include<gl/GL.h>				// C:/programfiles x86/windows kits/sdk number/include/um/include/gl 

#include<gl/GLU.h>				// native ahe not glut
// macros
#define WIN_WIDTH 640
#define WIN_HEIGHT 480

// link with OpenGL library
#pragma comment(lib,"OpenGL32.lib")

#pragma comment(lib,"glu32.lib")

//global function declarations
// _far mdhe thevleli functions; OS la disayla have, callback ahe
// WPARAM 16 bits, LPARAM 32 bits

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//entry point function
// WinMain is internally, main 

// __WinMainCRTStartup
	// __MainCRTStartup => C Runtime

// hInstance attahcya window cha instance, hPrevInstance NULL ,
// last param => hi window kashi dakhvu, if not given by user show default

// global variable declarations

HWND ghwnd = NULL;
BOOL gbActive = FALSE;
DWORD dwStyle = 0;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
BOOL gbFullscreen = FALSE;
FILE* gpFile = NULL;

// OpenGL related global variables 
HDC ghdc = NULL;

// rendering context
HGLRC ghrc = NULL;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpszCmdLine, int iCmdShow)
{
	//local variable declarations
	// kontya class chi window tayar kraychi te
	// there are predefined classes too like Button class
	WNDCLASSEX wndClass;

	// cannot derefer handle pointer => Opaque; DONT USE POINTER
	HWND hwnd;
	MSG msg;

	//UNICODE, wide character of 2 bytes 
	TCHAR szAppName[] = TEXT("PABWindow");
	int iResult = 0;
	BOOL bDone = FALSE;

	// function declarations
	int initialize();
	void uninitialize();
	void display();
	void update();

	// code
	gpFile = fopen("Log.txt", "w");
	if (gpFile == NULL)
	{
		MessageBox(NULL, TEXT("log file cannot be opened !!"), TEXT("ERROR"), MB_OK | MB_ICONERROR);
		exit(0);
	}

	fprintf(gpFile, "Program started successfully\n");

	// wndClass initialization

	// count of bytes
	wndClass.cbSize = sizeof(WNDCLASSEX);

	// class style => if extended/resized horizonal/vertical the REDRAW

	wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;

	// count of bytes => extra information if any
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;

	// callback register 
	wndClass.lpfnWndProc = WndProc;

	// attachya window cha instance handle
	wndClass.hInstance = hInstance;

	// background cha color sathi brush, brush, font or pen mhnun GetStockObject
	// return value => HGDIOBJECT => graphics device interface
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);

	// handle to icon
	// icon ha resource mhnun load, tuzyakde aslele application sathicha icon lav; i dont have any 
	wndClass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	// same as icon 
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);

	// class cha nav
	wndClass.lpszClassName = szAppName;

	// Menu 
	wndClass.lpszMenuName = NULL;

	// taskbar wala icon 
	wndClass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	// Register wndClass structure 
	RegisterClassEx(&wndClass);

	// create window, window style => WS
	// create window in memory
	// using CreateWindowEx taskbar var fullscreen window yenyasathi
	// mhnun Ex la 12 param, the first param will be new now i.e Extended window style

	int width = GetSystemMetrics(SM_CXSCREEN);
	int height = GetSystemMetrics(SM_CYSCREEN);

	/*
	 WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE
	 child window astil tar, ani tya children ne jar area kapla/cover kela tr kapu deu nkos exclude kar

	 jar sibling ne clip kela tar to pn exclude kar

	 ya exclusion mdhe pn tujhi window dislich pahije

	*/

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Prajakta Aditya Bartakke"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		width / 4,
		height / 4,
		width / 2,
		height / 2,
		NULL,
		NULL,
		hInstance,
		NULL
	);

	// show the window
	// 2nd param msdn 

	ghwnd = hwnd;

	iResult = initialize();

	if (iResult != 0)
	{
		MessageBox(hwnd, TEXT("initialize() failed"), TEXT("ERROR"), MB_OK | MB_ICONERROR);
		DestroyWindow(hwnd);
	}

	ShowWindow(hwnd, iCmdShow);

	// mazya window la foreground la aan 
	// overlapped mhnlay tari ka, karan child/sibling clip mule background la geli apli window tri, pudhe aan 
	SetForegroundWindow(hwnd);

	// WM_SETFOCUS message pathvta WndProc la 
	SetFocus(hwnd);

	//Removing UpdateWindow

	// GAME LOOP

	while (bDone == FALSE)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = TRUE;
			}
			else
			{
				// button dabla tevha he
				char messageBuffer[256];
				//FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, msg.message, 0, messageBuffer, sizeof(messageBuffer), NULL);
				fprintf(gpFile, "Message : %d\n", msg.message);

				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			// Active focus asel tr render kar nahitr pause kar 
			if (gbActive == TRUE)
			{
				// Render
				display();

				// Update
				update();
			}
		}
	}

	// unitialization 
	uninitialize();

	return (int)msg.wParam;
}

//callback function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declare
	void ToggleFullscreen(void);

	void resize(int, int);

	// code
	switch (iMsg)
	{
		// jevha kontya window var focus yeil tevha
	case WM_SETFOCUS:
		gbActive = TRUE;
		break;

	case WM_KILLFOCUS:
		// animation thambnyasathi 
		// jevvha dusrya window var control gela tr apla animation thambayhca
		gbActive = FALSE;
		break;

	case WM_SIZE:
		// lparam width ani height sangto 
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

		// for WM_PAINT we used device context and PAINSTRUCT jyat bg erase kru ki nko/repaint as param hota
		// jo BeginPaint kadun dc yaycha tyat bg erase kraychi capacity aste, tyamule aaplyala hello world vaigere dista hota
		// pn ata WM_PAINT nai, pn tari low level rendering library karnare 
		// mg ti ji bg erase honare ti, WM_PAINT n karta, ata ti ji library ahe ti ata he karnare asa hi case sangte

	case WM_ERASEBKGND:
		// aplya window chya bg la rangvayla apn dilelya brush rangane
		// pn brushcha rangane window rangwayla windows os WM_PAINT vaprte, pn aplyala retained mode nkoy
		// erase ata konitari veglach karnar ahey tu nko karus sangayla return 0
		// return 0 karun to defWindow kde janyapasun advtoy 
		// jar return 0 nasta zala tr te os kde gele asta part ani os la vatla asta ki he erase bg he WM_PAINT ne karaycha ahe 
		// he return 0 sangna mhnje as sangna ki bg erase karyala mazyakde dusra kahiri(OpenGL,directX) ahe, WM_PAINT vapru nkos
		// ha call kdhi kdhi janar jevha jevha WM_PAINT ala ki for 8 cases, jitkya vela WM_PAINT titkya vela ERASE 
		return 0;

	case WM_KEYDOWN:
		switch (LOWORD(wParam))
		{
			// for non character keys 
			// vk is virtual VK_ESCAPE is 27
		case VK_ESCAPE:
			// sends WM_DESTROY
			DestroyWindow(hwnd);
			break;
		}
		break;

	case WM_CHAR:
		// for character key
		switch (LOWORD(wParam))
		{
		case 'F':
		case 'f':
			if (gbFullscreen == FALSE)
			{
				ToggleFullscreen();
				gbFullscreen = TRUE;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = FALSE;
			}
			break;
		}
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}

	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	// local variable declarations

	MONITORINFO mi = { sizeof(MONITORINFO) };

	// code

	if (gbFullscreen == FALSE)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			// primary monitor sathi MonitorFromWindow
			// primary graphics card jyala lavla to primary
			// jyala graphics card cha main adapter lavla to primary 
			// MONITORINFOF_PRIMARY monitor chi information f mhnje flag 

			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo((MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY)), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);			// bitwise one's complement. WS_OVERLAPPEDWINDOW aahe ka shodhla & mule aahe kalal ~ ne remove

				// by second param of below function we are bringing back WS_OVERLAPPED from WS_OVERLAPPEDWINDOW, which was removed previously removed by above function 
				// 7th param parent chi Z order badlu nkos | frame fadun baher janar ,tymule non client area recalculate kar internally message pathvto WM_NCCALCSIZE
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);

	}// A in our case

	else
	{
		// placemenet junya sarkhi kara
		SetWindowPlacement(ghwnd, &wpPrev);

		// WS_OVERLAPPEDWINDOW ADD
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		// window ashi set kara ki ti top var anna var kely OVERLAPPED pn tari ya khalycha function la jast priority
		// NOMOVE window halvu nko, jith hoto tithch
		// size bdlu nko
		// NOOWNERZORDER jr owner chi zorder bdalli tri ya window chi z order bdlu nko
		// swatcha karrmane pn z order badlu nko window chi 
		// RECALCULATE 
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		// system cursor dakhv
		ShowCursor(TRUE);
	}

}


int initialize()
{

	void resize(int width, int height);
	// function declarations

	// code
	// chota bacchan sathi cha pixel format, mi sangtoy te vapar, mi 9 sangtoi baki tu default ghe

	// step 1 Init PIXELFORMATDESCRIPTOR struct 

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;

	// pfd structure saglya members la 0 init, total 28
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	// initialisation of struct
	// struct bytesize
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);

	// conventionally yacha version 1 ahe, windows ne lock kelela OpenGL 1.5, for DirectX, n mhnje integer
	pfd.nVersion = 1;

	// window var draw kar, bitmap/printer var pn draw krta yet pn mla window vr kraych
	// attaprynt tu retained mode draw kart hotas ata immediate mhnun supoort OpenGL
	// vel alyawr 
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;

	// pixel cha type RGBA vapar
	pfd.iPixelType = PFD_TYPE_RGBA;

	// kiti bit chya RGBA vapru
	pfd.cColorBits = 32;	// 8 bits per color

	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	// step2 get DC painter, for step 3
	// many ways to create DC than BeginPaint, createCompatibleDC
	// BeginPaint wlaa DC fakt WM_PAINT mdhe vaprtat.
	// mhnun ata BeginPaint wala painter nko, GetDC wala painter kshaala hi vapru shktos
	// GetDC wala lai bhari kam krta jevha konitari vegla engine
	ghdc = GetDC(ghwnd);

	if (ghdc == NULL)
	{
		fprintf(gpFile, "GetDC() failed\n");
		return -1;
	}

	// step 3 : he ghe maza pixelformatdescriptor, tuzyakde asnarya anek pixelFormatDescriptor paiki
	// jyachyashi close match hoil tyacha index de., mi det aslela HDC vaprun 

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)			// should be nonzero positive
	{
		fprintf(gpFile, "ChoosePixelFormat() failed\n");
		return -2;
	}

	// step 4 set obtained pixel format
	// konamdhe set karaycha ahe -> ghdc
	// kontya index mdhun 

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		fprintf(gpFile, "SetPixelFormat() failed\n");
		return -3;
	}

	// ata hdc madhe aplyala havi ti pixel format chi information ahe
	// ha dc os ne dilela specialist ahe os chya painting 
	// tyachi capactity ahe rendering chi dc vhaychi
	// windowing mdhun OpenGL mdhun jatana aplayla bridge lagto 
	// windows mdhlya brigde la WGL
	// tell WGL to give me OpenGL compatible DC from using ghdc 

	// step 5 
	// tel WGL to give OpenGL device context from this device context

	// create OpenGL context from device context 

	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL)
	{
		fprintf(gpFile, "wglCreateContext() failed\n");
		return -4;
	}

	// ataprynt kam ghdc karat hota pn ata to control rc kde deil for further drawing
	// make rendering context current 

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(gpFile, "wglMakeCurrent() failed\n");
		return -5;
	}

	// set the clear color of window to blue
	// retained mode RGB => 256 color 2 raised of 8
	// OpenGL sathi 2^32 (apanch dila 32 bits var)

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // alpha mhnje 1.0 atta transparent nahiyye

	// HERE OpenGL STARTS....

	resize(WIN_WIDTH, WIN_HEIGHT);

	return 0;
}

void resize(int width, int height)
{
	
	glViewport(0, 0, (GLsizei)width, (GLsizei)height); // (durbin) window javal karun bghaycha pn atta mla akkahi window bghaychi ahe

		// he sagle pudhe
	glMatrixMode(GL_PROJECTION);	// matrix chi maths GL_PROJECTION matrix vapar, durchya gosticha bghayla projection bdalat
	glLoadIdentity();				// start la identity matrix vapar

	// standard asnare ata purna batch
	// near far peksha javal
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void stageFront()
{
	glBegin(GL_POLYGON);

//	rgb(204, 119, 34)
	glColor3f(204.0f / 255.0f, 119.0f / 255.0f, 34.0f / 255.0f);
	//glColor3f(139.0f / 255.0f, 69.0f / 255.0f, 19.0f / 255.0f);

	
	glVertex3f(10.0f, 0.0f, 0.0f);
	glVertex3f(10.0f, 0.07f, 0.0f);
	glVertex3f(-10.0f, 0.07f, 0.0f);
	glVertex3f(-10.0f, 0.0f, 0.0f);

	glEnd();
}

void stage()
{
	glBegin(GL_POLYGON);

	//rgb(139, 69, 19)

	//glColor3f(204.0f / 255.0f, 119.0f / 255.0f, 34.0f / 255.0f);
	glColor3f(139.0f / 255.0f, 69.0f / 255.0f, 19.0f / 255.0f);

	glVertex3f(5.0f, 2.0f, 0.0f);
	glVertex3f(-5.0f, 2.0f, 0.0f);
	glVertex3f(-7.5f, 0.0f, 0.0f);
	glVertex3f(7.5f, 0.0f, 0.0f);

	glEnd();

	

}

void stageBg()
{
	glBegin(GL_POLYGON);

	//glColor3f(136.0f / 255.0f, 8.0f / 255.0f, 8.0f / 255.0f);

	//glColor3f(212.0f / 255.0f, 0.0f / 255.0f, 0.0f / 255.0f);

	glColor3f(136.0f / 255.0f, 8.0f / 255.0f, 8.0f / 255.0f);
	glVertex3f(1.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, 0.0f);

	glEnd();

}


void drawStage()
{
	glPushMatrix();
	{
		glTranslatef(0.0f, -3.0f, 0.0f);
		glScalef(3.0f, 3.0f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);
		stageFront();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(0.0f, 0.0f, 0.0f);
		glScalef(8.0f, 2.5f, 0.0f);
		stageBg();
	}
	glPopMatrix();

#if 1
	glPushMatrix();
	{
		glTranslatef(0.0f, -2.8f, 0.0f);
		stage();
	}
	glPopMatrix();

#endif	


}

void display()
{
	// code 
	// init mdhe je apn sangto ki clear color to blue tithch to blue krun taknar nahiye
	// proper bhint ithe rangvli jail display mdhe
	glClear(GL_COLOR_BUFFER_BIT); //glClearColor internally la COLOR_BUFFER=FRAME_BUFFER sets blue

	// proper drawing cha matrix havey mhnun MODELVIEW, resize hota tevha projection 
	// MATRIX STACK cha lecture mdhe he
	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -9.0f);// 3 ne aat dhakkla , OpenGL mdhe positive mhnje baher
	
	drawStage();

	// double buffering he os buffer swap kar
	// why ghdc parat ? karan ghrc ha OpenGL la kalto ghdc os la kalto pn doghe same ahet
	SwapBuffers(ghdc);
}

void update()
{
	// FOR ANIMATION 
}

void uninitialize()
{
	// function declarations 
	void ToggleFullscreen(void);

	// code 
	// full screen astana band kela mhnun, tevha mg nntr ti normal hoil karan full screen mdhe os kadche jast buffers vaprle jatil
	// if application is exiting in full screen 
	if (gbFullscreen == TRUE)
	{
		ToggleFullscreen();
		gbFullscreen = FALSE;
	}

	// make the hdc as current context
	// jar chalu context ghrc asel tr donhi param  null
	if (wglGetCurrentContext() == ghrc)
	{
		// os la sangun OpenGL cha kadhun dc la control 
		wglMakeCurrent(NULL, NULL);
	}

	// Delete rendering context
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	// Release the hdc
	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// Destroy window
	if (ghwnd)
	{
		DestroyWindow(ghwnd);
		ghwnd = NULL;
	}

	// close the log file

	if (gpFile)
	{
		fprintf(gpFile, "Program ended successfully\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}


